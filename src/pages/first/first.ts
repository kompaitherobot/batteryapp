import { Component, OnInit } from "@angular/core";
import { NavController } from "ionic-angular";
import { ParamService } from "../../services/param.service";
import { ApiService } from "../../services/api.service";
import { AlertService } from "../../services/alert.service";
import { HomePage } from "../home/home";
import { LoadingController } from "ionic-angular";
import { PopupService } from "../../services/popup.service";
import { SpeechService } from "../../services/speech.service";

@Component({
  selector: "page-first",
  templateUrl: "first.html",
})
export class FirstPage implements OnInit {
  homePage = HomePage;
  appopen: any;
  cpt_open: number;
  loading: any;

  constructor(
    public loadingCtrl: LoadingController,
    public popup: PopupService,
    public param: ParamService,
    public alert: AlertService,
    public api: ApiService,
    public speech: SpeechService,
    public navCtrl: NavController
  ) {
    this.cpt_open = 0;
    this.loading = this.loadingCtrl.create({});
    this.loading.present();
  }

  ngOnInit() {
    this.appopen = setInterval(() => this.appOpen(), 1000);
    this.speech.getVoice();
  }

  appOpen() {
    if (this.param.localhost != undefined) {
      this.api.checkrobot();

    }
    this.api.checkInternet();
    this.alert.checkwifi(this.api.wifiok);
    this.cpt_open += 1;
    if (this.cpt_open === 15) {
      this.popup.startFailedAlert();
      this.speech.speak(this.param.datatext.cantlaunch);
    }
    if (!this.api.appOpened) {
      this.param.getDataRobot();
      this.param.getMail();
      this.param.getDurationNS();
      this.param.getQR();
      this.param.getBattery();
      if (this.param.robot) {
        this.param.fillData();
        if (this.param.robot.httpskomnav == 0) {
          this.api.httpskomnav = "http://";
          this.api.wsskomnav = "ws://";
        } else {
          this.api.httpskomnav = "https://";
          this.api.wsskomnav = "wss://";
        }
        if (this.param.robot.httpsros == 0) {
          this.api.wssros = "ws://";
        } else {
          this.api.wssros = "wss://";
        }
      }

      if (this.api.robotok && this.param.langage && this.param.maillist) {
        this.api.abortHttp();
        if (!this.api.socketok)
        {
          this.api.instanciate();
        }
        this.api.getCurrentMap();
        this.api.appOpened = true;
        this.api.eyesHttp(4);
      }
    } else if (this.api.appOpened && this.cpt_open >= 6) {

      this.alert.appOpen(this.api.mailAddInformationBasic());
      this.navCtrl.setRoot(this.homePage);
      this.loading.dismiss();
      if (this.popup.alert_blocked) {
        this.popup.alert_blocked.dismiss();
      }
      clearInterval(this.appopen);
      console.log(this.cpt_open);
    }
  }
}
