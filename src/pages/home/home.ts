import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Renderer2,
} from "@angular/core";
import { ApiService } from "../../services/api.service";
import { AlertService } from "../../services/alert.service";
import { PopupService } from "../../services/popup.service";
import { ParamService } from "../../services/param.service";
import { SpeechService } from "../../services/speech.service";
import { LoadingController, NavController } from "ionic-angular";
import { ToastController } from "ionic-angular";
import { Select } from "ionic-angular";
import nipplejs from "nipplejs";
import jsQR from "jsqr";

@Component({
  selector: "page-home",
  templateUrl: "home.html",
})
export class HomePage implements OnInit {
  size: number = 70;
  maxrad: number = 0.08;
  maxlin: number = 0.12;
  maxback: number = 0.08;
  cptnospeed: any = 0;
  vrad: number;
  vlin: number;
  gamepadinterv: any;
  update: any;
  loading: any;
  start_from_docking: boolean; //to know when the robot start from docking
  cpt_locked: number; // cpt to know if kompai is blocked
  is_blocked: boolean;
  sum_blocked: number;
  time_blocked: number;
  date_blocked: Date;
  cptduration: any;
  selectOptions: any;
  wait: boolean = false;
  @ViewChild("video") videoElement: ElementRef;
  @ViewChild("canvas") canvas: ElementRef;
  @ViewChild("videoCanvas") videoCanvas: ElementRef;
  videoWidth = 0;
  videoHeight = 0;

  currentx: number;
  currenty: number;
  lastx: number;
  lasty: number;
  countjoystick: number;
  checkjoystick: boolean;
  stopjoystick: boolean;
  manager:any
  qrdetection:boolean;
  imgRos : any;

  ngOnInit() {
    this.speech.getVoice();
  }

  @ViewChild("select1") select1: Select;

  constructor(
    
    public navCtrl: NavController,
    public param: ParamService,
    public toastCtrl: ToastController,
    public speech: SpeechService,
    public popup: PopupService,
    public loadingCtrl: LoadingController,
    public api: ApiService,
    public alert: AlertService,
    private renderer: Renderer2
  ) {

    this.start_from_docking = false;
    this.cpt_locked = 0;
    this.sum_blocked = 0;
    this.api.QRselected = undefined;
    this.time_blocked = 0;
    this.selectOptions = {
      title: this.param.datatext.qrcode,
    };
    // declenche la popup de permission camera si besoin
    this.setupconstraint();
    this.api.poiDocking();

    this.update = setInterval(() => this.getUpdate(), 500); // update the status and the trajectory every 1/2 secondes
    this.cptduration = setInterval(() => this.mail(), 120000); // toutes les 2 min actualise time

  }

  mail() {

    this.param.cptDuration(); //update duration
    if (this.param.durationNS.length > 1) {
      if (this.api.wifiok) {
        //this.param.updateDurationNS(this.param.durationNS[0].id_duration);
        this.alert.duration(this.param.durationNS[0], this.api.mailAddInformationBasic());
        //this.param.getDurationNS();
      }
    }
  }

  dismissLoading() {
    if (this.loading) {
      this.loading.dismiss();
      this.loading = null;
    }
  }

  okToast(m: string, n: number) {
    const toast = this.toastCtrl.create({
      message: m,
      duration: n,
      position: "middle",
      cssClass: "toastam",
    });
    toast.present();
  }

  scan(ev) {
    ev.preventDefault();
     ///si cam usb
    if(this.param.robot.cam_USB==1){
    console.log("enter scan usb");
    try{
      this.videoCanvas.nativeElement.height = this.videoElement.nativeElement.videoHeight;
      this.videoCanvas.nativeElement.width = this.videoElement.nativeElement.videoWidth;
  
      const g = this.videoCanvas.nativeElement.getContext("2d");
      g.drawImage(this.videoElement.nativeElement, 0, 0, this.videoCanvas.nativeElement.width, this.videoCanvas.nativeElement.height);
      const imageData = g.getImageData(0, 0, this.videoCanvas.nativeElement.width, this.videoCanvas.nativeElement.height);
      const code = jsQR(imageData.data, imageData.width, imageData.height, {
        inversionAttempts: "dontInvert",
      });
  
      if (code){
        console.log(code.data.trim());
        if (
          code.data !== null &&
          this.param.qrcodes.filter((x) => x.qr == code.data.trim())
            .length > 0
        ) {
          this.api.QRselected = this.param.qrcodes.filter(
            (x) => x.qr == code.data.trim()
          )[0];
          this.onReloc();
        } else if (
          code.data !== null &&
          this.param.qrcodes.filter((x) => x.qr == code.data.trim())
            .length === 0
        ) {
          this.okToast(this.param.datatext.unknownqrcode, 4000);
        }
      }else{
        this.popup.showToastRed(
          this.param.datatext.noqrdetected,
          5000,
          "middle"
        );
        this.speech.speak(this.param.datatext.noqrdetected);
      }
    } catch(err){
      console.log("Error", err);
    }

   
          /////si cam ros
    }else{
      console.log("scan qr ros");
      this.qrdetection=true;
      this.api.startQrDetection(true);
      setTimeout(() => {
        if(this.qrdetection){
          this.qrdetection=false;
          this.api.startQrDetection(false);
          this.popup.showToastRed(
            this.param.datatext.noqrdetected,
            5000,
            "middle"
          );
          this.speech.speak(this.param.datatext.noqrdetected);
        }
      }, 5000);
      this.api.listener_qr.subscribe((message) => {
        console.log('Received message on ' + this.api.listener_qr.name + ': ' + message.data);
        if(this.qrdetection){
          this.qrdetection=false;
          this.api.startQrDetection(false);
          if (
            message.data !== null &&
            this.param.qrcodes.filter((x) => x.qr == message.data.trim())
              .length > 0
          ) {
            this.api.QRselected = this.param.qrcodes.filter(
              (x) => x.qr == message.data.trim()
            )[0];
            this.onReloc();
          } else if (
            message.data !== null &&
            this.param.qrcodes.filter((x) => x.qr == message.data.trim())
              .length === 0
          ) {
            this.popup.showToastRed(this.param.datatext.unknownqrcode, 4000,"middle");
            this.speech.speak(this.param.datatext.unknownqrcode);
          }
          }

      })

      }
  }

  showLoading() {
    if (!this.loading) {
      this.loading = this.loadingCtrl.create({
        content: this.param.datatext.relocalizing,
      });
      this.loading.present();
    }
  }

  onQRChange(event) {
    this.api.QRselected = this.param.qrcodes.filter((x) => x.qr == event)[0];
    this.onReloc();

  }

  ionViewWillLeave() {
    if(this.param.robot.cam_USB==0){
      this.api.listener_camera.removeAllListeners();
    }
  }
  convertDataURIToBinary2(dataURI) {
    var raw = window.atob(dataURI);
    var rawLength = raw.length;
    var array = new Uint8Array(new ArrayBuffer(rawLength));


    for(var i = 0; i < rawLength; i++) {
      array[i] = raw.charCodeAt(i);
    }
    return array;
  }

  ionViewDidEnter() {
    // CAMERA ROS
   if( this.param.robot.cam_USB==0 ){

    this.api.listener_camera.subscribe((message) => {

      //console.log('Received message on ' + listener_compressed.name + ': ');
      this.imgRos = document.getElementById('imgcompressed');
      //console.log(message);

      var bufferdata = this.convertDataURIToBinary2(message.data);

      var blob = new Blob([bufferdata], {type: 'image/jpeg'});
      var blobUrl = URL.createObjectURL(blob);

      this.imgRos.src = blobUrl;


    });

}
  }

  btnPressed() {
    // stop the robot if bouton pressed
    if (this.api.is_background || this.api.btnPush || !this.api.is_connected) {
      this.api.foreground();
      if (this.api.towardDocking) {
        this.api.abortNavHttp();
        this.api.abortDockingHttp();
        this.api.towardDocking = false;
        if (this.api.battery_status.remaining <= this.param.battery.critical) {
          // lowBattery
          this.popup.lowBattery();
        }
      }
      this.api.btnPush = false; // to say that we have received the btn pushed status
    }
  }

  updateTrajectory() {
    // listen the round status to allow the robot to go to the next
    //// go docking or go poi in progress
    if (this.api.towardDocking) {
      //console.log("hehe");
      if (this.api.differential_status.TargetAngularSpeed === 0 && this.api.differential_status.TargetLinearSpeed === 0) {
        this.cptnospeed = this.cptnospeed + 1;
      }
      if (this.cptnospeed == 15) {
        this.api.towardDocking = false;
        this.api.abortNavHttp();
        this.cptnospeed = 0;
        this.wait = false;
      }
      if (this.api.differential_status.status === 2) {
        // if current is hight
        this.api.towardDocking = false;
        this.api.abortNavHttp();
        this.popup.errorBlocked();
        this.alert.errorblocked(this.api.mailAddInformationBasic());
        this.capture();
      } else if (this.api.navigation_status.status === 5) {
        // nav error

        this.api.towardDocking = false;
        this.api.towardPOI = false;
        this.api.abortNavHttp();
        this.popup.errorNavAlert();
        this.alert.naverror(this.api.mailAddInformationBasic());
      } else if (this.api.battery_status.status === 0) {
        this.api.towardDocking = false;
        this.alert.charging(this.api.mailAddInformationBasic());
        this.api.close_app = true; //close application
        setTimeout(() => {
          window.close();
        }, 1000);
      } else if (
        this.api.towardDocking &&
        this.api.navigation_status.status === 0 &&
        this.api.docking_status.detected
      ) {
        //connect to the docking when he detect it
        this.api.connectHttp();
      }

    }
  }


  getUpdate() {
    //console.log("l");
    if (this.api.mapdata) {
      if (this.api.battery_status.status != 0 && !this.api.towardDocking && !this.wait && this.api.battery_status.remaining <= this.param.battery.critical) {
        this.wait = true;


        setTimeout(() => {
          this.speech.speak(this.param.datatext.godocking);
          this.popup.showToast(this.param.datatext.godocking, 10000, "middle");
          this.onGo();
        }, 4000);

        setTimeout(() => {
          this.wait = false;
        }, 180000);
      }
    }

    // update trajectory, butons input, round status
    if (this.api.close_app) {
      clearInterval(this.update);
      clearInterval(this.cptduration);
    }
    if (!this.api.is_connected) {
      this.api.towardDocking = false;
    } else {
      if (this.api.fct_onGo) {
        this.api.fct_onGo = false;
        this.onGo();
      }
      this.btnPressed();
      this.updateTrajectory();
      this.watchIfLocked();
    }
    this.sendMailduration();


      // test joystick count
      if(this.checkjoystick){

    
        if(this.currentx == this.lastx ){
          if(this.currenty == this.lasty)
          {
            this.countjoystick = this.countjoystick + 1;
            if(this.countjoystick >= 5)
            {
              console.log("stopjoystick");
              this.joystickstop();
              this.stopjoystick = true;
            }
            if(this.countjoystick >= 15)
            {
              clearInterval(this.gamepadinterv);
              this.checkjoystick = false;
              this.manager.destroy();
              this.createjoystick();
              this.checkjoystick=false;
            }
          }
          else
          {
            this.countjoystick = 0;
            this.stopjoystick = false;
            this.checkjoystick = true;
            this.lastx = this.currentx;
            this.lasty = this.currenty;
          }
        }
        else
        {
          this.countjoystick = 0;
          this.stopjoystick = false;
          this.checkjoystick = true;
          this.lastx = this.currentx;
          this.lasty = this.currenty;
        }
      
      }
  }

  sendMailduration() {
    if (this.param.sendduration) {
      this.param.sendduration = false;
      this.alert.duration(
        this.param.olddata,
        this.api.mailAddInformationBasic()
      );
    }
  }

  watchIfLocked() {
    if (this.api.towardDocking) {
      if (
        (this.api.towardDocking && this.api.docking_status.detected) ||
        this.api.docking_status.status === 3 ||
        (this.api.anticollision_status.forward < 2 &&
          this.api.anticollision_status.right < 2 &&
          this.api.anticollision_status.left < 2)
      ) {
        this.cpt_locked = 0;
        if (this.is_blocked) {
          this.is_blocked = false;
          this.popup.alert_blocked.dismiss();
        }
      } else if (
        this.api.anticollision_status.forward === 2 ||
        this.api.anticollision_status.right === 2 ||
        this.api.anticollision_status.left === 2
      ) {
        this.cpt_locked += 1;
      }

      if (this.cpt_locked > 30 && !this.is_blocked) {
        this.popup.blockedAlert();
        this.is_blocked = true;
        this.alert.blockingdocking(this.api.mailAddInformationBasic());
      } else if (this.cpt_locked === 80 && this.is_blocked) {
        this.capture();
      }
    }
  }

  getDistance(x1, y1, x2, y2) {
    let y = x2 - x1;
    let x = y2 - y1;
    let res = Math.sqrt(x * x + y * y);
    //console.log(res);
    return res;
  }

  calcDiffMin(one: Date, two: Date) {
    var DateBegMin = one.getMinutes();
    var DateBegHours = one.getHours() * 60;
    var DateBeg = DateBegMin + DateBegHours;

    var DateEndMin = two.getMinutes();
    var DateEndHours = two.getHours() * 60;
    return DateEndMin + DateEndHours - DateBeg;
  }

  onReloc() {
    // buton go code
    this.alert.reloc(this.api.mailAddInformationBasic());
    this.showLoading();
    this.speech.speak(this.param.datatext.alertReloc);
    this.api.getCurrentMap();
    setTimeout(() => {
      if (!(this.api.id_current_map == this.api.QRselected.id_map)) {
        console.log("changemap");
        this.api.changeMapbyIdHttp();
      }
    }, 2000);
    setTimeout(() => {
      this.api.getFirstLocationHttp();
    }, 3000);
    setTimeout(() => {
      this.api.resetLocationHttp();
    }, 5000);
    setTimeout(() => {
      this.api.getCurrentMap();
    }, 6000);
    setTimeout(() => {
      this.dismissLoading();
      this.api.poiDocking();
      this.okToast(
        this.param.datatext.localizationDone + this.api.mapdata.Name,
        4000
      );

    }, 7000);
  }

  onclickGo(ev) {
    ev.preventDefault();
    this.onGo();
  }

  onGo() {
    this.cptnospeed = 0;
    // buton go code
    if (this.api.towardDocking) {
      // stop go docking
      this.api.towardDocking = false;
      this.api.abortNavHttp();
      this.api.abortDockingHttp();
      if (this.api.battery_status.remaining <= this.param.battery.critical) {
        // lowBattery
        this.popup.lowBattery();
      } else {
        //this.popup.goDockingConfirm();
      }
    } else {
      this.alert.askCharge(this.api.mailAddInformationBasic());
     
      if (this.api.dockingexist) {
        this.api.towardDocking = true;
        // var x1 = this.api.docking.Pose["X"];
        // var y1 = this.api.docking.Pose["Y"];
        // var x2 = this.api.localization_status.positionx;
        // var y2 = this.api.localization_status.positiony;
        // var dist = this.getDistance(x1, y1, x2, y2);

        // if (dist < 0.5) {
        //   if (this.api.docking_status.detected && this.api.anticollision_status.forward == 1) {
        //     this.api.towardDocking = true;
        //     this.api.connectHttp();
        //   }
        //   else if (this.api.docking_status.detected && this.api.anticollision_status.forward == 2) {
        //     this.api.towardDocking = true;
        //     this.api.disconnectHttp();
        //     setTimeout(() => {
        //       if (this.api.docking_status.detected) {
        //         this.api.connectHttp();
        //       } else {
        //         this.api.reachHttp("docking");
        //       }

        //     }, 7000);
        //   }
        // }
        // console.log(this.api.anticollision_status.forward < 2)
        // console.log(this.api.anticollision_status.reverse > 0);
        // console.log(this.api.anticollision_status.left > 0);
        // console.log(this.api.anticollision_status.right > 0);
        if (this.api.anticollision_status.forward<2 && (this.api.anticollision_status.reverse>0 || this.api.anticollision_status.locked)  && ( this.api.anticollision_status.right >0 || this.api.anticollision_status.left >0)) {
          //console.log("coucou");
          this.api.joystickHttp(0.2, 0);
          setTimeout(() => {
            this.api.joystickHttp(0.2, 0);
          }, 1000);
        }
        else if (this.api.anticollision_status.forward<2 && this.api.anticollision_status.right==2 && this.api.anticollision_status.left<2){
          this.api.joystickHttp(0, 0.3);
          setTimeout(() => {
            this.api.joystickHttp(0, 0.3);
          }, 1000);
        }
        else if (this.api.anticollision_status.forward<2 && this.api.anticollision_status.left==2 && this.api.anticollision_status.right<2){
          this.api.joystickHttp(0, -0.3);
          setTimeout(() => {
            this.api.joystickHttp(0, -0.3);
          }, 1000);
        }
        
        else if (this.api.anticollision_status.forward == 2) {
          this.api.disconnectHttp();
          setTimeout(() => {
            if(this.api.towardDocking){
              this.api.reachHttp("docking");
            }
            
          }, 8000);
        }

        else {
          this.api.reachHttp("docking");
        }


      } else {
        this.popup.showToastRed(this.param.datatext.nodocking, 10000, "middle");
      }

    }
  }

  constraints = {
    //audio: false,
    //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
    video: true,

    facingMode: "environment",
    width: { ideal: 1920 },
    height: { ideal: 1080 },
  };

  // config la variable constraints
  setupconstraint() {
    // demande de permission camera
    navigator.mediaDevices.getUserMedia(this.constraints);

    // verification des permissions video et de la disponibilité de devices
    if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
      // enumeration des appareils connectes pour filtrer
      navigator.mediaDevices.enumerateDevices().then((result) => {
        var constraints;
        console.log(result);

        result.forEach(function (device) {
          // filtrer pour garder les flux video
          if (device.kind.includes("videoinput")) {
            // filtrer le label de la camera
            if (device.label.includes("USB")) {
              constraints = {
                audio: false,
                //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
                // affecter le deviceid filtré
                video: { deviceId: device.deviceId },

                facingMode: "environment",
                width: { ideal: 1920 },
                height: { ideal: 1080 },
              };
            }
          }
        });
        // on lance la connexion de la vidéo
        if (constraints) {
          this.constraints = constraints;
          this.startCamera(constraints);
          console.log("startcam");
        }
        //this.startCamera(constraints);
      });
    } else {
      console.log("Sorry, camera not available.");
    }
  }

  // function launch connection camera
  startCamera(cs) {
    if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
      // recupération du flux avec les constraints configurés puis on attache le flux à la balise video
      navigator.mediaDevices
        .getUserMedia(cs)
        .then(this.attachVideo.bind(this))
        .catch(this.handleError);
    } else {
      alert("Sorry, camera not available.");
    }
  }

  handleError(error) {
    console.log("Error: ", error);
  }

  // function qui attache le flux video à la balise video
  attachVideo(stream) {
    this.renderer.setProperty(
      this.videoElement.nativeElement,
      "srcObject",
      stream
    );
    this.renderer.listen(this.videoElement.nativeElement, "play", (event) => {
      this.videoHeight = this.videoElement.nativeElement.videoHeight;
      this.videoWidth = this.videoElement.nativeElement.videoWidth;
    });
  }

  // function take snapshot and send mail
  capture() {
    if (this.param.robot.send_pic == 1) {
      this.setupconstraint();
      this.renderer.setProperty(
        this.canvas.nativeElement,
        "width",
        this.videoWidth
      );
      this.renderer.setProperty(
        this.canvas.nativeElement,
        "height",
        this.videoHeight
      );
      this.canvas.nativeElement
        .getContext("2d")
        .drawImage(this.videoElement.nativeElement, 0, 0);
      var url = this.canvas.nativeElement.toDataURL();
      //console.log(url);
      this.alert.Blocked_c(url);
    } else {
      this.alert.Block_c();

    }

  }

  onSliderRelease(ev, id: Select){
    ev.preventDefault();
    id.open();
  }
  joystickmove() {
    this.api.joystickHttp(this.vlin, this.vrad);
  }

  joystickstop() {
    this.api.joystickHttp(0, 0);
  }

  createjoystick(){

    var options = {
      zone: document.getElementById("zone_joystick"),
      size: 3 * this.size,
    };

    this.manager = nipplejs.create(options);

    this.manager.on("move", (evt, nipple) => {
      this.currentx = nipple.raw.position.x ;
      this.currenty = nipple.raw.position.y ;
      this.checkjoystick = true;
      if(!this.stopjoystick){
        if (nipple.direction && nipple.direction.angle && nipple.angle) {
          //console.log(nipple.direction.angle);
          if (nipple.direction.angle === "left") {
            this.vlin = 0;
            this.vrad = (0.3 * nipple.distance) / 50;
          } else if (nipple.direction.angle === "right") {
            this.vlin = 0;
            this.vrad = -(0.3 * nipple.distance) / 50;
          } else if (nipple.direction.angle === "up") {
            this.vlin = (this.maxlin * nipple.distance) / 50;
            this.vrad = 0;
            // if (Math.abs((nipple.angle.radian - Math.PI / 2) / 2) > 0.18) {
            //   this.vrad = (nipple.angle.radian - Math.PI / 2) / 2 - 0.09;
            // } else {
            // this.vrad = 0;
            // }
          } else if (nipple.direction.angle === "down") {
            this.vlin = -(this.maxback * nipple.distance) / 50;
            this.vrad = 0;
          }
        }
      }else{
        this.vlin = 0;
        this.vrad = 0;
      }
    });

    this.manager.on("added", (evt, nipple) => {
      console.log("added");
      this.gamepadinterv = setInterval(() => {if(!this.stopjoystick){this.joystickmove();}}, 500);
    });
    this.manager.on("end", (evt, nipple) => {
      console.log("end");
      this.vlin = 0;
      this.vrad = 0;
      clearInterval(this.gamepadinterv);
      this.checkjoystick = false;
      this.stopjoystick = false;
      this.joystickstop();
    });
  }

  ngAfterViewInit() {
    this.createjoystick();
  }

}
