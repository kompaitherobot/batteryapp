import { Component, OnInit, ViewChild } from '@angular/core';
import { ParamService } from '../../services/param.service';
import { LoadingController } from 'ionic-angular';
import { Select } from 'ionic-angular';




@Component({
  selector: 'page-langue',
  templateUrl: 'langue.html'
})

export class LanguePage implements OnInit {

  loading:any;

  monitor: string;
  ngOnInit() {

  }

  @ViewChild('select1') select1: Select;

  constructor( public param:ParamService, public loadingCtrl:LoadingController) {
    this.monitor=this.param.langage;

  }

  monitorHandler(){
    this.loading = this.loadingCtrl.create({
    });
    this.loading.present();//loading animation display
    this.param.updateRobot();
    setTimeout(
      () => {
        document.location.reload();
      }, 1000
    );
    
  }

  onSliderRelease(ev, id: Select){
    ev.preventDefault();
    id.open();
  }
}
