import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ParamService } from '../../services/param.service';

import PDFJS from "pdfjs-dist/webpack.js";


@Component({
  selector: 'page-tuto',
  templateUrl: 'tuto.html'
})

export class TutoPage implements OnInit {

  pageNum = 1;

  pdfSrc = "https://vadimdez.github.io/ng2-pdf-viewer/assets/pdf-test.pdf";

  pdfDocument: PDFJS.PDFDocumentProxy;
  PDFJSViewer = PDFJS;

  currPage:number = 1; //Pages are 1-based not 0-based
  numPages = 0;
  thePDF = null;

  pageRendering = false;
  pageNumPending = null;

  ngOnInit() {

  }

  ionViewDidEnter() {
    
    this.launchdoc();
}

  constructor(public sanitizer: DomSanitizer, public param: ParamService) {
  
  }

  launchdoc(){
    //This is where you start
    PDFJS.getDocument(this.param.datatext.URL_battery).then((pdf)=> {

            //Set PDFJS global object (so we can easily access in our page functions
            this.thePDF = pdf;

            //How many pages it has
            this.numPages = pdf.numPages;

            //Start with first page
            pdf.getPage( 1 ).then( (page)=> {this.handlePages(page)} );
    });
  }

  handlePages(page)
  {
      //This gives us the page's dimensions at full scale
      var viewport = page.getViewport( 2 );

      //We'll create a canvas for each page to draw it on
      var canvas = document.createElement( "canvas" );
      canvas.style.display = "block";
      var context = canvas.getContext('2d');


      canvas.height = viewport.height;
      canvas.width = viewport.width;

      //Draw it on the canvas
      page.render({canvasContext: context, viewport: viewport});

      //Add it to the web page
      //document.body.appendChild( canvas );
      //console.log(document.getElementById("ionbody"));
      document.getElementById("ionbody").appendChild(canvas);

      var line = document.createElement("hr");
      document.getElementById("ionbody").appendChild( line );

      //console.log(this.currPage);
      //Move to next page
      this.currPageAdd();
      if ( this.thePDF !== null && this.currPage <= this.numPages )
      {
          this.thePDF.getPage( this.currPage ).then( (page)=> {this.handlePages(page)} );
      }
  }

  currPageAdd(){
    this.currPage = this.currPage + 1 ;
  }
}
