import { NgModule } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from '../app/app.component';
import { HeadpageComponent } from './headpage/headpage';
@NgModule({
	declarations: [HeadpageComponent],
	imports: [IonicModule.forRoot(MyApp)],
	bootstrap: [IonicApp],
	entryComponents: [
		MyApp
	 
	  ],
	exports: [HeadpageComponent]
})
export class ComponentsModule {}
