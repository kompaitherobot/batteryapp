import { Component, Input, OnInit } from "@angular/core";
import { HomePage } from "../../pages/home/home";
import { TutoPage } from "../../pages/tuto/tuto";
import { NavController } from "ionic-angular";
import { ApiService } from "../../services/api.service";
import { AlertService } from "../../services/alert.service";
import { PopupService } from "../../services/popup.service";
import { SpeechService } from "../../services/speech.service";
import { ParamService } from "../../services/param.service";
import { ParamPage } from "../../pages/param/param";
import { MailPage } from "../../pages/mail/mail";
import { ToastController } from "ionic-angular";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: "headpage",
  templateUrl: "headpage.html",
})
export class HeadpageComponent implements OnInit {
  homePage = HomePage;
  tutoPage = TutoPage;
  paramPage = ParamPage;
  mailPage = MailPage;
  lowbattery: any;
  cpt_battery: number;
  microon: boolean;
  cpt_open: number;
  update: any;
  textBoutonHeader: string; // bouton 's text (Return / Exit), to the left of the header, depends on the page you are on
  textHeadBand: string;
  volumeState: number; //0 mute,  1 low,  2 hight
  popup_battery: boolean;
  source: any;

  @Input() pageName: string;
  constructor(
    public toastCtrl: ToastController,
    public navCtrl: NavController,
    public param: ParamService,
    public speech: SpeechService,
    public api: ApiService,
    public alert: AlertService,
    public popup: PopupService,
    public sanitizer: DomSanitizer
  ) {
   
    this.cpt_open = 0;
    this.cpt_battery = 0;
    
  }

  ionViewWillLeave() {
    clearInterval(this.update);
    clearInterval(this.lowbattery);
    clearInterval(this.source);
  }

  ngOnDestroy(): void {
    clearInterval(this.update);
    clearInterval(this.lowbattery);
    clearInterval(this.source);
  }

  onTouchWifi(ev) {
    ev.preventDefault();
    if (!this.api.wifiok) {
      this.okToast(this.param.datatext.nointernet);
    } else {
      this.okToast(this.param.datatext.connectedToI);
    }
  }

  updateBattery() {
    if (this.api.statusRobot === 2) {
      // battery logo will not apppear
      this.api.batteryState = 10;
    }
    if (this.api.battery_status.status < 2) {
      //charging
      this.api.batteryState = 4;
    } else if (
      this.api.battery_status.status === 3 ||
      this.api.battery_status.remaining <= this.param.battery.critical
    ) {
      //critical
      this.api.batteryState = 0;
    } else if (this.api.battery_status.remaining > this.param.battery.high) {
      //hight
      this.api.batteryState = 3;
    } else if (this.api.battery_status.remaining > this.param.battery.low) {
      //mean
      this.api.batteryState = 2;
    } else if (this.api.battery_status.remaining <= this.param.battery.low) {
      //low
      this.api.batteryState = 1;
    } else {
      // battery logo will not apppear
      this.api.batteryState = 10;
    }
  }

  ngOnInit() {
    this.popup.onSomethingHappened1(this.accessparam.bind(this));
    // bouton 's text (Return / Exit), to the left of the header, depends on the page you are on
    if (this.pageName === "home") {
      this.update = setInterval(() => this.getUpdate(), 1000); //update hour, battery every 1/2 secondes
      this.lowbattery = setInterval(() => this.monitoringBattery(), 60000);
      this.source = setInterval(() => this.api.jetsonOK(), 2000);
      this.textBoutonHeader = this.param.datatext.quit;
      this.textHeadBand = this.param.datatext.battery;
    } else if (this.pageName === "param") {
      this.textBoutonHeader = this.param.datatext.return;
      this.textHeadBand = this.param.datatext.param;
    } else if (this.pageName === "sms") {
      this.textBoutonHeader = this.param.datatext.return;
      this.textHeadBand = this.param.datatext.receivesms;
    } else if (this.pageName === "mail") {
      this.textBoutonHeader = this.param.datatext.return;
      this.textHeadBand = this.param.datatext.receivemail;
    } else if (this.pageName === "langue") {
      this.textBoutonHeader = this.param.datatext.return;
      this.textHeadBand = this.param.datatext.changelangage;
    } else if(this.pageName==="password"){
      this.textBoutonHeader=this.param.datatext.return;
      this.textHeadBand=this.param.datatext.editpswd;
    }else {
      this.textBoutonHeader = this.param.datatext.return;
      this.textHeadBand = this.param.datatext.tutorial;
    }
  }

  onTouchStatus(ev) {
    ev.preventDefault();
    if (!this.api.towardDocking) {
      //the robot is not mooving

      if (this.api.statusRobot === 2) {
        // if status red
        this.popup.statusRedPresent();
      } else {
        this.okToast(this.param.datatext.statusGreenPresent_message);
      }
    }
  }

  monitoringBattery() {
    if (this.api.batteryState === 0) {
      //critical
      if(!this.api.dockingexist){
        this.popup.lowBattery();
      }
      this.alert.lowBattery(this.api.mailAddInformationBasic());
      this.alert.Battery_c();
      clearInterval(this.lowbattery);
    }
  }

  onTouchBattery(ev) {
    ev.preventDefault();

      if(this.api.batteryState == 0){
        
        this.popup.showToastRed(
          this.param.datatext.lowBattery_title +
            " : " +
            this.api.battery_status.remaining +
            "%",3000,"top"
        );
      }else{
        this.okToast(
        this.param.datatext.battery +
          " : " +
          this.api.battery_status.remaining +
          "%"
      );
      }
     
    
  }

  okToast(m: string) {
    const toast = this.toastCtrl.create({
      message: m,
      duration: 3000,
      position: "middle",
      cssClass: "toastok",
    });
    toast.present();
  }

  getUpdate() {
    //update hour, battery
    this.updateBattery();
    this.updateBand();
    var now = new Date().toLocaleString("fr-FR", {
      hour: "numeric",
      minute: "numeric",
    });
    this.api.hour = now;

    if (this.api.statusRobot === 0 && this.api.batteryState === 10) {
      this.cpt_battery += 1;
    } else {
      this.cpt_battery = 0;
    }

    if (this.cpt_battery > 10) {
      this.api.statusRobot = 2;
    }
  }

  updateBand() {
    if (this.api.towardDocking) {
      this.textHeadBand = this.param.datatext.godocking;
    } else if (this.pageName === "home") {
      if (this.api.docking_status.status === 3 && this.api.mapdata) {
        this.textHeadBand =
          this.param.datatext.charging_remaining +
          this.api.battery_status.remaining +
          this.param.datatext.map +
          this.api.mapdata.Name;
      } else if (this.api.docking_status.status === 3) {
        this.textHeadBand =
          this.param.datatext.charging_remaining +
          this.api.battery_status.remaining +
          this.param.datatext.map +
          "--";
      } else if (this.api.mapdata) {
        if(this.api.batteryState == 0){
          this.textHeadBand =
          this.param.datatext.lowBattery_title +
          this.param.datatext.map +
          this.api.mapdata.Name;
        }else{
          this.textHeadBand =
          this.param.datatext.battery +
          " : " +
          this.api.battery_status.remaining +
          this.param.datatext.map +
          this.api.mapdata.Name;
        }
        
      } else {
        if(this.api.batteryState == 0){
          this.textHeadBand =
          this.param.datatext.lowBattery_title +
          this.param.datatext.map +
          "--";
        }else{
          this.textHeadBand =
          this.param.datatext.battery +
          " : " +
          this.api.battery_status.remaining +
          this.param.datatext.map +
          "--";
        }
      }
    }
  }

  onTouchHelp(ev) {
    ev.preventDefault();
    if (!this.api.towardDocking) {
      //robot is not moving
      if (!(this.pageName === "tuto")) {
        this.alert.displayTuto(this.api.mailAddInformationBasic());
        this.navCtrl.push(this.tutoPage);
      }
    }
  }

  accessparam(){
    this.navCtrl.push(this.paramPage);
  }
  
  onTouchParam(ev) {
    ev.preventDefault();
    if (!this.api.towardDocking) {
      //robot is not moving
      if (
        this.pageName === "param" ||
        this.pageName === "sms" ||
        this.pageName === "mail" ||
        this.pageName === "langue"||
        this.pageName === "password"
      ) {
        console.log("already");
      } else {
         
          this.popup.askpswd();
      }
    }
  }

  onquit(ev){
    ev.preventDefault();
    this.clicOnMenu();
  }
  
  clicOnMenu() {
    if (this.pageName === "tuto" || this.pageName === "param") {
      //return to home page
      this.navCtrl.popToRoot();
    } else if (
      this.pageName === "sms" ||
      this.pageName === "mail" ||
      this.pageName === "langue"||
      this.pageName==="password"
    ) {
      this.navCtrl.pop();
    } else {
      if (this.api.towardDocking) {
        this.api.abortNavHttp();
        this.api.towardDocking = false;
        this.popup.quitConfirm();
      } else {
        this.api.close_app = true;
        this.alert.appClosed(this.api.mailAddInformationBasic());
        //stop the round and quit the app
        this.api.abortHttp();
        setTimeout(() => {
          window.close();
        }, 1000);
      }
    }
  }
}
