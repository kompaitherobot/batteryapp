// Allows Http communication with komnav
// cf Marc komnav_http_manual for more information
// https://drive.google.com/drive/u/3/folders/1-18kyKv6Ep-fzPU1F-dp0tQ1r8usyuZ7

import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable, OnInit } from "@angular/core";
import { App } from "ionic-angular";
import { AlertService } from "./alert.service";
import { ParamService } from "./param.service";
declare var ROSLIB: any;

export class Battery {
  autonomy: number; //the estimated remaining operation time, in minutes.
  current: number; //the actual current drawn from the battery, in Amps.
  remaining: number; //the percentage of energy remaining in the battery, range 0 - 100 %.
  status: number; // Charging 0 Chrged 1 Ok 2 Critical 3
  timestamp: number;
  voltage: number; // the actual battery voltage in Volts.
}

export class Statistics {
  timestamp: number;
  totalTime: number;
  totalDistance: number;
}

export class Anticollision {
  timestamp: number;
  enabled: boolean;
  locked: boolean;
  forward: number; // 0 ok , 1 speed low , 2 obstacle
  reverse: number;
  left: number;
  right: number;
}

export class Iostate {
  // receive robot's boutons input
  timestamp: number;
  dIn: any;
  aIn: any;
}

export class Docking {
  status: number;
  detected: boolean;
}
/*
{ Docking status
  "Unknown    ",
  "Undocked   ",
  "Docking    ",
  "Undocking  ",
  "Error      "
};*/

export class Round {
  round: any;
  acknowledge: boolean; // send to the next poi
  abort: boolean; // stop round
  pause: boolean; // pause the round
  status: number; //0none   1ready to be executed  2moving  3paused  4onPOI  5Completed
}

export class Differential {
  status: number;
  TargetLinearSpeed: number;
  TargetAngularSpeed: number;
}

export class Localization {
  positionx: number;
  positiony: number;
  positiont: number;
}

export class Navigation {
  avoided: number;
  status: number;
}

// The status can have the following values:
// • 0 - Waiting: the robot is ready for a new destination speciﬁed with a PUT /navigation/destination request.
// • 1 - Following: the robot is following the trajectory computed to join the destination.
// • 2 - Aiming: the robot has completed the trajectory and is now rotating to aim the destination accurately
// • 3 - Translating: the robot has ﬁnished aiming and is now translating to reach the destination
// • 4 - Rotating: this is the last rotation, executed only if the destination was speciﬁed with “Rotate” : true
// • 5-Error: indicatesthattherobothasencounteredanerrorthatpreventedittojointherequireddestination.
// This can be cleared by sending a new destination.

@Injectable()
export class ApiService implements OnInit {
  batteryremaining: number;
  battery_status: Battery;
  battery_socket: WebSocket;

  navigation_status: Navigation;
  navigation_socket: WebSocket;

  round_status: Round;
  round_socket: WebSocket;

  anticollision_status: Anticollision;
  anticollision_socket: WebSocket;

  statistics_socket: WebSocket;
  statistics_status: Statistics;

  io_socket: WebSocket;
  io_status: Iostate;

  docking_socket: WebSocket;
  docking_status: Docking;

  differential_status: Differential;
  differential_socket: WebSocket;

  localization_status: Localization;
  localization_socket: WebSocket;

  textHeadBand: string;
  hour: any;
  appOpened: boolean;
  hourStart: Date;
  start: boolean;
  b_pressed: boolean;
  btnPush: boolean;
  towardDocking: boolean;
  towardPOI: boolean;
  close_app: boolean;
  is_connected: boolean;
  is_background: boolean;
  is_localized: boolean;
  roundActive: boolean;
  cpt_jetsonOK: number;
  wifiok: boolean;
  statusRobot: number; //0 ok,  1 jetson without internet,  2 no connexion with jetson
  //mapLoaded:boolean;
  fct_onGo: boolean;
  fct_startRound: boolean;
  batteryState: number;
  locationdata: any;
  QRselected: any;
  firstlocation: any;
  id_current_map: number;
  mapdata: any;
  robotok: boolean;
  dockingdata: any;
  docking: any;
  dockingexist: boolean;
  socketok: boolean = false;

  httpskomnav: string;
  wsskomnav: string;
  httpsros: string;
  wssros: string;

  ros: any;
  listener_qr: any;
  listener_camera: any;

  constructor(
    private httpClient: HttpClient,
    public app: App,
    public alert: AlertService,
    public param: ParamService
  ) {
    this.is_connected = false; // to know if we are connected to the robot
    this.is_background = false;
    //this.mapLoaded=false;
    this.is_localized = false;
    this.roundActive = false; // to know if the round is in process
    this.towardDocking = false; //to know if the robot is moving toward the docking
    this.towardPOI; //to know if th robot is moving toward a poi for the sentinel mode
    this.start = false;
    this.fct_startRound = false;
    this.fct_onGo = false;
    this.statusRobot = 0;
    this.cpt_jetsonOK = 0;
    this.close_app = false;
    this.appOpened = false;
    this.textHeadBand = "";
    this.robotok = false;
  }

  ngOnInit() { }



  instanciate() {
    this.socketok = true;
    var apiserv = this;
    ////////////////////// localization socket

    this.localization_status = new Localization();

    this.localization_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/localization/socket",
      "json"
    );

    this.localization_socket.onerror = function (event) {
      console.log("error localization socket");
    };

    this.localization_socket.onopen = function (event) {
      console.log("localization socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.localization_status.positionx = test["Pose"]["X"];
        apiserv.localization_status.positiony = test["Pose"]["Y"];
        apiserv.localization_status.positiont = test["Pose"]["T"];
      };

      this.onclose = function () {
        console.log("localization socket closed");
      };
    };

    ////////////////////// docking socket

    this.docking_status = new Docking();

    this.docking_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/docking/socket",
      "json"
    );

    this.docking_socket.onerror = function (event) {
      console.log("error docking socket");
    };

    this.docking_socket.onopen = function (event) {
      console.log("docking socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.docking_status.status = test["Status"];
        apiserv.docking_status.detected = test["Detected"];
      };

      this.onclose = function () {
        console.log("docking socket closed");
      };
    };

    ////////////////////// differential socket

    this.differential_status = new Differential();

    this.differential_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/differential/socket",
      "json"
    );

    this.differential_socket.onerror = function (event) {
      console.log("error differential socket");
    };

    this.differential_socket.onopen = function (event) {
      console.log("differential socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.differential_status.status = test["Status"];
        apiserv.differential_status.TargetLinearSpeed = test["TargetLinearSpeed"];
        apiserv.differential_status.TargetAngularSpeed = test["TargetAngularSpeed"];
      };

      this.onclose = function () {
        console.log("differential socket closed");
      };
    };

    ////////////////////// navigation socket

    this.navigation_status = new Navigation();

    this.navigation_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/navigation/socket",
      "json"
    );

    this.navigation_socket.onerror = function (event) {
      console.log("error navigation socket");
    };

    this.navigation_socket.onopen = function (event) {
      console.log("navigation socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.navigation_status.status = test["Status"];
        apiserv.navigation_status.avoided = test["Avoided"];
      };

      this.onclose = function () {
        console.log("navigation socket closed");
      };
    };

    ////////////////////// anticollision socket

    this.anticollision_status = new Anticollision();

    this.anticollision_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/anticollision/socket",
      "json"
    );

    this.anticollision_socket.onerror = function (event) {
      console.log("error anticollision socket");
    };

    this.anticollision_socket.onopen = function (event) {
      console.log("anticollision socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.anticollision_status.timestamp = test["Timestamp"];
        apiserv.anticollision_status.enabled = test["Enabled"];
        apiserv.anticollision_status.locked = test["Locked"];
        apiserv.anticollision_status.forward = parseInt(test["Forward"]);
        apiserv.anticollision_status.right = parseInt(test["Right"]);
        apiserv.anticollision_status.left = parseInt(test["Left"]);
        apiserv.anticollision_status.reverse = parseInt(test["Reverse"]);
      };

      this.onclose = function () {
        console.log("anticollision socket closed");
      };
    };

    ////////////////////// statistics socket

    this.statistics_status = new Statistics();

    this.statistics_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/statistics/socket",
      "json"
    );

    this.statistics_socket.onerror = function (event) {
      console.log("error statistic socket");
    };

    this.statistics_socket.onopen = function (event) {
      console.log("statistic socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.statistics_status.totalTime = test["TotalTime"];
        apiserv.statistics_status.totalDistance = test["TotalDistance"];
        apiserv.statistics_status.timestamp = test["Timestamp"];
      };

      this.onclose = function () {
        console.log("statistic socket closed");
      };
    };

    ///////////////////////// battery socket
    this.battery_status = new Battery();

    this.battery_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/battery/socket",
      "json"
    );

    this.battery_socket.onerror = function (event) {
      console.log("error battery socket");
    };

    this.battery_socket.onopen = function (event) {
      console.log("battery socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.battery_status.autonomy = test["Autonomy"];
        apiserv.battery_status.current = test["Current"];
        apiserv.battery_status.remaining = test["Remaining"];
        apiserv.battery_status.status = test["Status"];
        apiserv.battery_status.timestamp = test["Timestamp"];
        apiserv.battery_status.voltage = test["Voltage"];
      };

      this.onclose = function () {
        console.log("battery socket closed");
      };
    };

    //////////////////////// round socket
    this.round_status = new Round();

    this.round_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/rounds/socket",
      "json"
    );

    this.round_socket.onerror = function (event) {
      console.log("error round socket");
    };

    this.round_socket.onopen = function (event) {
      console.log("round socket opened");

      this.onmessage = function (event) {
        var test = JSON.parse(event.data);
        apiserv.round_status.round = test["Round"];
        apiserv.round_status.acknowledge = test["Acknowledge"];
        apiserv.round_status.abort = test["Abort"];
        apiserv.round_status.pause = test["Pause"];
        apiserv.round_status.status = test["Status"];
      };

      this.onclose = function () {
        console.log("round socket closed");
      };
    };

    //////////////////////////////// io socket
    this.b_pressed = false;
    this.btnPush = false;
    this.io_status = new Iostate();
    this.io_socket = new WebSocket(
      this.wsskomnav + this.param.localhost + "/api/io/socket",
      "json"
    );
    this.io_socket.onerror = function (event) {
      console.log("error iosocket");
    };

    this.io_socket.onopen = function (event) {
      console.log("io socket opened");

      this.onclose = function () {
        console.log("io socket closed");
      };
    };

    this.io_socket.onmessage = function (event) {
      var test = JSON.parse(event.data);
      apiserv.io_status.timestamp = test["Timestamp"];
      apiserv.io_status.dIn = test["DIn"];
      apiserv.io_status.aIn = test["AIn"];

      if (
        apiserv.io_status.dIn[2] ||
        apiserv.io_status.dIn[3] ||
        apiserv.io_status.dIn[8]
      ) {
        //smart button and button A of the game pad
        if (!apiserv.b_pressed) {
          apiserv.btnPush = true;
          apiserv.b_pressed = true;
        }
      } else {
        if (apiserv.b_pressed) {
          apiserv.b_pressed = false;
        }
      }
    };



    //////////////////////////////// ROS TOPIC connection
    if(this.param.robot.cam_USB==0){

    
    this.ros = new ROSLIB.Ros({
      url: 'ws://' + this.param.robot.rosip
    });

    this.ros.on('connection', function () {
      console.log('Connected to ros websocket server.');
    });

    this.ros.on('error', function (error) {
      console.log('Error connecting to ros websocket server: ', error);
    });

    this.ros.on('close', function () {
      console.log('Connection to ros websocket server closed.');
    });

    //////////////////////////////// ROS TOPIC qrcode
    this.listener_qr = new ROSLIB.Topic({
      ros: this.ros,
      name: '/barcode',
      messageType: 'std_msgs/String'
    });
    //////////////////////////////// ROS TOPIC cam
    this.listener_camera = new ROSLIB.Topic({
      ros: this.ros,
      name: '/top_webcam/image_raw/compressed',
      //name : '/fisheye_cam/image_raw',
      //name : '/D435_camera_FWD/color/image_raw',
      //name : '/fisheye_cam/compressed',

      messageType: 'sensor_msgs/CompressedImage'
    });
  }

  }


  mailAddInformationBasic() {
    var now = new Date().toLocaleString("en-GB", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
      hour: "numeric",
      minute: "numeric",
      second: "numeric",
    });
    return (
      "<br> Time : " +
      now +
      "<br> Serial number : " +
      this.param.serialnumber +
      "<br> Battery remaining: " +
      this.battery_status.remaining +
      "%" +
      "<br> Battery state : " +
      this.battery_status.status +
      "<br> Battery voltage : " +
      this.battery_status.voltage +
      "<br> Battery current : " +
      this.battery_status.current +
      "<br> Battery autonomy : " +
      this.battery_status.autonomy +
      "<br> Docking state : " +
      this.docking_status.status +
      "<br> Status Forward : " +
      this.anticollision_status.forward +
      "<br> Status Right : " +
      this.anticollision_status.right +
      "<br> Status Left : " +
      this.anticollision_status.left +
      "<br> Status Reverse : " +
      this.anticollision_status.reverse +
      "<br> Navigation state : " +
      this.navigation_status.status +
      "<br> Differential state : " +
      this.differential_status.status +
      "<br> Odometer : " +
      this.statistics_status.totalDistance +
      "<br> Position X : " +
      this.localization_status.positionx +
      "<br> Position Y : " +
      this.localization_status.positiony +
      "<br> Position T : " +
      this.localization_status.positiont +
      "<br> Application : 4" +
      "<br>"
    );
  }

  checkrobot() {
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/battery/state")
      .subscribe(
        (data) => {
          this.robotok = true;
        },
        (err) => {
          console.log(err);
          this.robotok = false;
        }
      );
  }
  jetsonOK() {
    this.checkInternet();
    this.alert.checkwifi(this.wifiok);
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/battery/state", {
        observe: "response",
      })
 
      .subscribe(
        (resp) => {
          if (this.statusRobot === 2) {
            this.alert.appError(this.mailAddInformationBasic());
            document.location.reload(); //if error then refresh the page and relaunch websocket
          }
          if (resp.status === 200) {
            this.cpt_jetsonOK = 0;
            this.statusRobot = 0; // everything is ok
            this.connect();
          } else {
            this.cpt_jetsonOK += 1;
            if (this.cpt_jetsonOK > 5) {
              this.statusRobot = 2; //no connection
              this.connectionLost();
            }
          }
        },
        (err) => {
          console.log(err); //no conection
          this.cpt_jetsonOK += 1;
          if (this.cpt_jetsonOK > 10) {
            this.statusRobot = 2;
            this.connectionLost();
          }
        }
      );
  }

  pauseHttp() {
    // suspend the round

    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/rounds/current/pause", {
        observe: "response",
      })
    
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("pauseOK");
        },
        (err) => {
          console.log(err);
          console.log("pausePBM");
        }
      );
  }

  async isConnectedInternet(): Promise<boolean> {
    try {
      const response = await fetch('http://localhost/ionicDB/internetping.php');
      const text = await response.text(); // Récupère le contenu texte de la réponse
      //console.log(text);
      
      // Analyse du texte pour déterminer la connectivité Internet
      return text.trim() === 'true'; // Renvoie true si le texte est 'true', sinon false
      
    } catch (error) {
      console.error('Error checking internet connectivity:', error);
      return false;
    }
  }

  checkInternet() {
    this.isConnectedInternet().then(connecte => {
      if (connecte) {
        //console.log("L'ordinateur est connecté à Internet");
        this.wifiok = true;
      } else {
        //console.log("L'ordinateur n'est pas connecté à Internet");
        this.wifiok = false;
      }
    });

  }

  connectHttp() {
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/docking/connect", {
        observe: "response",
      })
      
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("connectOK");
        },
        (err) => {
          console.log(err);
          console.log("connectPBM");
        }
      );
  }

  disconnectHttp() {
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/docking/disconnect", {
        observe: "response",
      })
     
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("disconnectOK");
        },
        (err) => {
          console.log(err);
          console.log("disconnectPBM");
        }
      );
  }

  abortDockingHttp() {
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/docking/abort", {
        observe: "response",
      })

      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("abortdockingOK");
        },
        (err) => {
          console.log(err);
          console.log("abortdockingPBM");
        }
      );
  }

  getCurrentMap() {
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/maps/current/properties")
      .subscribe(
        (data) => {
          console.log(data);
          this.mapdata = data;
          //console.log(this.mapdata.Id);
          if (this.mapdata) {
            this.id_current_map = this.mapdata.Id;
          }
        },
        (err) => {
          console.log(err);
        }
      );
  }

  changeMapbyIdHttp() {
    this.httpClient
      .put(
        this.httpskomnav +
        this.param.localhost +
        "/api/maps/current/id/" +
        this.QRselected.id_map,
        { observe: "response" }
      )
  
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("changeMapOK");
        },
        (err) => {
          console.log(err);
          console.log("changeMapPBM");
        }
      );
  }

  resetLocationHttp() {
    var pose = {
      X: this.firstlocation[0].Pose.X,
      Y: this.firstlocation[0].Pose.Y,
      T: this.firstlocation[0].Pose.T,
    };

    let body = JSON.stringify(pose);
    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    let options = { headers: headers };
    return new Promise((resolve) => {
      this.httpClient
        .put(
          this.httpskomnav + this.param.localhost + "/api/localization/pose",
          body,
          options
        )
        .subscribe(
          (resp) => {
            console.log(resp);
            console.log("resetlocationOK");
          },
          (error) => {
            console.log(error);
            console.log("resetlocationPBM");
          }
        );
    });
  }

  getFirstLocationHttp() {
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/maps/current/locations")
      .subscribe(
        (data) => {
          console.log(data);
          this.locationdata = data;
          if (this.locationdata) {
            this.firstlocation = this.locationdata.filter(
              (x) => x.Id == this.QRselected.id_poi
            );
          }
        },
        (err) => {
          console.log(err);
        }
      );
  }



  goDockingHttp() {
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/navigation/dock", {
        observe: "response",
      })
    
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("godockingOK");
        },
        (err) => {
          console.log(err);
          console.log("godockingPBM");
        }
      );
  }

  reachHttp(poiname: string) {
    this.httpClient
      .put(
        this.httpskomnav +
        this.param.localhost +
        "/api/navigation/destination/name/" +
        poiname,
        { observe: "response" }
      )

      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("reachOK");
        },
        (err) => {
          console.log(err);
          console.log("reachPBM");
        }
      );
  }

  abortHttp() {
    // stop the round
    this.start = false;
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/rounds/current/abort", {
        observe: "response",
      })
    
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("abortOK");
        },
        (err) => {
          console.log(err);
          console.log("abortPBM");
        }
      );
  }

  abortNavHttp() {
    // stop the navigation
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/navigation/abort", {
        observe: "response",
      })

      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("abortNavOK");
        },
        (err) => {
          console.log(err);
          console.log("abortNavPBM");
        }
      );
  }

  resumeHttp() {
    // resume round

    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/rounds/current/resume", {
        observe: "response",
      })

      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("resumeOK");
        },
        (err) => {
          console.log(err);
          console.log("resumePBM");
        }
      );
  }

  acknowledgeHttp() {
    //go to next poi

    this.httpClient
      .put(
        this.httpskomnav + this.param.localhost + "/api/rounds/current/acknowledge",
        { observe: "response" }
      )
  
      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("acknowledgeOK");
        },
        (err) => {
          console.log(err);
          console.log("acknowledgePBM");
        }
      );
  }

  startRoundHttp(idRound: number) {
    // start the round

    this.httpClient
      .put(
        this.httpskomnav + this.param.localhost + "/api/rounds/current/id/" + idRound,
        { observe: "response" }
      )

      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("startRoundOK");
        },
        (err) => {
          console.log(err);
          console.log("startRoundPBM");
        }
      );
  }

  connect() {
    this.is_connected = true;
  }

  connectionLost() {
    this.is_connected = false;
  }

  background() {
    this.is_background = true;
  }

  foreground() {
    this.is_background = false;
  }


  eyesHttp(id: number) {
    this.httpClient
      .put(this.httpskomnav + this.param.localhost + "/api/eyes?id=" + id, {
        observe: "response",
      })

      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("eyesOK");
        },
        (err) => {
          console.log(err);
          console.log("eyesPBM");
        }
      );
  }


  deleteEyesHttp(id: number) {
    this.httpClient
      .delete(this.httpskomnav + this.param.localhost + "/api/eyes?id=" + id, {
        observe: "response",
      })

      .subscribe(
        (resp) => {
          console.log(resp);
          console.log("eyesOK");
        },
        (err) => {
          console.log(err);
          console.log("eyesPBM");
        }
      );
  }

  poiDocking() {
    this.httpClient
      .get(this.httpskomnav + this.param.localhost + "/api/maps/current/locations")
      .subscribe(
        (data) => {
          this.dockingdata = data;
          console.log(this.dockingdata.filter((x) => x.Name == "docking"));
          if (this.dockingdata.filter((x) => x.Name == "docking").length > 0) {
            console.log("docking exist");
            this.dockingexist = true;
            this.docking = this.dockingdata.filter((x) => x.Name == "docking")[0];
          } else {
            this.dockingexist = false;
          }
        },
        (err) => {
          console.log(err);
        }
      );
  }


  joystickHttp(lin, rad) {
    var cmd = { Enable: true, TargetLinearSpeed: lin, TargetAngularSpeed: rad };
    //console.log(lin, rad);
    let body = JSON.stringify(cmd);
    let headers = new HttpHeaders({ "Content-Type": "application/json" });
    let options = { headers: headers };
    return new Promise((resolve) => {
      this.httpClient
        .put(
          this.httpskomnav + this.param.localhost + "/api/differential/command",
          body,
          options
        )
        .subscribe(
          (resp) => {
            //console.log(resp);
            console.log("joystickOK");
          },
          (error) => {
            console.log(error);
            console.log("joystickPBM");
          }
        );
    });
  }

  startQrDetection(start: boolean) {
    var serviceQrDetection = new ROSLIB.Service({
      ros: this.ros,
      name: '/toggle_barcode_detection_node',
      serviceType: 'std_srvs/SetBool'
    });

    var request = new ROSLIB.ServiceRequest({
      data: start,

    });

    serviceQrDetection.callService(request, function (result) {
      console.log("*********service qr****************")
      console.log(result);
    });

    if (!start) {
      this.listener_qr.removeAllListeners();
    }
  }

}
