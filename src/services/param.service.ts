// the parameter script

import frtext from "../langage/fr-FR.json";
import entext from "../langage/en-GB.json";
import estext from "../langage/es-ES.json";
import detext from "../langage/de-DE.json";
import grtext from "../langage/el-GR.json";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { DomSanitizer } from "@angular/platform-browser";

@Injectable()
export class ParamService {
  localhost: string;
  robotmail: string;
  datamail: string;
  langage: string;
  phonenumberlist: string[];
  maillist: string[];
  datamaillist: string[];
  serialnumber: string;
  mailrobotpassw: string;
  maildatapassw: string;
  allowspeech: boolean;
  name: string;
  datatext: any;
  robot: any;
  battery:any;
  duration: any;
  tableduration: any = {};
  tablerobot: any = {};
  mail: any = {};
  phone: any = {};
  date: any;
  currentduration: any = {};
  cpt: number;
  olddata: any = {};
  sendduration: boolean;
  qrcodes: any;
  source: any;
  durationNS:any={};
  tabledurationNS: any = {};

  constructor(private httpClient: HttpClient, public sanitizer: DomSanitizer) {
    this.maillist = [];
    this.phonenumberlist = [];
    this.cpt = 0;
    this.sendduration = false;
  }

  updateDurationNS(id:number){
    

    this.tabledurationNS.action="update";
    this.tabledurationNS.id_duration=id;
    this.httpClient
      .post(
        "http://localhost/ionicDB/duration/updateduration.php",
        JSON.stringify( this.tabledurationNS)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getBattery(){
    this.httpClient.get("http://localhost/ionicDB/getbattery.php").subscribe(
      (data) => {
        this.battery = data[0];
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getDurationNS(){
    this.httpClient.get("http://localhost/ionicDB/duration/getduration.php").subscribe(
      (data) => {
        
        this.duration=data;
        this.durationNS = data;
        //console.log(data);
        //console.log(this.durationNS.length);
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getDataRobot() {
    this.httpClient.get("http://localhost/ionicDB/getrobot.php").subscribe(
      (data) => {
        //console.log("robot:"+data);
        this.robot = data[0];
        this.langage = this.robot.langage;
        if (this.langage === "fr-FR") {
          this.datatext = frtext;
        } else if (this.langage === "en-GB") {
          this.datatext = entext;
        } else if (this.langage === "es-ES") {
          this.datatext = estext;
        }else if(this.langage === "el-GR"){
          this.datatext = grtext;
        }else if (this.langage === "de-DE") {
          this.datatext = detext;
        }
        this.source = this.sanitizer.bypassSecurityTrustResourceUrl(this.datatext.URL_battery);
      },
      (err) => {
        console.log(err);
      }
    );
  }

  updateRobot() {
    this.tablerobot.action = "update";
    this.tablerobot.langage = this.robot.langage;
    this.tablerobot.serialnumber = this.serialnumber;
    this.tablerobot.name = this.name;
    this.tablerobot.send_pic = this.robot.send_pic;
    this.tablerobot.password = this.robot.password;
    this.httpClient
      .post(
        "http://localhost/ionicDB/updaterobotpassword.php",
        JSON.stringify(this.tablerobot)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getQR() {
    this.httpClient.get("http://localhost/ionicDB/getqrcodes.php").subscribe(
      (data) => {
        this.qrcodes = data;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getPhone() {
    this.httpClient.get("http://localhost/ionicDB/getphone.php").subscribe(
      (data) => {
        if (this.phonenumberlist.length === 0) {
          for (let value in data) {
            this.phonenumberlist.push(data[value].phonenumber);
          }
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  addMail(m: string) {
    this.mail.action = "insert";
    this.mail.mail = m;
    this.mail.serialnumber = this.serialnumber;
    this.httpClient
      .post("http://localhost/ionicDB/addmail.php", JSON.stringify(this.mail))
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  deleteMail(m: string) {
    this.mail.action = "delete";
    this.mail.mail = m;
    this.httpClient
      .post(
        "http://localhost/ionicDB/deletemail.php",
        JSON.stringify(this.mail)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  addPhone(m: string) {
    this.phone.action = "insert";
    this.phone.phonenumber = m;
    this.phone.serialnumber = this.serialnumber;
    this.httpClient
      .post("http://localhost/ionicDB/addphone.php", JSON.stringify(this.phone))
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  deletePhone(m: string) {
    this.phone.action = "delete";
    this.phone.phonenumber = m;
    this.httpClient
      .post(
        "http://localhost/ionicDB/deletephone.php",
        JSON.stringify(this.phone)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getMail() {
    this.httpClient.get("http://localhost/ionicDB/getmail.php").subscribe(
      (data) => {
        if (this.maillist.length === 0) {
          for (let value in data) {
            this.maillist.push(data[value].mail);
          }
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  addDuration() {
    this.tableduration.action = "insert";
    this.tableduration.serialnumber = this.serialnumber;
    this.tableduration.round = this.currentduration.round;
    this.tableduration.battery = this.currentduration.battery;
    this.tableduration.walk = this.currentduration.walk;
    this.tableduration.patrol = this.currentduration.patrol;
    this.tableduration.date = this.currentduration.date;
    this.httpClient
      .post(
        "http://localhost/ionicDB/addduration.php",
        JSON.stringify(this.tableduration)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }



  updateDuration() {
    this.tableduration.action = "update";
    this.tableduration.battery = this.currentduration.battery;
    this.tableduration.date = this.currentduration.date;
    this.httpClient
      .post(
        "http://localhost/ionicDB/duration/updatedurationbattery.php",
        JSON.stringify(this.tableduration)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  fillData() {
    this.name = this.robot.name;
    this.allowspeech = this.robot.allowspeech;
    this.maildatapassw = atob(this.robot.maildatapass);
    this.mailrobotpassw = atob(this.robot.mailrobotpass);
    this.localhost = this.robot.localhost;
    console.log(this.localhost);
    this.serialnumber = this.robot.serialnumber;
    this.robotmail = this.robot.mailrobot;
    this.datamail = this.robot.maildata;
    this.datamaillist = ["data@kompai.com"];
    if(this.duration.length>0){
    if (
      this.duration[this.duration.length - 1].date ===
      new Date().toLocaleDateString("fr-CA")
    ) {
      this.currentduration.date = this.duration[this.duration.length - 1].date;
      this.currentduration.round = this.duration[
        this.duration.length - 1
      ].round;
      this.currentduration.battery = this.duration[
        this.duration.length - 1
      ].battery;
      this.currentduration.patrol = this.duration[
        this.duration.length - 1
      ].patrol;
      this.currentduration.walk = this.duration[this.duration.length - 1].walk;
      this.currentduration.toolbox = this.duration[this.duration.length - 1].toolbox;
      this.currentduration.logistic = this.duration[this.duration.length - 1].logistic;
    } else {
      if(!this.sendduration){
        this.sendduration = true;
        this.init_currentduration();
        this.addDuration();
      }
    }}else{
      if(!this.sendduration){
        this.sendduration = true;
        this.init_currentduration();
        this.addDuration();
      }

    }
  }

  cptDuration() {
    if (this.currentduration.date === new Date().toLocaleDateString("fr-CA")) {
      this.cpt = parseInt(this.currentduration.battery);
      this.currentduration.battery = this.cpt + 2;
      this.updateDuration();
    } else {
      this.init_currentduration();
      this.addDuration();
    }
  }


  init_currentduration(){
    this.currentduration.round = 0;
    this.currentduration.battery = 0;
    this.currentduration.patrol = 0;
    this.currentduration.walk = 0;
    this.currentduration.toolbox = 0;
    this.currentduration.logistic = 0;
    this.currentduration.date = new Date().toLocaleDateString("fr-CA");
  }
}
