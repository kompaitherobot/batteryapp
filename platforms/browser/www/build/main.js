webpackJsonp([1],{

/***/ 115:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_api_service__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_alert_service__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_popup_service__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_param_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_speech_service__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_nipplejs__ = __webpack_require__(318);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_nipplejs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_nipplejs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_jsqr__ = __webpack_require__(319);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_jsqr___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_jsqr__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, param, toastCtrl, speech, popup, loadingCtrl, api, alert, renderer) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.param = param;
        this.toastCtrl = toastCtrl;
        this.speech = speech;
        this.popup = popup;
        this.loadingCtrl = loadingCtrl;
        this.api = api;
        this.alert = alert;
        this.renderer = renderer;
        this.size = 70;
        this.maxrad = 0.08;
        this.maxlin = 0.12;
        this.maxback = 0.08;
        this.cptnospeed = 0;
        this.wait = false;
        this.videoWidth = 0;
        this.videoHeight = 0;
        this.constraints = {
            //audio: false,
            //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
            video: true,
            facingMode: "environment",
            width: { ideal: 1920 },
            height: { ideal: 1080 },
        };
        this.start_from_docking = false;
        this.cpt_locked = 0;
        this.sum_blocked = 0;
        this.api.QRselected = undefined;
        this.time_blocked = 0;
        this.selectOptions = {
            title: this.param.datatext.qrcode,
        };
        // declenche la popup de permission camera si besoin
        this.setupconstraint();
        this.api.poiDocking();
        this.update = setInterval(function () { return _this.getUpdate(); }, 500); // update the status and the trajectory every 1/2 secondes
        this.cptduration = setInterval(function () { return _this.mail(); }, 120000); // toutes les 2 min actualise time
    }
    HomePage.prototype.ngOnInit = function () {
        this.speech.getVoice();
    };
    HomePage.prototype.mail = function () {
        this.param.cptDuration(); //update duration
        if (this.param.durationNS.length > 1) {
            if (this.api.wifiok) {
                //this.param.updateDurationNS(this.param.durationNS[0].id_duration);
                this.alert.duration(this.param.durationNS[0], this.api.mailAddInformationBasic());
                //this.param.getDurationNS();
            }
        }
    };
    HomePage.prototype.dismissLoading = function () {
        if (this.loading) {
            this.loading.dismiss();
            this.loading = null;
        }
    };
    HomePage.prototype.okToast = function (m, n) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: n,
            position: "middle",
            cssClass: "toastam",
        });
        toast.present();
    };
    HomePage.prototype.scan = function (ev) {
        var _this = this;
        ev.preventDefault();
        ///si cam usb
        if (this.param.robot.cam_USB == 1) {
            console.log("enter scan usb");
            try {
                this.videoCanvas.nativeElement.height = this.videoElement.nativeElement.videoHeight;
                this.videoCanvas.nativeElement.width = this.videoElement.nativeElement.videoWidth;
                var g = this.videoCanvas.nativeElement.getContext("2d");
                g.drawImage(this.videoElement.nativeElement, 0, 0, this.videoCanvas.nativeElement.width, this.videoCanvas.nativeElement.height);
                var imageData = g.getImageData(0, 0, this.videoCanvas.nativeElement.width, this.videoCanvas.nativeElement.height);
                var code_1 = __WEBPACK_IMPORTED_MODULE_8_jsqr___default()(imageData.data, imageData.width, imageData.height, {
                    inversionAttempts: "dontInvert",
                });
                if (code_1) {
                    console.log(code_1.data.trim());
                    if (code_1.data !== null &&
                        this.param.qrcodes.filter(function (x) { return x.qr == code_1.data.trim(); })
                            .length > 0) {
                        this.api.QRselected = this.param.qrcodes.filter(function (x) { return x.qr == code_1.data.trim(); })[0];
                        this.onReloc();
                    }
                    else if (code_1.data !== null &&
                        this.param.qrcodes.filter(function (x) { return x.qr == code_1.data.trim(); })
                            .length === 0) {
                        this.okToast(this.param.datatext.unknownqrcode, 4000);
                    }
                }
                else {
                    this.popup.showToastRed(this.param.datatext.noqrdetected, 5000, "middle");
                    this.speech.speak(this.param.datatext.noqrdetected);
                }
            }
            catch (err) {
                console.log("Error", err);
            }
            /////si cam ros
        }
        else {
            console.log("scan qr ros");
            this.qrdetection = true;
            this.api.startQrDetection(true);
            setTimeout(function () {
                if (_this.qrdetection) {
                    _this.qrdetection = false;
                    _this.api.startQrDetection(false);
                    _this.popup.showToastRed(_this.param.datatext.noqrdetected, 5000, "middle");
                    _this.speech.speak(_this.param.datatext.noqrdetected);
                }
            }, 5000);
            this.api.listener_qr.subscribe(function (message) {
                console.log('Received message on ' + _this.api.listener_qr.name + ': ' + message.data);
                if (_this.qrdetection) {
                    _this.qrdetection = false;
                    _this.api.startQrDetection(false);
                    if (message.data !== null &&
                        _this.param.qrcodes.filter(function (x) { return x.qr == message.data.trim(); })
                            .length > 0) {
                        _this.api.QRselected = _this.param.qrcodes.filter(function (x) { return x.qr == message.data.trim(); })[0];
                        _this.onReloc();
                    }
                    else if (message.data !== null &&
                        _this.param.qrcodes.filter(function (x) { return x.qr == message.data.trim(); })
                            .length === 0) {
                        _this.popup.showToastRed(_this.param.datatext.unknownqrcode, 4000, "middle");
                        _this.speech.speak(_this.param.datatext.unknownqrcode);
                    }
                }
            });
        }
    };
    HomePage.prototype.showLoading = function () {
        if (!this.loading) {
            this.loading = this.loadingCtrl.create({
                content: this.param.datatext.relocalizing,
            });
            this.loading.present();
        }
    };
    HomePage.prototype.onQRChange = function (event) {
        this.api.QRselected = this.param.qrcodes.filter(function (x) { return x.qr == event; })[0];
        this.onReloc();
    };
    HomePage.prototype.ionViewWillLeave = function () {
        if (this.param.robot.cam_USB == 0) {
            this.api.listener_camera.removeAllListeners();
        }
    };
    HomePage.prototype.convertDataURIToBinary2 = function (dataURI) {
        var raw = window.atob(dataURI);
        var rawLength = raw.length;
        var array = new Uint8Array(new ArrayBuffer(rawLength));
        for (var i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array;
    };
    HomePage.prototype.ionViewDidEnter = function () {
        var _this = this;
        // CAMERA ROS
        if (this.param.robot.cam_USB == 0) {
            this.api.listener_camera.subscribe(function (message) {
                //console.log('Received message on ' + listener_compressed.name + ': ');
                _this.imgRos = document.getElementById('imgcompressed');
                //console.log(message);
                var bufferdata = _this.convertDataURIToBinary2(message.data);
                var blob = new Blob([bufferdata], { type: 'image/jpeg' });
                var blobUrl = URL.createObjectURL(blob);
                _this.imgRos.src = blobUrl;
            });
        }
    };
    HomePage.prototype.btnPressed = function () {
        // stop the robot if bouton pressed
        if (this.api.is_background || this.api.btnPush || !this.api.is_connected) {
            this.api.foreground();
            if (this.api.towardDocking) {
                this.api.abortNavHttp();
                this.api.abortDockingHttp();
                this.api.towardDocking = false;
                if (this.api.battery_status.remaining <= this.param.battery.critical) {
                    // lowBattery
                    this.popup.lowBattery();
                }
            }
            this.api.btnPush = false; // to say that we have received the btn pushed status
        }
    };
    HomePage.prototype.updateTrajectory = function () {
        // listen the round status to allow the robot to go to the next
        //// go docking or go poi in progress
        if (this.api.towardDocking) {
            //console.log("hehe");
            if (this.api.differential_status.TargetAngularSpeed === 0 && this.api.differential_status.TargetLinearSpeed === 0) {
                this.cptnospeed = this.cptnospeed + 1;
            }
            if (this.cptnospeed == 15) {
                this.api.towardDocking = false;
                this.api.abortNavHttp();
                this.cptnospeed = 0;
                this.wait = false;
            }
            if (this.api.differential_status.status === 2) {
                // if current is hight
                this.api.towardDocking = false;
                this.api.abortNavHttp();
                this.popup.errorBlocked();
                this.alert.errorblocked(this.api.mailAddInformationBasic());
                this.capture();
            }
            else if (this.api.navigation_status.status === 5) {
                // nav error
                this.api.towardDocking = false;
                this.api.towardPOI = false;
                this.api.abortNavHttp();
                this.popup.errorNavAlert();
                this.alert.naverror(this.api.mailAddInformationBasic());
            }
            else if (this.api.battery_status.status === 0) {
                this.api.towardDocking = false;
                this.alert.charging(this.api.mailAddInformationBasic());
                this.api.close_app = true; //close application
                setTimeout(function () {
                    window.close();
                }, 1000);
            }
            else if (this.api.towardDocking &&
                this.api.navigation_status.status === 0 &&
                this.api.docking_status.detected) {
                //connect to the docking when he detect it
                this.api.connectHttp();
            }
        }
    };
    HomePage.prototype.getUpdate = function () {
        var _this = this;
        //console.log("l");
        if (this.api.mapdata) {
            if (this.api.battery_status.status != 0 && !this.api.towardDocking && !this.wait && this.api.battery_status.remaining <= this.param.battery.critical) {
                this.wait = true;
                setTimeout(function () {
                    _this.speech.speak(_this.param.datatext.godocking);
                    _this.popup.showToast(_this.param.datatext.godocking, 10000, "middle");
                    _this.onGo();
                }, 4000);
                setTimeout(function () {
                    _this.wait = false;
                }, 180000);
            }
        }
        // update trajectory, butons input, round status
        if (this.api.close_app) {
            clearInterval(this.update);
            clearInterval(this.cptduration);
        }
        if (!this.api.is_connected) {
            this.api.towardDocking = false;
        }
        else {
            if (this.api.fct_onGo) {
                this.api.fct_onGo = false;
                this.onGo();
            }
            this.btnPressed();
            this.updateTrajectory();
            this.watchIfLocked();
        }
        this.sendMailduration();
        // test joystick count
        if (this.checkjoystick) {
            if (this.currentx == this.lastx) {
                if (this.currenty == this.lasty) {
                    this.countjoystick = this.countjoystick + 1;
                    if (this.countjoystick >= 5) {
                        console.log("stopjoystick");
                        this.joystickstop();
                        this.stopjoystick = true;
                    }
                    if (this.countjoystick >= 15) {
                        clearInterval(this.gamepadinterv);
                        this.checkjoystick = false;
                        this.manager.destroy();
                        this.createjoystick();
                        this.checkjoystick = false;
                    }
                }
                else {
                    this.countjoystick = 0;
                    this.stopjoystick = false;
                    this.checkjoystick = true;
                    this.lastx = this.currentx;
                    this.lasty = this.currenty;
                }
            }
            else {
                this.countjoystick = 0;
                this.stopjoystick = false;
                this.checkjoystick = true;
                this.lastx = this.currentx;
                this.lasty = this.currenty;
            }
        }
    };
    HomePage.prototype.sendMailduration = function () {
        if (this.param.sendduration) {
            this.param.sendduration = false;
            this.alert.duration(this.param.olddata, this.api.mailAddInformationBasic());
        }
    };
    HomePage.prototype.watchIfLocked = function () {
        if (this.api.towardDocking) {
            if ((this.api.towardDocking && this.api.docking_status.detected) ||
                this.api.docking_status.status === 3 ||
                (this.api.anticollision_status.forward < 2 &&
                    this.api.anticollision_status.right < 2 &&
                    this.api.anticollision_status.left < 2)) {
                this.cpt_locked = 0;
                if (this.is_blocked) {
                    this.is_blocked = false;
                    this.popup.alert_blocked.dismiss();
                }
            }
            else if (this.api.anticollision_status.forward === 2 ||
                this.api.anticollision_status.right === 2 ||
                this.api.anticollision_status.left === 2) {
                this.cpt_locked += 1;
            }
            if (this.cpt_locked > 30 && !this.is_blocked) {
                this.popup.blockedAlert();
                this.is_blocked = true;
                this.alert.blockingdocking(this.api.mailAddInformationBasic());
            }
            else if (this.cpt_locked === 80 && this.is_blocked) {
                this.capture();
            }
        }
    };
    HomePage.prototype.getDistance = function (x1, y1, x2, y2) {
        var y = x2 - x1;
        var x = y2 - y1;
        var res = Math.sqrt(x * x + y * y);
        //console.log(res);
        return res;
    };
    HomePage.prototype.calcDiffMin = function (one, two) {
        var DateBegMin = one.getMinutes();
        var DateBegHours = one.getHours() * 60;
        var DateBeg = DateBegMin + DateBegHours;
        var DateEndMin = two.getMinutes();
        var DateEndHours = two.getHours() * 60;
        return DateEndMin + DateEndHours - DateBeg;
    };
    HomePage.prototype.onReloc = function () {
        var _this = this;
        // buton go code
        this.alert.reloc(this.api.mailAddInformationBasic());
        this.showLoading();
        this.speech.speak(this.param.datatext.alertReloc);
        this.api.getCurrentMap();
        setTimeout(function () {
            if (!(_this.api.id_current_map == _this.api.QRselected.id_map)) {
                console.log("changemap");
                _this.api.changeMapbyIdHttp();
            }
        }, 2000);
        setTimeout(function () {
            _this.api.getFirstLocationHttp();
        }, 3000);
        setTimeout(function () {
            _this.api.resetLocationHttp();
        }, 5000);
        setTimeout(function () {
            _this.api.getCurrentMap();
        }, 6000);
        setTimeout(function () {
            _this.dismissLoading();
            _this.api.poiDocking();
            _this.okToast(_this.param.datatext.localizationDone + _this.api.mapdata.Name, 4000);
        }, 7000);
    };
    HomePage.prototype.onclickGo = function (ev) {
        ev.preventDefault();
        this.onGo();
    };
    HomePage.prototype.onGo = function () {
        var _this = this;
        this.cptnospeed = 0;
        // buton go code
        if (this.api.towardDocking) {
            // stop go docking
            this.api.towardDocking = false;
            this.api.abortNavHttp();
            this.api.abortDockingHttp();
            if (this.api.battery_status.remaining <= this.param.battery.critical) {
                // lowBattery
                this.popup.lowBattery();
            }
            else {
                //this.popup.goDockingConfirm();
            }
        }
        else {
            this.alert.askCharge(this.api.mailAddInformationBasic());
            if (this.api.dockingexist) {
                this.api.towardDocking = true;
                // var x1 = this.api.docking.Pose["X"];
                // var y1 = this.api.docking.Pose["Y"];
                // var x2 = this.api.localization_status.positionx;
                // var y2 = this.api.localization_status.positiony;
                // var dist = this.getDistance(x1, y1, x2, y2);
                // if (dist < 0.5) {
                //   if (this.api.docking_status.detected && this.api.anticollision_status.forward == 1) {
                //     this.api.towardDocking = true;
                //     this.api.connectHttp();
                //   }
                //   else if (this.api.docking_status.detected && this.api.anticollision_status.forward == 2) {
                //     this.api.towardDocking = true;
                //     this.api.disconnectHttp();
                //     setTimeout(() => {
                //       if (this.api.docking_status.detected) {
                //         this.api.connectHttp();
                //       } else {
                //         this.api.reachHttp("docking");
                //       }
                //     }, 7000);
                //   }
                // }
                // console.log(this.api.anticollision_status.forward < 2)
                // console.log(this.api.anticollision_status.reverse > 0);
                // console.log(this.api.anticollision_status.left > 0);
                // console.log(this.api.anticollision_status.right > 0);
                if (this.api.anticollision_status.forward < 2 && (this.api.anticollision_status.reverse > 0 || this.api.anticollision_status.locked) && (this.api.anticollision_status.right > 0 || this.api.anticollision_status.left > 0)) {
                    //console.log("coucou");
                    this.api.joystickHttp(0.2, 0);
                    setTimeout(function () {
                        _this.api.joystickHttp(0.2, 0);
                    }, 1000);
                }
                else if (this.api.anticollision_status.forward < 2 && this.api.anticollision_status.right == 2 && this.api.anticollision_status.left < 2) {
                    this.api.joystickHttp(0, 0.3);
                    setTimeout(function () {
                        _this.api.joystickHttp(0, 0.3);
                    }, 1000);
                }
                else if (this.api.anticollision_status.forward < 2 && this.api.anticollision_status.left == 2 && this.api.anticollision_status.right < 2) {
                    this.api.joystickHttp(0, -0.3);
                    setTimeout(function () {
                        _this.api.joystickHttp(0, -0.3);
                    }, 1000);
                }
                else if (this.api.anticollision_status.forward == 2) {
                    this.api.disconnectHttp();
                    setTimeout(function () {
                        if (_this.api.towardDocking) {
                            _this.api.reachHttp("docking");
                        }
                    }, 8000);
                }
                else {
                    this.api.reachHttp("docking");
                }
            }
            else {
                this.popup.showToastRed(this.param.datatext.nodocking, 10000, "middle");
            }
        }
    };
    // config la variable constraints
    HomePage.prototype.setupconstraint = function () {
        var _this = this;
        // demande de permission camera
        navigator.mediaDevices.getUserMedia(this.constraints);
        // verification des permissions video et de la disponibilité de devices
        if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
            // enumeration des appareils connectes pour filtrer
            navigator.mediaDevices.enumerateDevices().then(function (result) {
                var constraints;
                console.log(result);
                result.forEach(function (device) {
                    // filtrer pour garder les flux video
                    if (device.kind.includes("videoinput")) {
                        // filtrer le label de la camera
                        if (device.label.includes("USB")) {
                            constraints = {
                                audio: false,
                                //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
                                // affecter le deviceid filtré
                                video: { deviceId: device.deviceId },
                                facingMode: "environment",
                                width: { ideal: 1920 },
                                height: { ideal: 1080 },
                            };
                        }
                    }
                });
                // on lance la connexion de la vidéo
                if (constraints) {
                    _this.constraints = constraints;
                    _this.startCamera(constraints);
                    console.log("startcam");
                }
                //this.startCamera(constraints);
            });
        }
        else {
            console.log("Sorry, camera not available.");
        }
    };
    // function launch connection camera
    HomePage.prototype.startCamera = function (cs) {
        if (!!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia)) {
            // recupération du flux avec les constraints configurés puis on attache le flux à la balise video
            navigator.mediaDevices
                .getUserMedia(cs)
                .then(this.attachVideo.bind(this))
                .catch(this.handleError);
        }
        else {
            alert("Sorry, camera not available.");
        }
    };
    HomePage.prototype.handleError = function (error) {
        console.log("Error: ", error);
    };
    // function qui attache le flux video à la balise video
    HomePage.prototype.attachVideo = function (stream) {
        var _this = this;
        this.renderer.setProperty(this.videoElement.nativeElement, "srcObject", stream);
        this.renderer.listen(this.videoElement.nativeElement, "play", function (event) {
            _this.videoHeight = _this.videoElement.nativeElement.videoHeight;
            _this.videoWidth = _this.videoElement.nativeElement.videoWidth;
        });
    };
    // function take snapshot and send mail
    HomePage.prototype.capture = function () {
        if (this.param.robot.send_pic == 1) {
            this.setupconstraint();
            this.renderer.setProperty(this.canvas.nativeElement, "width", this.videoWidth);
            this.renderer.setProperty(this.canvas.nativeElement, "height", this.videoHeight);
            this.canvas.nativeElement
                .getContext("2d")
                .drawImage(this.videoElement.nativeElement, 0, 0);
            var url = this.canvas.nativeElement.toDataURL();
            //console.log(url);
            this.alert.Blocked_c(url);
        }
        else {
            this.alert.Block_c();
        }
    };
    HomePage.prototype.onSliderRelease = function (ev, id) {
        ev.preventDefault();
        id.open();
    };
    HomePage.prototype.joystickmove = function () {
        this.api.joystickHttp(this.vlin, this.vrad);
    };
    HomePage.prototype.joystickstop = function () {
        this.api.joystickHttp(0, 0);
    };
    HomePage.prototype.createjoystick = function () {
        var _this = this;
        var options = {
            zone: document.getElementById("zone_joystick"),
            size: 3 * this.size,
        };
        this.manager = __WEBPACK_IMPORTED_MODULE_7_nipplejs___default.a.create(options);
        this.manager.on("move", function (evt, nipple) {
            _this.currentx = nipple.raw.position.x;
            _this.currenty = nipple.raw.position.y;
            _this.checkjoystick = true;
            if (!_this.stopjoystick) {
                if (nipple.direction && nipple.direction.angle && nipple.angle) {
                    //console.log(nipple.direction.angle);
                    if (nipple.direction.angle === "left") {
                        _this.vlin = 0;
                        _this.vrad = (0.3 * nipple.distance) / 50;
                    }
                    else if (nipple.direction.angle === "right") {
                        _this.vlin = 0;
                        _this.vrad = -(0.3 * nipple.distance) / 50;
                    }
                    else if (nipple.direction.angle === "up") {
                        _this.vlin = (_this.maxlin * nipple.distance) / 50;
                        _this.vrad = 0;
                        // if (Math.abs((nipple.angle.radian - Math.PI / 2) / 2) > 0.18) {
                        //   this.vrad = (nipple.angle.radian - Math.PI / 2) / 2 - 0.09;
                        // } else {
                        // this.vrad = 0;
                        // }
                    }
                    else if (nipple.direction.angle === "down") {
                        _this.vlin = -(_this.maxback * nipple.distance) / 50;
                        _this.vrad = 0;
                    }
                }
            }
            else {
                _this.vlin = 0;
                _this.vrad = 0;
            }
        });
        this.manager.on("added", function (evt, nipple) {
            console.log("added");
            _this.gamepadinterv = setInterval(function () { if (!_this.stopjoystick) {
                _this.joystickmove();
            } }, 500);
        });
        this.manager.on("end", function (evt, nipple) {
            console.log("end");
            _this.vlin = 0;
            _this.vrad = 0;
            clearInterval(_this.gamepadinterv);
            _this.checkjoystick = false;
            _this.stopjoystick = false;
            _this.joystickstop();
        });
    };
    HomePage.prototype.ngAfterViewInit = function () {
        this.createjoystick();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("video"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], HomePage.prototype, "videoElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("canvas"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], HomePage.prototype, "canvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("videoCanvas"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], HomePage.prototype, "videoCanvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["j" /* Select */])
    ], HomePage.prototype, "select1", void 0);
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-home",template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\batteryapp\src\pages\home\home.html"*/'<!-- home page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="home"></headpage>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-card text-center class="big_card">\n\n    <ion-grid class="heightstyle">\n\n      <ion-row class="heightstyle">\n\n        <ion-col col-3 class="heightstyle2">\n\n          <ion-select #select1 *ngIf="!api.towardDocking" slot="end" okText="{{this.param.datatext.btn_ok}}"\n\n            cancelText="{{this.param.datatext.btn_cancel}}" [selectOptions]="selectOptions" class="myCustomSelect"\n\n            placeholder="{{this.param.datatext.manualreloc}}" [(ngModel)]="SelectRound" (ionChange)="onQRChange($event)"\n\n            (mouseup)="onSliderRelease($event,select1)">\n\n            <ion-option *ngFor="let reloc of this.param.qrcodes" value="{{reloc.qr}}">{{reloc.qr}}</ion-option>\n\n          </ion-select>\n\n\n\n          <button *ngIf="!api.towardDocking" ion-button color="primary" [disabled]="!api.is_connected"\n\n            (mouseup)="scan($event)" class="btn_reloc">\n\n            <ion-icon ngcolor="light" name="qr-scanner" class="icon_style">\n\n              <p class="legende">{{this.param.datatext.reloc}}</p>\n\n            </ion-icon>\n\n          </button>\n\n          <div class="div_btn_go">\n\n            <!-- the Go button is disabled if the robot is not connected -->\n\n\n\n            <button *ngIf="!api.towardDocking" ion-button color="secondary"\n\n              [disabled]="!api.is_connected || api.battery_status.status ===0" (mouseup)="onclickGo($event)"\n\n              class="btn_go">\n\n              <ion-icon ngcolor="light" name="battery-charging" class="icon_style">\n\n                <p class="legende">\n\n                  {{this.param.datatext.btn_charge}}\n\n                </p>\n\n              </ion-icon>\n\n            </button>\n\n            <button *ngIf="api.towardDocking" ion-button color="danger" (mouseup)="onclickGo($event)" class="btn_go">\n\n              <ion-icon color="light" name="hand" class="icon_style">\n\n                <p class="legende">\n\n                  {{this.param.datatext.btn_stop}}\n\n                </p>\n\n              </ion-icon>\n\n            </button>\n\n          </div>\n\n        </ion-col>\n\n\n\n        <ion-col col-9 class="heightstyle2">\n\n          <ion-card class="test">\n\n            <canvas #videoCanvas style="display: none;"></canvas> \n\n            <video #video autoplay class="video"></video>\n\n            <img [hidden]="param.robot.cam_USB==1" class="video" id="imgcompressed" />\n\n            <button ion-button class="framebtn" [disabled]="true">\n\n              <ion-icon class="frame" name="qr-scanner"></ion-icon>\n\n            </button>\n\n           \n\n            <div class="joy" id="zone_joystick"></div>\n\n\n\n          </ion-card>\n\n        </ion-col>\n\n      </ion-row>\n\n    </ion-grid>\n\n  </ion-card>\n\n  <canvas #canvas style="display: none"> </canvas>\n\n</ion-content>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\batteryapp\src\pages\home\home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4__services_param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["k" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_5__services_speech_service__["a" /* SpeechService */],
            __WEBPACK_IMPORTED_MODULE_3__services_popup_service__["a" /* PopupService */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_2__services_alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["W" /* Renderer2 */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 122:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_popup_service__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MailPage = /** @class */ (function () {
    function MailPage(param, popup, toastCtrl) {
        this.param = param;
        this.popup = popup;
        this.toastCtrl = toastCtrl;
        this.inputmail = "";
    }
    MailPage.prototype.ngOnInit = function () {
        this.popup.onSomethingHappened2(this.addMail.bind(this));
    };
    MailPage.prototype.errorToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toast"
        });
        toast.present();
    };
    MailPage.prototype.okToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toastok"
        });
        toast.present();
    };
    MailPage.prototype.removeMail = function (ev, m) {
        ev.preventDefault();
        var index = this.param.maillist.indexOf(m);
        if (index !== -1) {
            this.param.maillist.splice(index, 1);
            this.param.deleteMail(m);
            this.okToast(this.param.datatext.mailRemove);
        }
    };
    MailPage.prototype.is_a_mail = function (m) {
        var mailformat = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$";
        return m.match(mailformat);
    };
    MailPage.prototype.addMail = function (ev) {
        var _this = this;
        ev.preventDefault();
        if (this.is_a_mail(this.inputmail)) {
            var index = this.param.maillist.indexOf(this.inputmail);
            if (index !== -1) {
                this.errorToast(this.param.datatext.mailexist);
            }
            else {
                this.param.maillist.push(this.inputmail);
                this.param.addMail(this.inputmail);
                this.okToast(this.param.datatext.mailadd);
                setTimeout(function () {
                    _this.content1.scrollToBottom();
                }, 300);
            }
        }
        else {
            this.errorToast(this.param.datatext.mailincorrect);
        }
    };
    MailPage.prototype.update_send_pic = function () {
        if (this.param.robot.send_pic == 1) {
            this.param.robot.send_pic = 0;
        }
        else {
            this.param.robot.send_pic = 1;
        }
        this.param.updateRobot();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("content1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* Content */])
    ], MailPage.prototype, "content1", void 0);
    MailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-mail',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\batteryapp\src\pages\mail\mail.html"*/'<!-- mail page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="mail"></headpage>\n\n</ion-header>\n\n\n\n\n\n<ion-content class="big-ion-content" padding >\n\n  <ion-card text-center class="big_card" >\n\n    <ion-grid class="heightstyle">\n\n      <ion-item >\n\n        <ion-label class="labelcss" stacked>{{param.datatext.receivepic}}</ion-label>\n\n        <ion-toggle\n\n              [checked]="this.param.robot.send_pic ==1 "\n\n              (ionChange)="update_send_pic()"\n\n              item-end\n\n            ></ion-toggle>\n\n            <ion-icon\n\n              class="iconscss"\n\n              name="camera"\n\n              item-start\n\n              color="primary"\n\n            ></ion-icon>\n\n      </ion-item>\n\n        <ion-item >\n\n          <ion-label class="txt" stacked>{{param.datatext.newmail}}</ion-label>\n\n          <ion-input inputmode="email" [(ngModel)]="inputmail" placeholder="kompai@gmail.com"></ion-input>\n\n          <button *ngIf="is_a_mail(inputmail)" class="btn-add" (mouseup)="popup.displayrgpd($event)" item-end>\n\n            <ion-icon  color="light" name="add"></ion-icon>\n\n          </button>\n\n          <button *ngIf="!(is_a_mail(inputmail))" class="mailnotvalid" (mouseup)="addMail($event)" item-end>\n\n            <ion-icon  color="light" name="add"></ion-icon>\n\n          </button>\n\n        </ion-item>\n\n       \n\n        <ion-content\n\n        #content1\n\n        style="height: 80%; width: 100%; background-color: transparent;"\n\n      >\n\n        \n\n          <ion-item *ngFor="let m of this.param.maillist">\n\n            <ion-icon class="iconscss" name="at" item-start color="primary"></ion-icon>{{m}}\n\n            <button class="btn-trash" item-end (mouseup)="removeMail($event,m)">\n\n              <ion-icon class="btn-trash" name="trash"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n        \n\n        </ion-content>\n\n    \n\n    </ion-grid> \n\n\n\n</ion-card>\n\n \n\n\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\batteryapp\src\pages\mail\mail.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_3__services_popup_service__["a" /* PopupService */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ToastController */]])
    ], MailPage);
    return MailPage;
}());

//# sourceMappingURL=mail.js.map

/***/ }),

/***/ 13:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParamService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__langage_fr_FR_json__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__langage_fr_FR_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__langage_fr_FR_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__langage_en_GB_json__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__langage_en_GB_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__langage_en_GB_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__langage_es_ES_json__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__langage_es_ES_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__langage_es_ES_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__langage_de_DE_json__ = __webpack_require__(316);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__langage_de_DE_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__langage_de_DE_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__langage_el_GR_json__ = __webpack_require__(317);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__langage_el_GR_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__langage_el_GR_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common_http__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser__ = __webpack_require__(21);
// the parameter script
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ParamService = /** @class */ (function () {
    function ParamService(httpClient, sanitizer) {
        this.httpClient = httpClient;
        this.sanitizer = sanitizer;
        this.tableduration = {};
        this.tablerobot = {};
        this.mail = {};
        this.phone = {};
        this.currentduration = {};
        this.olddata = {};
        this.durationNS = {};
        this.tabledurationNS = {};
        this.maillist = [];
        this.phonenumberlist = [];
        this.cpt = 0;
        this.sendduration = false;
    }
    ParamService.prototype.updateDurationNS = function (id) {
        this.tabledurationNS.action = "update";
        this.tabledurationNS.id_duration = id;
        this.httpClient
            .post("http://localhost/ionicDB/duration/updateduration.php", JSON.stringify(this.tabledurationNS))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getBattery = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/getbattery.php").subscribe(function (data) {
            _this.battery = data[0];
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getDurationNS = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/duration/getduration.php").subscribe(function (data) {
            _this.duration = data;
            _this.durationNS = data;
            //console.log(data);
            //console.log(this.durationNS.length);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getDataRobot = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/getrobot.php").subscribe(function (data) {
            //console.log("robot:"+data);
            _this.robot = data[0];
            _this.langage = _this.robot.langage;
            if (_this.langage === "fr-FR") {
                _this.datatext = __WEBPACK_IMPORTED_MODULE_0__langage_fr_FR_json___default.a;
            }
            else if (_this.langage === "en-GB") {
                _this.datatext = __WEBPACK_IMPORTED_MODULE_1__langage_en_GB_json___default.a;
            }
            else if (_this.langage === "es-ES") {
                _this.datatext = __WEBPACK_IMPORTED_MODULE_2__langage_es_ES_json___default.a;
            }
            else if (_this.langage === "el-GR") {
                _this.datatext = __WEBPACK_IMPORTED_MODULE_4__langage_el_GR_json___default.a;
            }
            else if (_this.langage === "de-DE") {
                _this.datatext = __WEBPACK_IMPORTED_MODULE_3__langage_de_DE_json___default.a;
            }
            _this.source = _this.sanitizer.bypassSecurityTrustResourceUrl(_this.datatext.URL_battery);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.updateRobot = function () {
        this.tablerobot.action = "update";
        this.tablerobot.langage = this.robot.langage;
        this.tablerobot.serialnumber = this.serialnumber;
        this.tablerobot.name = this.name;
        this.tablerobot.send_pic = this.robot.send_pic;
        this.tablerobot.password = this.robot.password;
        this.httpClient
            .post("http://localhost/ionicDB/updaterobotpassword.php", JSON.stringify(this.tablerobot))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getQR = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/getqrcodes.php").subscribe(function (data) {
            _this.qrcodes = data;
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getPhone = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/getphone.php").subscribe(function (data) {
            if (_this.phonenumberlist.length === 0) {
                for (var value in data) {
                    _this.phonenumberlist.push(data[value].phonenumber);
                }
            }
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.addMail = function (m) {
        this.mail.action = "insert";
        this.mail.mail = m;
        this.mail.serialnumber = this.serialnumber;
        this.httpClient
            .post("http://localhost/ionicDB/addmail.php", JSON.stringify(this.mail))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.deleteMail = function (m) {
        this.mail.action = "delete";
        this.mail.mail = m;
        this.httpClient
            .post("http://localhost/ionicDB/deletemail.php", JSON.stringify(this.mail))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.addPhone = function (m) {
        this.phone.action = "insert";
        this.phone.phonenumber = m;
        this.phone.serialnumber = this.serialnumber;
        this.httpClient
            .post("http://localhost/ionicDB/addphone.php", JSON.stringify(this.phone))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.deletePhone = function (m) {
        this.phone.action = "delete";
        this.phone.phonenumber = m;
        this.httpClient
            .post("http://localhost/ionicDB/deletephone.php", JSON.stringify(this.phone))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getMail = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/getmail.php").subscribe(function (data) {
            if (_this.maillist.length === 0) {
                for (var value in data) {
                    _this.maillist.push(data[value].mail);
                }
            }
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.addDuration = function () {
        this.tableduration.action = "insert";
        this.tableduration.serialnumber = this.serialnumber;
        this.tableduration.round = this.currentduration.round;
        this.tableduration.battery = this.currentduration.battery;
        this.tableduration.walk = this.currentduration.walk;
        this.tableduration.patrol = this.currentduration.patrol;
        this.tableduration.date = this.currentduration.date;
        this.httpClient
            .post("http://localhost/ionicDB/addduration.php", JSON.stringify(this.tableduration))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.updateDuration = function () {
        this.tableduration.action = "update";
        this.tableduration.battery = this.currentduration.battery;
        this.tableduration.date = this.currentduration.date;
        this.httpClient
            .post("http://localhost/ionicDB/duration/updatedurationbattery.php", JSON.stringify(this.tableduration))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.fillData = function () {
        this.name = this.robot.name;
        this.allowspeech = this.robot.allowspeech;
        this.maildatapassw = atob(this.robot.maildatapass);
        this.mailrobotpassw = atob(this.robot.mailrobotpass);
        this.localhost = this.robot.localhost;
        console.log(this.localhost);
        this.serialnumber = this.robot.serialnumber;
        this.robotmail = this.robot.mailrobot;
        this.datamail = this.robot.maildata;
        this.datamaillist = ["data@kompai.com"];
        if (this.duration.length > 0) {
            if (this.duration[this.duration.length - 1].date ===
                new Date().toLocaleDateString("fr-CA")) {
                this.currentduration.date = this.duration[this.duration.length - 1].date;
                this.currentduration.round = this.duration[this.duration.length - 1].round;
                this.currentduration.battery = this.duration[this.duration.length - 1].battery;
                this.currentduration.patrol = this.duration[this.duration.length - 1].patrol;
                this.currentduration.walk = this.duration[this.duration.length - 1].walk;
                this.currentduration.toolbox = this.duration[this.duration.length - 1].toolbox;
                this.currentduration.logistic = this.duration[this.duration.length - 1].logistic;
            }
            else {
                if (!this.sendduration) {
                    this.sendduration = true;
                    this.init_currentduration();
                    this.addDuration();
                }
            }
        }
        else {
            if (!this.sendduration) {
                this.sendduration = true;
                this.init_currentduration();
                this.addDuration();
            }
        }
    };
    ParamService.prototype.cptDuration = function () {
        if (this.currentduration.date === new Date().toLocaleDateString("fr-CA")) {
            this.cpt = parseInt(this.currentduration.battery);
            this.currentduration.battery = this.cpt + 2;
            this.updateDuration();
        }
        else {
            this.init_currentduration();
            this.addDuration();
        }
    };
    ParamService.prototype.init_currentduration = function () {
        this.currentduration.round = 0;
        this.currentduration.battery = 0;
        this.currentduration.patrol = 0;
        this.currentduration.walk = 0;
        this.currentduration.toolbox = 0;
        this.currentduration.logistic = 0;
        this.currentduration.date = new Date().toLocaleDateString("fr-CA");
    };
    ParamService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_5__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_6__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser__["c" /* DomSanitizer */]])
    ], ParamService);
    return ParamService;
}());

//# sourceMappingURL=param.service.js.map

/***/ }),

/***/ 133:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 133;

/***/ }),

/***/ 175:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 175;

/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_api_service__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_first_first__ = __webpack_require__(219);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, api) {
        var _this = this;
        this.api = api;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_first_first__["a" /* FirstPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            platform.pause.subscribe(function () {
                _this.api.pauseHttp();
                _this.api.background();
                window.close();
            });
            platform.resume.subscribe(function () { });
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\batteryapp\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\batteryapp\src\app\app.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_4__services_api_service__["a" /* ApiService */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 219:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FirstPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_param_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_api_service__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_alert_service__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__home_home__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_popup_service__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_speech_service__ = __webpack_require__(61);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var FirstPage = /** @class */ (function () {
    function FirstPage(loadingCtrl, popup, param, alert, api, speech, navCtrl) {
        this.loadingCtrl = loadingCtrl;
        this.popup = popup;
        this.param = param;
        this.alert = alert;
        this.api = api;
        this.speech = speech;
        this.navCtrl = navCtrl;
        this.homePage = __WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */];
        this.cpt_open = 0;
        this.loading = this.loadingCtrl.create({});
        this.loading.present();
    }
    FirstPage.prototype.ngOnInit = function () {
        var _this = this;
        this.appopen = setInterval(function () { return _this.appOpen(); }, 1000);
        this.speech.getVoice();
    };
    FirstPage.prototype.appOpen = function () {
        if (this.param.localhost != undefined) {
            this.api.checkrobot();
        }
        this.api.checkInternet();
        this.alert.checkwifi(this.api.wifiok);
        this.cpt_open += 1;
        if (this.cpt_open === 15) {
            this.popup.startFailedAlert();
            this.speech.speak(this.param.datatext.cantlaunch);
        }
        if (!this.api.appOpened) {
            this.param.getDataRobot();
            this.param.getMail();
            this.param.getDurationNS();
            this.param.getQR();
            this.param.getBattery();
            if (this.param.robot) {
                this.param.fillData();
                if (this.param.robot.httpskomnav == 0) {
                    this.api.httpskomnav = "http://";
                    this.api.wsskomnav = "ws://";
                }
                else {
                    this.api.httpskomnav = "https://";
                    this.api.wsskomnav = "wss://";
                }
                if (this.param.robot.httpsros == 0) {
                    this.api.wssros = "ws://";
                }
                else {
                    this.api.wssros = "wss://";
                }
            }
            if (this.api.robotok && this.param.langage && this.param.maillist) {
                this.api.abortHttp();
                if (!this.api.socketok) {
                    this.api.instanciate();
                }
                this.api.getCurrentMap();
                this.api.appOpened = true;
                this.api.eyesHttp(4);
            }
        }
        else if (this.api.appOpened && this.cpt_open >= 6) {
            this.alert.appOpen(this.api.mailAddInformationBasic());
            this.navCtrl.setRoot(this.homePage);
            this.loading.dismiss();
            if (this.popup.alert_blocked) {
                this.popup.alert_blocked.dismiss();
            }
            clearInterval(this.appopen);
            console.log(this.cpt_open);
        }
    };
    FirstPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-first",template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\batteryapp\src\pages\first\first.html"*/'<ion-header no-border>\n\n \n\n</ion-header>\n\n<ion-content padding >\n\n  <ion-card text-center class="big_card" >\n\n      <ion-grid class="heightstyle">\n\n       \n\n      </ion-grid> \n\n\n\n  </ion-card>\n\n \n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\batteryapp\src\pages\first\first.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_6__services_popup_service__["a" /* PopupService */],
            __WEBPACK_IMPORTED_MODULE_2__services_param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_4__services_alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_7__services_speech_service__["a" /* SpeechService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
    ], FirstPage);
    return FirstPage;
}());

//# sourceMappingURL=first.js.map

/***/ }),

/***/ 220:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeadpageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_home_home__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_tuto_tuto__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_api_service__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_alert_service__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_popup_service__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_speech_service__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_param_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_param_param__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_mail_mail__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_platform_browser__ = __webpack_require__(21);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var HeadpageComponent = /** @class */ (function () {
    function HeadpageComponent(toastCtrl, navCtrl, param, speech, api, alert, popup, sanitizer) {
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.param = param;
        this.speech = speech;
        this.api = api;
        this.alert = alert;
        this.popup = popup;
        this.sanitizer = sanitizer;
        this.homePage = __WEBPACK_IMPORTED_MODULE_1__pages_home_home__["a" /* HomePage */];
        this.tutoPage = __WEBPACK_IMPORTED_MODULE_2__pages_tuto_tuto__["a" /* TutoPage */];
        this.paramPage = __WEBPACK_IMPORTED_MODULE_9__pages_param_param__["a" /* ParamPage */];
        this.mailPage = __WEBPACK_IMPORTED_MODULE_10__pages_mail_mail__["a" /* MailPage */];
        this.cpt_open = 0;
        this.cpt_battery = 0;
    }
    HeadpageComponent.prototype.ionViewWillLeave = function () {
        clearInterval(this.update);
        clearInterval(this.lowbattery);
        clearInterval(this.source);
    };
    HeadpageComponent.prototype.ngOnDestroy = function () {
        clearInterval(this.update);
        clearInterval(this.lowbattery);
        clearInterval(this.source);
    };
    HeadpageComponent.prototype.onTouchWifi = function (ev) {
        ev.preventDefault();
        if (!this.api.wifiok) {
            this.okToast(this.param.datatext.nointernet);
        }
        else {
            this.okToast(this.param.datatext.connectedToI);
        }
    };
    HeadpageComponent.prototype.updateBattery = function () {
        if (this.api.statusRobot === 2) {
            // battery logo will not apppear
            this.api.batteryState = 10;
        }
        if (this.api.battery_status.status < 2) {
            //charging
            this.api.batteryState = 4;
        }
        else if (this.api.battery_status.status === 3 ||
            this.api.battery_status.remaining <= this.param.battery.critical) {
            //critical
            this.api.batteryState = 0;
        }
        else if (this.api.battery_status.remaining > this.param.battery.high) {
            //hight
            this.api.batteryState = 3;
        }
        else if (this.api.battery_status.remaining > this.param.battery.low) {
            //mean
            this.api.batteryState = 2;
        }
        else if (this.api.battery_status.remaining <= this.param.battery.low) {
            //low
            this.api.batteryState = 1;
        }
        else {
            // battery logo will not apppear
            this.api.batteryState = 10;
        }
    };
    HeadpageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.popup.onSomethingHappened1(this.accessparam.bind(this));
        // bouton 's text (Return / Exit), to the left of the header, depends on the page you are on
        if (this.pageName === "home") {
            this.update = setInterval(function () { return _this.getUpdate(); }, 1000); //update hour, battery every 1/2 secondes
            this.lowbattery = setInterval(function () { return _this.monitoringBattery(); }, 60000);
            this.source = setInterval(function () { return _this.api.jetsonOK(); }, 2000);
            this.textBoutonHeader = this.param.datatext.quit;
            this.textHeadBand = this.param.datatext.battery;
        }
        else if (this.pageName === "param") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.param;
        }
        else if (this.pageName === "sms") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.receivesms;
        }
        else if (this.pageName === "mail") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.receivemail;
        }
        else if (this.pageName === "langue") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.changelangage;
        }
        else if (this.pageName === "password") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.editpswd;
        }
        else {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.tutorial;
        }
    };
    HeadpageComponent.prototype.onTouchStatus = function (ev) {
        ev.preventDefault();
        if (!this.api.towardDocking) {
            //the robot is not mooving
            if (this.api.statusRobot === 2) {
                // if status red
                this.popup.statusRedPresent();
            }
            else {
                this.okToast(this.param.datatext.statusGreenPresent_message);
            }
        }
    };
    HeadpageComponent.prototype.monitoringBattery = function () {
        if (this.api.batteryState === 0) {
            //critical
            if (!this.api.dockingexist) {
                this.popup.lowBattery();
            }
            this.alert.lowBattery(this.api.mailAddInformationBasic());
            this.alert.Battery_c();
            clearInterval(this.lowbattery);
        }
    };
    HeadpageComponent.prototype.onTouchBattery = function (ev) {
        ev.preventDefault();
        if (this.api.batteryState == 0) {
            this.popup.showToastRed(this.param.datatext.lowBattery_title +
                " : " +
                this.api.battery_status.remaining +
                "%", 3000, "top");
        }
        else {
            this.okToast(this.param.datatext.battery +
                " : " +
                this.api.battery_status.remaining +
                "%");
        }
    };
    HeadpageComponent.prototype.okToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: "middle",
            cssClass: "toastok",
        });
        toast.present();
    };
    HeadpageComponent.prototype.getUpdate = function () {
        //update hour, battery
        this.updateBattery();
        this.updateBand();
        var now = new Date().toLocaleString("fr-FR", {
            hour: "numeric",
            minute: "numeric",
        });
        this.api.hour = now;
        if (this.api.statusRobot === 0 && this.api.batteryState === 10) {
            this.cpt_battery += 1;
        }
        else {
            this.cpt_battery = 0;
        }
        if (this.cpt_battery > 10) {
            this.api.statusRobot = 2;
        }
    };
    HeadpageComponent.prototype.updateBand = function () {
        if (this.api.towardDocking) {
            this.textHeadBand = this.param.datatext.godocking;
        }
        else if (this.pageName === "home") {
            if (this.api.docking_status.status === 3 && this.api.mapdata) {
                this.textHeadBand =
                    this.param.datatext.charging_remaining +
                        this.api.battery_status.remaining +
                        this.param.datatext.map +
                        this.api.mapdata.Name;
            }
            else if (this.api.docking_status.status === 3) {
                this.textHeadBand =
                    this.param.datatext.charging_remaining +
                        this.api.battery_status.remaining +
                        this.param.datatext.map +
                        "--";
            }
            else if (this.api.mapdata) {
                if (this.api.batteryState == 0) {
                    this.textHeadBand =
                        this.param.datatext.lowBattery_title +
                            this.param.datatext.map +
                            this.api.mapdata.Name;
                }
                else {
                    this.textHeadBand =
                        this.param.datatext.battery +
                            " : " +
                            this.api.battery_status.remaining +
                            this.param.datatext.map +
                            this.api.mapdata.Name;
                }
            }
            else {
                if (this.api.batteryState == 0) {
                    this.textHeadBand =
                        this.param.datatext.lowBattery_title +
                            this.param.datatext.map +
                            "--";
                }
                else {
                    this.textHeadBand =
                        this.param.datatext.battery +
                            " : " +
                            this.api.battery_status.remaining +
                            this.param.datatext.map +
                            "--";
                }
            }
        }
    };
    HeadpageComponent.prototype.onTouchHelp = function (ev) {
        ev.preventDefault();
        if (!this.api.towardDocking) {
            //robot is not moving
            if (!(this.pageName === "tuto")) {
                this.alert.displayTuto(this.api.mailAddInformationBasic());
                this.navCtrl.push(this.tutoPage);
            }
        }
    };
    HeadpageComponent.prototype.accessparam = function () {
        this.navCtrl.push(this.paramPage);
    };
    HeadpageComponent.prototype.onTouchParam = function (ev) {
        ev.preventDefault();
        if (!this.api.towardDocking) {
            //robot is not moving
            if (this.pageName === "param" ||
                this.pageName === "sms" ||
                this.pageName === "mail" ||
                this.pageName === "langue" ||
                this.pageName === "password") {
                console.log("already");
            }
            else {
                this.popup.askpswd();
            }
        }
    };
    HeadpageComponent.prototype.onquit = function (ev) {
        ev.preventDefault();
        this.clicOnMenu();
    };
    HeadpageComponent.prototype.clicOnMenu = function () {
        if (this.pageName === "tuto" || this.pageName === "param") {
            //return to home page
            this.navCtrl.popToRoot();
        }
        else if (this.pageName === "sms" ||
            this.pageName === "mail" ||
            this.pageName === "langue" ||
            this.pageName === "password") {
            this.navCtrl.pop();
        }
        else {
            if (this.api.towardDocking) {
                this.api.abortNavHttp();
                this.api.towardDocking = false;
                this.popup.quitConfirm();
            }
            else {
                this.api.close_app = true;
                this.alert.appClosed(this.api.mailAddInformationBasic());
                //stop the round and quit the app
                this.api.abortHttp();
                setTimeout(function () {
                    window.close();
                }, 1000);
            }
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], HeadpageComponent.prototype, "pageName", void 0);
    HeadpageComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "headpage",template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\batteryapp\src\components\headpage\headpage.html"*/'<!-- Html component of the HMI header with the Back / Exit button, the time, the sound buttons, microphone, parameters etc ...-->\n\n\n\n<ion-navbar hideBackButton>\n\n\n\n  <ion-buttons left class="btn_menu_size">\n\n    <button ion-button solid large class="btn_menu" (mouseup)="onquit($event)" >\n\n      <font class="font_menu_size">{{this.textBoutonHeader}}</font>\n\n    </button>\n\n  </ion-buttons>\n\n\n\n  <ion-title text-center>\n\n    <font class="hour_size">{{api.hour}}</font>\n\n  </ion-title>\n\n\n\n  <ion-buttons right>\n\n    <button ion-button class="btn_header" (mouseup)="onTouchHelp($event)" >\n\n      <img class="imgicon" src="./assets/imgs/help.png" />\n\n    </button>\n\n    <button ion-button class="btn_header" (mouseup)="onTouchParam($event)">\n\n      <img class="imgicon" src="./assets/imgs/parameter.png" />\n\n    </button>\n\n    <!-- <button ion-button disabled class="btn_header"  >\n\n      <img *ngIf="!microon" class="imgicon" src="./assets/imgs/microoff.png"/>\n\n      <img *ngIf="microon" class="imgicon" src="./assets/imgs/microon.png"/>\n\n    </button>\n\n    <button ion-button disabled class="btn_header">\n\n      <img *ngIf="volumeState === 0" class="imgicon" src="./assets/imgs/volumemute.png"/>\n\n      <img *ngIf="volumeState === 1" class="imgicon" src="./assets/imgs/volumelow.png"/>\n\n      <img *ngIf="volumeState === 2" class="imgicon" src="./assets/imgs/volumehight.png"/>\n\n    </button> -->\n\n    <button ion-button class="btn_header" (mouseup)="onTouchWifi($event)" >\n\n      <img *ngIf="!api.wifiok" class="imginternet" src="./assets/imgs/wifioff.png" />\n\n      <img *ngIf="api.wifiok" class="imginternet" src="./assets/imgs/wifion.png" />\n\n    </button>\n\n    <button ion-button class="btn_header" (mouseup)="onTouchStatus($event)" >\n\n      <img *ngIf="api.statusRobot===0 " class="imgwifi" src="./assets/imgs/statusgreen.png" />\n\n      <img *ngIf="api.statusRobot ===1" class="imgwifi" src="./assets/imgs/statusorange.png" />\n\n      <img *ngIf="api.statusRobot===2" class="imgwifi" src="./assets/imgs/statusred.png" />\n\n    </button>\n\n    <button ion-button class="btn_header" (mouseup)="onTouchBattery($event)" >\n\n      <img *ngIf="api.batteryState === 0" class="imgbattery" src="./assets/imgs/batteryoff.png" />\n\n      <img *ngIf="api.batteryState === 1" class="imgbattery" src="./assets/imgs/batterylow.png" />\n\n      <img *ngIf="api.batteryState === 2" class="imgbattery" src="./assets/imgs/batterymean.png" />\n\n      <img *ngIf="api.batteryState === 3" class="imgbattery" src="./assets/imgs/batteryhight.png" />\n\n      <img *ngIf="api.batteryState === 4" class="imgbattery" src="./assets/imgs/batterycharge.png" />\n\n    </button>\n\n  </ion-buttons>\n\n\n\n</ion-navbar>\n\n<ion-toolbar no-padding>\n\n  <div class="scroll_parent">\n\n    <ion-title class="scroll">\n\n      <pre class="preclass"><font class="scroll_text">{{this.textHeadBand}}</font></pre>\n\n    </ion-title>\n\n  </div>\n\n\n\n</ion-toolbar>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\batteryapp\src\components\headpage\headpage.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["k" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_8__services_param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_7__services_speech_service__["a" /* SpeechService */],
            __WEBPACK_IMPORTED_MODULE_4__services_api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_5__services_alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_6__services_popup_service__["a" /* PopupService */],
            __WEBPACK_IMPORTED_MODULE_11__angular_platform_browser__["c" /* DomSanitizer */]])
    ], HeadpageComponent);
    return HeadpageComponent;
}());

//# sourceMappingURL=headpage.js.map

/***/ }),

/***/ 221:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TutoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_param_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_pdfjs_dist_webpack_js__ = __webpack_require__(320);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_pdfjs_dist_webpack_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_pdfjs_dist_webpack_js__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TutoPage = /** @class */ (function () {
    function TutoPage(sanitizer, param) {
        this.sanitizer = sanitizer;
        this.param = param;
        this.pageNum = 1;
        this.pdfSrc = "https://vadimdez.github.io/ng2-pdf-viewer/assets/pdf-test.pdf";
        this.PDFJSViewer = __WEBPACK_IMPORTED_MODULE_3_pdfjs_dist_webpack_js___default.a;
        this.currPage = 1; //Pages are 1-based not 0-based
        this.numPages = 0;
        this.thePDF = null;
        this.pageRendering = false;
        this.pageNumPending = null;
    }
    TutoPage.prototype.ngOnInit = function () {
    };
    TutoPage.prototype.ionViewDidEnter = function () {
        this.launchdoc();
    };
    TutoPage.prototype.launchdoc = function () {
        var _this = this;
        //This is where you start
        __WEBPACK_IMPORTED_MODULE_3_pdfjs_dist_webpack_js___default.a.getDocument(this.param.datatext.URL_battery).then(function (pdf) {
            //Set PDFJS global object (so we can easily access in our page functions
            _this.thePDF = pdf;
            //How many pages it has
            _this.numPages = pdf.numPages;
            //Start with first page
            pdf.getPage(1).then(function (page) { _this.handlePages(page); });
        });
    };
    TutoPage.prototype.handlePages = function (page) {
        var _this = this;
        //This gives us the page's dimensions at full scale
        var viewport = page.getViewport(2);
        //We'll create a canvas for each page to draw it on
        var canvas = document.createElement("canvas");
        canvas.style.display = "block";
        var context = canvas.getContext('2d');
        canvas.height = viewport.height;
        canvas.width = viewport.width;
        //Draw it on the canvas
        page.render({ canvasContext: context, viewport: viewport });
        //Add it to the web page
        //document.body.appendChild( canvas );
        //console.log(document.getElementById("ionbody"));
        document.getElementById("ionbody").appendChild(canvas);
        var line = document.createElement("hr");
        document.getElementById("ionbody").appendChild(line);
        //console.log(this.currPage);
        //Move to next page
        this.currPageAdd();
        if (this.thePDF !== null && this.currPage <= this.numPages) {
            this.thePDF.getPage(this.currPage).then(function (page) { _this.handlePages(page); });
        }
    };
    TutoPage.prototype.currPageAdd = function () {
        this.currPage = this.currPage + 1;
    };
    TutoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tuto',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\batteryapp\src\pages\tuto\tuto.html"*/'<!-- home page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="tuto"></headpage>\n\n</ion-header>\n\n\n\n<ion-content padding >\n\n  <ion-scroll scrollY="true" style="height:100%;width:80%;left:50%;transform:translateX(-50%); " >\n\n    <div class="center" style="height:100%;">\n\n      <div id="ionbody" style="display: flex;flex-grow: 1;  flex-direction: column;background: #ddd;overflow-y: auto;" width="60%" height="100%">\n\n\n\n      </div>\n\n    </div>\n\n  </ion-scroll>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\batteryapp\src\pages\tuto\tuto.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_2__services_param_service__["a" /* ParamService */]])
    ], TutoPage);
    return TutoPage;
}());

//# sourceMappingURL=tuto.js.map

/***/ }),

/***/ 233:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParamPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api_service__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_mail_mail__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__sms_sms__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__langue_langue__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_param_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__password_password__ = __webpack_require__(236);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ParamPage = /** @class */ (function () {
    function ParamPage(navCtrl, api, param) {
        this.navCtrl = navCtrl;
        this.api = api;
        this.param = param;
        this.mailPage = __WEBPACK_IMPORTED_MODULE_3__pages_mail_mail__["a" /* MailPage */];
        this.languePage = __WEBPACK_IMPORTED_MODULE_5__langue_langue__["a" /* LanguePage */];
        this.smsPage = __WEBPACK_IMPORTED_MODULE_4__sms_sms__["a" /* SMSPage */];
        this.passwordPage = __WEBPACK_IMPORTED_MODULE_7__password_password__["a" /* PasswordPage */];
    }
    ParamPage.prototype.ngOnInit = function () {
    };
    ParamPage.prototype.goMail = function (ev) {
        ev.preventDefault();
        this.navCtrl.push(this.mailPage);
    };
    ParamPage.prototype.goSms = function (ev) {
        ev.preventDefault();
        this.navCtrl.push(this.smsPage);
    };
    ParamPage.prototype.goLangue = function (ev) {
        ev.preventDefault();
        this.navCtrl.push(this.languePage);
    };
    ParamPage.prototype.goPassword = function (ev) {
        ev.preventDefault();
        this.navCtrl.push(this.passwordPage);
    };
    ParamPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-param',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\batteryapp\src\pages\param\param.html"*/'<!-- param page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="param"></headpage>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding >\n\n  <ion-card text-center class="big_card" >\n\n      <ion-grid class="heightstyle">\n\n       \n\n          <ion-list>\n\n\n\n            <button ion-item class="btnscss" (mouseup)="goMail($event)" ><ion-icon class="iconscss" name="at" item-start></ion-icon>{{param.datatext.mails}}<ion-icon class="iconscss" name="at" item-end></ion-icon></button>\n\n            <!-- <button ion-item class="btnscss" (click)="goSms()"><ion-icon  class="iconscss" name="chatboxes" item-start></ion-icon>{{param.datatext.sms}}<ion-icon  class="iconscss" name="chatboxes" item-end></ion-icon></button> -->\n\n            <button ion-item class="btnscss" (mouseup)="goLangue($event)" ><ion-icon  class="iconscss" name="globe" item-start></ion-icon>{{param.datatext.langage}}<ion-icon  class="iconscss" name="globe" item-end></ion-icon></button>\n\n            <button ion-item class="btnscss" (mouseup)="goPassword($event)" ><ion-icon class="iconscss" name="key" item-start></ion-icon>{{param.datatext.editpswd1}}<ion-icon class="iconscss" name="key" item-end></ion-icon></button>\n\n          </ion-list>\n\n       \n\n      </ion-grid> \n\n\n\n  </ion-card>\n\n \n\n\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\batteryapp\src\pages\param\param.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_api_service__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_6__services_param_service__["a" /* ParamService */]])
    ], ParamPage);
    return ParamPage;
}());

//# sourceMappingURL=param.js.map

/***/ }),

/***/ 234:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SMSPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SMSPage = /** @class */ (function () {
    function SMSPage(param, toastCtrl) {
        this.param = param;
        this.toastCtrl = toastCtrl;
    }
    SMSPage.prototype.ngOnInit = function () {
    };
    SMSPage.prototype.errorToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toast"
        });
        toast.present();
    };
    SMSPage.prototype.okToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toastok"
        });
        toast.present();
    };
    SMSPage.prototype.removeNum = function (ev, m) {
        ev.preventDefault();
        var index = this.param.phonenumberlist.indexOf(m);
        if (index !== -1) {
            this.param.phonenumberlist.splice(index, 1);
            this.param.deletePhone(m);
            this.okToast(this.param.datatext.deletionDone);
        }
    };
    SMSPage.prototype.is_a_num = function (m) {
        var numformat = "^[0-9]{10}$";
        return m.match(numformat);
    };
    SMSPage.prototype.changeNum = function (m) {
        return "+33" + m.substring(1);
    };
    SMSPage.prototype.displayNum = function (m) {
        return "0" + m.substring(3);
    };
    SMSPage.prototype.addNum = function (ev, m) {
        ev.preventDefault();
        if (this.is_a_num(m)) {
            var index = this.param.phonenumberlist.indexOf(this.changeNum(m));
            if (index !== -1) {
                this.errorToast(this.param.datatext.numexist);
            }
            else if (this.param.phonenumberlist.length > 5) {
                this.errorToast(this.param.datatext.deletenum);
            }
            else {
                this.param.phonenumberlist.push(this.changeNum(m));
                this.param.addPhone(this.changeNum(m));
                this.okToast(this.param.datatext.numadd);
            }
        }
        else {
            this.errorToast(this.param.datatext.numincorrect);
        }
    };
    SMSPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-sms',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\batteryapp\src\pages\sms\sms.html"*/'<!-- mail page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="sms"></headpage>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding >\n\n  <ion-card text-center class="big_card" >\n\n    <ion-grid class="heightstyle">\n\n     \n\n        <ion-item >\n\n          <ion-label class="txt" stacked>{{param.datatext.newnum}}</ion-label>\n\n          <ion-input #tel  inputmode="tel" placeholder="0630854706"></ion-input>\n\n          <button *ngIf="is_a_num(tel.value)" class="btn-add" (mouseup)="addNum($event,tel.value)" item-end>\n\n            <ion-icon  color="light" name="add"></ion-icon>\n\n          </button>\n\n          <button *ngIf="!(is_a_num(tel.value))" class="mailnotvalid" (mouseup)="addNum($event,tel.value)" item-end>\n\n            <ion-icon  color="light" name="add"></ion-icon>\n\n          </button>\n\n        </ion-item>\n\n       \n\n      \n\n        <ion-list >\n\n          <ion-item *ngFor="let m of this.param.phonenumberlist">\n\n            <ion-icon class="iconscss" name="chatboxes" item-start color="primary"></ion-icon>{{displayNum(m)}}\n\n            <button class="btn-trash" item-end (mouseup)="removeNum($event,m)">\n\n              <ion-icon class="btn-trash" name="trash"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n        </ion-list>\n\n    \n\n    </ion-grid> \n\n\n\n</ion-card>\n\n \n\n\n\n</ion-content>\n\n\n\n\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\batteryapp\src\pages\sms\sms.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ToastController */]])
    ], SMSPage);
    return SMSPage;
}());

//# sourceMappingURL=sms.js.map

/***/ }),

/***/ 235:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LanguePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LanguePage = /** @class */ (function () {
    function LanguePage(param, loadingCtrl) {
        this.param = param;
        this.loadingCtrl = loadingCtrl;
        this.monitor = this.param.langage;
    }
    LanguePage.prototype.ngOnInit = function () {
    };
    LanguePage.prototype.monitorHandler = function () {
        this.loading = this.loadingCtrl.create({});
        this.loading.present(); //loading animation display
        this.param.updateRobot();
        setTimeout(function () {
            document.location.reload();
        }, 1000);
    };
    LanguePage.prototype.onSliderRelease = function (ev, id) {
        ev.preventDefault();
        id.open();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('select1'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* Select */])
    ], LanguePage.prototype, "select1", void 0);
    LanguePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-langue',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\batteryapp\src\pages\langue\langue.html"*/'<!-- langue page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="langue"></headpage>\n\n</ion-header>\n\n<ion-content padding >\n\n  <ion-grid class="main_ion_grid">\n\n    <div style="background-color: rgb(255, 255, 255); border-radius: 15px; height: 40%; width: 60%; ">\n\n      <ion-row style="height: 25%; color: rgb(0, 0, 0);justify-content: center!important; align-items: center!important; font-size: 4em;">\n\n        {{param.datatext.langage}}\n\n      </ion-row>\n\n      <ion-row style="height: 75%;vertical-align: middle;\n\n      justify-content: center;\n\n      display: flex!important;\n\n      align-items: center!important">\n\n       \n\n        <ion-select #select1 [(ngModel)]="param.robot.langage" (ionChange)="monitorHandler()" style="width: 70% !important; font-size: x-large;background-color:rgba(26, 156, 195, 0.199);" interface="popover" (mouseup)="onSliderRelease($event,select1)">\n\n          <ion-option value="de-DE">Deutsch</ion-option>\n\n          <ion-option value="en-GB">English</ion-option>\n\n          <ion-option value="el-GR">Ελληνικά</ion-option>\n\n          <ion-option value="fr-FR">Français</ion-option>\n\n          <ion-option value="es-ES">Spanish</ion-option>\n\n        </ion-select>\n\n     \n\n      </ion-row>\n\n    </div>\n\n   </ion-grid>\n\n\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\batteryapp\src\pages\langue\langue.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* LoadingController */]])
    ], LanguePage);
    return LanguePage;
}());

//# sourceMappingURL=langue.js.map

/***/ }),

/***/ 236:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_popup_service__ = __webpack_require__(41);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PasswordPage = /** @class */ (function () {
    function PasswordPage(navCtrl, popup, param) {
        this.navCtrl = navCtrl;
        this.popup = popup;
        this.param = param;
        this.newpassword = "";
        this.warn = false;
    }
    PasswordPage.prototype.ngOnInit = function () {
    };
    PasswordPage.prototype.editpassword = function (ev) {
        ev.preventDefault();
        if (this.password === atob(this.param.robot.password)) {
            if (this.newpassword.length >= 2) {
                this.popup.showToast(this.param.datatext.pswdsaved, 3000, "middle");
                this.param.robot.password = btoa(this.newpassword);
                this.param.updateRobot();
                this.password = "";
                this.newpassword = "";
                this.navCtrl.pop();
            }
        }
        else {
            this.warn = true;
        }
    };
    PasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-password',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\batteryapp\src\pages\password\password.html"*/'<!-- password page html template -->\n\n<ion-header no-border>\n\n    <headpage pageName="password"></headpage>\n\n  </ion-header>\n\n  \n\n  \n\n  <ion-content padding >\n\n    <ion-grid class="main_ion_grid">\n\n        <div style="background-color: rgb(255, 255, 255); border-radius: 15px; height: 60%; width: 60%; ">\n\n    \n\n        <ion-row style="height: 25%;justify-content: center!important; align-items: center!important;">\n\n          <ion-item style="width: 90%;">\n\n          <ion-label style="font-size: x-large; min-width: 30% !important;\n\n          max-width: 30% !important;">{{param.datatext.currentpswd}}</ion-label>\n\n          <ion-input minlength=2 maxlength=15 [(ngModel)]="password" style="font-size: xx-large; color: rgb(26, 156, 195);"  required></ion-input>\n\n       </ion-item>\n\n          \n\n        </ion-row>\n\n        \n\n    \n\n        <ion-row style="height: 25%;justify-content: center!important; align-items: center!important;">\n\n          <ion-item style="width: 90%;">\n\n          <ion-label style="font-size: x-large; min-width: 30% !important;\n\n          max-width: 30% !important;">{{param.datatext.newpswd}}</ion-label>\n\n          <ion-input [(ngModel)]="newpassword" minlength=2 maxlength=15 style="font-size: xx-large; color: rgb(26, 156, 195);" required></ion-input>\n\n          </ion-item>\n\n          \n\n        </ion-row>\n\n       <ion-row style="height: 20%; justify-content: center!important; ">\n\n          <p *ngIf="warn" style="color: red; font-size: large;">** {{param.datatext.wrongpass}} **</p>\n\n        </ion-row>\n\n        <ion-row style="height: 25%;justify-content: center!important; align-items: center!important;">\n\n          <button [disabled]="2>this.newpassword.length" ion-button solid large class="btn_login" (mouseup)="editpassword($event)"> \n\n            <!-- the text of the button depends on the page you are on -->\n\n            <font class="font_menu_size" >{{param.datatext.save}}</font>\n\n          </button>\n\n     \n\n        </ion-row>\n\n        </div>\n\n       </ion-grid>\n\n  \n\n  </ion-content>\n\n  '/*ion-inline-end:"C:\Users\laele\Documents\KomApp\batteryapp\src\pages\password\password.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__services_popup_service__["a" /* PopupService */], __WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */]])
    ], PasswordPage);
    return PasswordPage;
}());

//# sourceMappingURL=password.js.map

/***/ }),

/***/ 237:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(238);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(258);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 258:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_components_module__ = __webpack_require__(307);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_first_first__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__components_headpage_headpage__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_api_service__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__services_alert_service__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_common_http__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_background_mode__ = __webpack_require__(368);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_tuto_tuto__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__services_popup_service__ = __webpack_require__(41);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__services_param_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__services_speech_service__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_param_param__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_langue_langue__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_sms_sms__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_mail_mail__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_password_password__ = __webpack_require__(236);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_8__pages_first_first__["a" /* FirstPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_tuto_tuto__["a" /* TutoPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_langue_langue__["a" /* LanguePage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_mail_mail__["a" /* MailPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_password_password__["a" /* PasswordPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_sms_sms__["a" /* SMSPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_param_param__["a" /* ParamPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_5__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_12__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */], {}, {
                    links: []
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_8__pages_first_first__["a" /* FirstPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_tuto_tuto__["a" /* TutoPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_param_param__["a" /* ParamPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_langue_langue__["a" /* LanguePage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_mail_mail__["a" /* MailPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_sms_sms__["a" /* SMSPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_password_password__["a" /* PasswordPage */],
                __WEBPACK_IMPORTED_MODULE_9__components_headpage_headpage__["a" /* HeadpageComponent */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_background_mode__["a" /* BackgroundMode */],
                __WEBPACK_IMPORTED_MODULE_10__services_api_service__["a" /* ApiService */],
                __WEBPACK_IMPORTED_MODULE_11__services_alert_service__["a" /* AlertService */],
                __WEBPACK_IMPORTED_MODULE_15__services_popup_service__["a" /* PopupService */],
                __WEBPACK_IMPORTED_MODULE_16__services_param_service__["a" /* ParamService */],
                __WEBPACK_IMPORTED_MODULE_17__services_speech_service__["a" /* SpeechService */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] },
            ],
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 307:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_component__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__headpage_headpage__ = __webpack_require__(220);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_3__headpage_headpage__["a" /* HeadpageComponent */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */])],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */]
            ],
            exports: [__WEBPACK_IMPORTED_MODULE_3__headpage_headpage__["a" /* HeadpageComponent */]]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 313:
/***/ (function(module, exports) {

module.exports = {"quit":"QUITTER","battery":"BATTERIE","return":"RETOUR","tutorial":"TUTORIEL","param":"PARAMÈTRES","receivesms":"RECEVOIR DES ALERTES SMS","receivemail":"RECEVOIR DES ALERTES PAR MAIL","changelangage":"CHANGER LA LANGUE","godocking":"EN ROUTE VERS LA STATION DE CHARGE","round":"TOURNÉE DE DIVERTISSEMENT","patrol":"Patrouille","patrolInProgress":"PATROUILLE EN COURS","roundInProgress":"TOURNÉE DE DIVERTISSEMENT EN COURS","moving":"DÉPLACEMENT EN COURS VERS :","charging_remaining":"ROBOT EN CHARGE - BATTERIE: ","walkInProgress":"MARCHEZ A VOTRE RYTHME SANS POUSSER","notpush":"INSTALLEZ VOUS COMME SUR LA PHOTO ET APPUYEZ SUR «ALLONS-Y»","mails":"Mails","sms":"SMS","langage":"Langue","distance_covered":"Distance parcourue","goto":"Se rendre au :","inProgress":"EN COURS","post":"Poste","sentinel":"Sentinelle","btn_go":"LANCER","btn_help":"Aide","btn_walk":"Allons-y !","btn_stop":"STOP","btn_charge":"CHARGER","btn_cancel":"Annuler","btn_ok":"OK","btn_yes":"OUI","btn_no":"NON","numexist":"Ce numéro existe déjà !","deletenum":"Veuillez supprimer un numéro d'abord","numadd":"Numéro ajouté","numincorrect":"Numéro invalide","newnum":"Nouveau numéro","mailexist":"Cette adresse existe déjà ! ","deletemail":"Veuillez supprimer une adresse d'abord","mailadd":"Adresse mail ajoutée !","mailincorrect":"Adresse mail invalide","newmail":"Nouvelle adresse","manualreloc":"Reloc manuelle","reloc":"RELOC","qrcode":"Choisir un QR code","unknownqrcode":"QR code non reconnu","relocalizing":"Relocalisation en cours ...","alertReloc":"Prière de ne pas déplacer le robot pendant la relocalisation","localizationDone":"Robot localisé sur map : ","deletionDone":"Suppression réussie","error":"Erreur","cantlaunch":"Impossible de démarrer l'application. Vérifiez que le robot est allumé correctement.","nointernet":"Pas d'internet","connectedToI":"Le robot est connecté à internet","btn_save":"Sauver","receivepic":"Recevoir des photos","editpswd":"CHANGER LE MOT DE PASSE","currentpswd":"Mot de passe actuel :","newpswd":"Nouveau mot de passe :","save":"Sauvegarder","success":"C'est Fait !","pswdsaved":"Mot de passe sauvegardé","langage1":"Changer la langue","editpswd1":"Changer le mot de passe","wrongpass":"Mauvais mot de passe","smsSOS":"Quelqu'un a appuyé sur le bouton SOS du robot","mailSOS_suj":"SOS","mailSOS_body":"<br> Une personne a besoin d'aide","mailBattery_suj":"Batterie Faible","mailBattery_body":"<br> Le robot a besoin d'etre chargé. Veuillez le mettre sur sa station de charge.","mailBlocked_suj":"Robot bloqué","mailBlocked_body":"<br> Le robot est bloqué <br>","mailFall_body":"Le robot détecte une personne à terre <br>","mailFall_suj":"CHUTE détectée","mailPerson_suj":"Personne détectée","mailPerson_body":"Le robot a détecté une personne <br>","presentAlert_title":"Robot en charge","presentAlert_message":"Attention je vais reculer !","presentConfirm_title":"Ronde suspendue","presentConfirm_message":"Reprendre la ronde ?","FallConfirm_title":"Chute détectée !","FallConfirm_message":"Reprendre la patrouille ?","RemoteConfirm_title":"Ronde arrêtée à distance","RemoteConfirm_message":"Reprendre la ronde ?","blockedAlert_title":"Robot bloqué !","blockedAlert_message":"Veuillez enlever l'obstacle ou me déplacer","goDockingConfirm_title":"Batterie: ","goDockingConfirm_message":"Aller en station de charge ?","lowBattery_title":"Batterie critique","lowBattery_message":"Veuillez amener le robot à sa station de charge.","errorlaunchAlert_title":"Une erreur est survenue","errorlaunchAlert_message":"Veuillez rententer de lancer la ronde","robotmuststayondocking_title":"Batterie faible","robotmuststayondocking_message":"Le robot doit rester sur la docking","errorNavAlert_title":"Une erreur est survenue","errorNavAlert_message":"Veuillez appeler le support technique si le problème persiste","lostAlert_title":"Robot perdu !","lostAlert_message":"Veuillez appeler le support technique","quitConfirm_title":"Quitter l'application ?","errorBlocked_title":"Robot bloqué","errorBlocked_message":"Veuillez vérifier que le robot est bien dégagé","statusRedPresent_title":"Erreur","statusRedPresent_message":"Veuillez me redémarrer OU appeler mon fabricant si le problème persiste","statusGreenPresent_title":"Statut vert","statusGreenPresent_message":"Le robot est en bonne santé !","leaveDockingConfirm_title":"Batterie: ","leaveDockingConfirm_message":"Sortir de la station de charge ?","leaveDockingAlert_title":"Robot en charge","leaveDockingAlert_message":"Veuillez me retirer de la station de charge","askHelp_title":"Aide demandée","askHelp_message":"Quelqu'un va venir vous aider","map":"%      CARTE : ","password":"Mot de passe","enterPassword":"Nécessaire pour accéder aux paramètres.","wrongPassword":"Mot de passe incorrect. Veuillez réessayer.","nodocking":"Le robot ne sait pas où est la station de charge","URL_battery":"assets/pdf/tuto_FR.pdf","rgpd_txt":"Conformément à la loi du 6 janvier 1978 modifiée et au « Règlement Général de Protection des Données », vos données personnelles de contact sont collectées afin que vous puissiez recevoir les alertes du robot Kompai (personne détectée, robot bloqué, batterie faible..).  Vos données seront traitées par Korian et Kompai pendant la durée requise pour l’utilisation de la plateforme. Vous pouvez, notamment, vous opposez au traitement de vos données,  obtenir une copie de vos données, les rectifier ou les supprimer en écrivant au DPO et en justifiant votre identité à l’adresse : rgpd@kompai.com ou KOMPAI Robotics, Technopole d'Izarbel - 97 allée Théodore Monod 64210 Bidart - France.","noqrdetected":"Aucun QR code détecté"}

/***/ }),

/***/ 314:
/***/ (function(module, exports) {

module.exports = {"quit":"LEAVE","battery":"BATTERY","return":"RETURN","tutorial":"TUTORIAL","param":"SETTINGS","receivesms":"RECEIVE ALERTS BY SMS","receivemail":"RECEIVE ALERTS BY EMAIL","changelangage":"CHANGE THE LANGUAGE","godocking":"ON THE WAY TO THE CHARGING STATION","moving":"TRAVELING IN PROGRESS TO :","charging_remaining":"ROBOT IN CHARGE - BATTERY: ","mails":"Emails","sms":"SMS","langage":"Language","goto":"Go to :","inProgress":"IN PROGRESS","post":"Position","btn_go":"GO","btn_walk":"Let's go !","btn_stop":"STOP","btn_charge":"LOAD","btn_cancel":"Cancel","btn_ok":"OK","btn_yes":"YES","btn_no":"NO","numexist":"This phone number already exists !","deletenum":"Please delete a phone number first","numadd":"Phone number added","numincorrect":"Invalid phone number","newnum":"New phone number","mailexist":"This address already exists ! ","deletemail":"Please delete an address first","mailadd":"Email address added!","mailincorrect":"Invalid email address","newmail":"New address","manualreloc":"Manual reloc","reloc":"RELOC","qrcode":"Select a QR code","unknownqrcode":"Unknown QR code","relocalizing":"Relocalizing ...","alertReloc":"Please do not move the robot during the relocalization","localizationDone":"Robot located on map : ","deletionDone":"Deletion done","error":"Error","cantlaunch":"Application launching failed. Check that the robot is correctly switched on.","nointernet":"No internet access","connectedToI":"The robot is connected to internet","btn_save":"Save","receivepic":"Receive pictures","editpswd":"EDIT PASSWORD","currentpswd":"Current password :","newpswd":"New password :","save":"Save","success":"Success","pswdsaved":"Password saved","langage1":"Langage","editpswd1":"Edit password","wrongpass":"Wrong password","smsSOS":"Someone pressed the robot's SOS button","mailSOS_suj":"SOS","mailSOS_body":"<br> A person needs help","mailBattery_suj":"Low Battery","mailBattery_body":"<br> The robot needs to be charged. Please put it on its charging station.","mailBlocked_suj":"Robot blocked","mailBlocked_body":"<br> The robot is blocked <br>","presentAlert_title":"Robot in charge","presentAlert_message":"I'm going to back up !","blockedAlert_title":"Robot blocked !","blockedAlert_message":"Please remove the obstacle or move me","goDockingConfirm_title":"Battery: ","goDockingConfirm_message":"Go to the charging station ?","lowBattery_title":"Low Battery","lowBattery_message":"Please take the robot to its charging station.","errorlaunchAlert_title":"A mistake has occured","errorlaunchAlert_message":"Please try again to start the round","robotmuststayondocking_title":"Low Battery","robotmuststayondocking_message":"The robot must stay on the docking","errorNavAlert_title":"A mistake has occured","errorNavAlert_message":"Please call technical support if the problem persists","lostAlert_title":"Robot lost !","lostAlert_message":"Please call technical support","quitConfirm_title":"Leave the application ?","errorBlocked_title":"Robot blocked","errorBlocked_message":"Please check that the robot is clear","statusRedPresent_title":"Error","statusRedPresent_message":"Please restart me OR call my manufacturer if the problem persists","statusGreenPresent_title":"Green status","statusGreenPresent_message":"The robot is healthy !","leaveDockingConfirm_title":"Battery: ","leaveDockingConfirm_message":"Get out of the charging station ?","leaveDockingAlert_title":"Robot in charge","leaveDockingAlert_message":"Please, remove me from the charging station","askHelp_title":"Help requested","askHelp_message":"Someone is going to come and help you","map":"%      MAP : ","password":"Password","enterPassword":"Required to access settings.","wrongPassword":"Wrong password. Please try again.","URL_battery":"assets/pdf/tuto_EN.pdf","nodocking":"The robot does not know where the charging station is","rgpd_txt":"In accordance with the amended law of 6 January 1978 and the «General Data Protection Regulations», your personal contact data are collected so that you can receive alerts from the Kompai robot (person detected, robot blocked, low battery, etc.). Your data will be processed by Korian and Kompai for the time required to use the platform. You can, in particular, oppose the processing of your data, obtain a copy of your data, rectify or delete them by writing to the DPO and justifying your identity at: rgpd@kompai.com or KOMPAI Robotics, Technopole d 'Izarbel - 97 allée Théodore Monod 64210 Bidart - France.","noqrdetected":"No QR detected"}

/***/ }),

/***/ 315:
/***/ (function(module, exports) {

module.exports = {"quit":"SALIR","battery":"BATERÍA","return":"REGRESAR","tutorial":"TUTORIAL","param":"PARÁMETROS","receivesms":"RECIBIR ALERTAS POR SMS","receivemail":"RECIBIR ALERTAS POR CORREO ELECTRÓNICO","changelangage":"CAMBIAR EL IDIOMA","godocking":"EN CAMINO A LA ESTACÍON DE CARGA","round":"RECORRIDO DE ENTRETENIMIENTO","patrol":"Patrulla","patrolInProgress":"PATRULLANDO","roundInProgress":"RECORRIDO DE ENTRETENIMIENTO EN CURSO","moving":"EN MOVIMIENTO PARA :","charging_remaining":"ROBOT EN CARGA - BATERÍA: ","walkInProgress":"CAMINE A SU PROPIO RITMO SIN EMPUJAR","notpush":"COLÓQUESE COMO EN LA FOTO Y PRENSE «EMPEZAR»","mails":"Correos electrónicos","sms":"SMS","langage":"Idioma","distance_covered":"Distancia recorrida","goto":"Ir a :","inProgress":"EN CURSO","post":"Posición","sentinel":"Centinela","btn_go":"INICIAR","btn_help":"Ayuda","btn_walk":"Empezar","btn_stop":"ALTO","btn_charge":"CARGAR","btn_cancel":"Cancelar","btn_ok":"OK","btn_yes":"SÍ","btn_no":"NO","numexist":"¡ Este número ya existe !","deletenum":"Por favor, borre un número primero","numadd":"Número añadido","numincorrect":"Número inválido","newnum":"Nuevo número","mailexist":"¡ Esta dirección de correo electrónico ya existe ! ","deletemail":"Por favor, elimine primero una dirección de correo electrónico","mailadd":"¡ Dirección de correo electrónico añadida !","mailincorrect":"Dirección de correo electrónico inválida","newmail":"Nueva dirección de correo electrónico","manualreloc":"Manual reloc","reloc":"RELOC","qrcode":"Elegir un código QR","unknownqrcode":"Código QR no reconocido","relocalizing":"Relocalizando ...","alertReloc":"Por favor, no mueva el robot durante la relocalización","localizationDone":"Robot localizado en el mapa : ","deletionDone":"Supresión realizada","error":"Error","cantlaunch":"No se puede iniciar la aplicación. Compruebe si el robot está bien encendido.","nointernet":"Sin acceso a internet","connectedToI":"El robot está conectado a internet","btn_save":"Guardar","receivepic":"Recibir fotos","editpswd":"EDITAR CONTRASEÑA","currentpswd":"Contraseña actual :","newpswd":"Nueva contraseña :","save":"Guardar","success":"Está hecho !","pswdsaved":"Contraseña guardada","langage1":"Idioma","editpswd1":"Editar contraseña","wrongpass":"Contraseña incorrecta","smsSOS":"Alguien ha pulsado el botón SOS del robot","mailSOS_suj":"SOS","mailSOS_body":"<br> Una persona necesita ayuda","mailBattery_suj":"Batería Baja","mailBattery_body":"<br> Hay que cargar al robot. Por favor, pongalo en su estación de carga.","mailBlocked_suj":"Robot bloqueado","mailBlocked_body":"<br> El robot está bloqueado <br>","mailFall_body":"El robot detecta a una persona en el suelo <br>","mailFall_suj":"CAÍDA detectada","mailPerson_suj":"Persona detectada","mailPerson_body":"El robot detectó a una persona <br>","presentAlert_title":"Robot en carga","presentAlert_message":"¡ Voy a ir hacia atrás !","presentConfirm_title":"Ronda suspendida","presentConfirm_message":"¿ Volver a la ronda ?","FallConfirm_title":"¡ Caída detectada !","FallConfirm_message":"¿ Volver a la patrulla ?","RemoteConfirm_title":"Ronda parada a distancia","RemoteConfirm_message":"¿ Volver a la ronda ?","blockedAlert_title":"¡ Robot bloqueado !","blockedAlert_message":"Por favor, quite el obstáculo o muevame","goDockingConfirm_title":"Batería: ","goDockingConfirm_message":"¿ Ir a la estación de carga ?","lowBattery_title":"Batería Baja","lowBattery_message":"Lleve el robot a su estación de carga.","errorlaunchAlert_title":"Se ha producido un error","errorlaunchAlert_message":"Intente volver a empezar la ronda, por favor","robotmuststayondocking_title":"Batería Baja","robotmuststayondocking_message":"El robot debe quedarse en el atraque","errorNavAlert_title":"Se ha producido un error","errorNavAlert_message":"Por favor, llame al soporte técnico si el problema persiste","lostAlert_title":"¡ Robot perdido !","lostAlert_message":"Por favor, llame al soporte técnico","quitConfirm_title":"¿ Salir de la aplicación ?","errorBlocked_title":"Robot bloqueado","errorBlocked_message":"Compruebe si el robot está desbloqueado","statusRedPresent_title":"Error","statusRedPresent_message":"Por favor, reinicie me o llame a mi fabricante si el problema persiste","statusGreenPresent_title":"Situación verde","statusGreenPresent_message":"¡ El robot está sano !","leaveDockingConfirm_title":"Batería: ","leaveDockingConfirm_message":"¿ Salir de la estación de carga ?","leaveDockingAlert_title":"Robot cargando","leaveDockingAlert_message":"Por favor, retireme de la estación de carga","askHelp_title":"Ayuda solicitada","askHelp_message":"Alguien vendrá a ayudarte","map":"%      MAPA : ","password":"Contraseña","enterPassword":"Requerido para acceder a la configuración.","wrongPassword":"Contraseña incorrecta. Por favor, intente de nuevo.","nodocking":"El robot no sabe dónde está la estación de carga","URL_battery":"assets/pdf/tuto_EN.pdf","rgpd_txt":"De acuerdo con la ley modificada del 6 de enero de 1978 y el «Reglamento general de protección de datos», sus datos de contacto personales se recopilan para que pueda recibir alertas del robot Kompai (persona detectada, robot bloqueado, batería baja, etc.). Korian y Kompai procesarán sus datos durante el tiempo necesario para utilizar la plataforma. Puede, en particular, oponerse al tratamiento de sus datos, obtener una copia de sus datos, rectificarlos o suprimirlos escribiendo al DPO y justificando su identidad en: rgpd@kompai.com o KOMPAI Robotics, Technopole d 'Izarbel - 97 allée Théodore Monod 64210 Bidart - Francia.","noqrdetected":"Ningún QR detectado"}

/***/ }),

/***/ 316:
/***/ (function(module, exports) {

module.exports = {"quit":"BEENDEN","battery":"BATTERIE","return":"ZURÜCK","tutorial":"ANLEITUNG","param":"EINSTELLUNGEN","receivesms":"BENACHRICHTIGUNG ALS SMS ERHALTEN","receivemail":"BENACHRICHTIGUNG ALS E-MAIL ERHALTEN","changelangage":"SPRACHE ÄNDERN","godocking":"AUF DEM WEG ZUR LADESTATION","round":"ENTERTAINMENT RUNDE","patrol":"Kontrollfahrt","patrolInProgress":"KONTROLLFAHRT WIRD DURCHGEFÜHRT","roundInProgress":"ENTERTAINMENT RUNDE WIRD DURCHGEFÜHRT","moving":"AUF DEM WEG ZU :","charging_remaining":"ROBOTER LÄDT AUF - BATTERIE: ","walkInProgress":"GEHE IN DEINEM EIGENEN TEMPO OHNE ANSCHIEBEN","notpush":"STELLE DICH AUF, WIE AUF DEM FOTO ZU SEHEN, UND DRÜCKE «LOS GEHT'S»","mails":"E-Mails","sms":"SMS","langage":"Sprache","distance_covered":"Zurückgelegte Strecke","goto":"Gehe zu:","inProgress":"WIRD DURCHGEFÜHRT","post":"Position","sentinel":"Überwachung","btn_go":"LOS","btn_help":"Hilfe","btn_walk":"Los geht's!","btn_stop":"STOP","btn_charge":"LADEN","btn_cancel":"Abbrechen","btn_ok":"OK","btn_yes":"JA","btn_no":"NEIN","numexist":"Diese Telefonnummer existiert bereits!","deletenum":"Bitte lösche zuerst eine Telefonnummer","numadd":"Telefonnummer hinzugefügt","numincorrect":"Ungültige Telefonnummer","newnum":"Neue Telefonnummer","mailexist":"Diese Adresse  existiert bereits ! ","deletemail":"Bitte lösche zuerst eine Adresse","mailadd":"E-Mail Adresse hinzugefügt!","mailincorrect":"Ungültige E-Mail Adresse","newmail":"Neue Adresse","manualreloc":"Manuelle Relokalisierung","reloc":"QR-Code","qrcode":"Wähle einen QR-Code","unknownqrcode":"Unbekannter QR-Code","relocalizing":"Relokalisierung ...","alertReloc":"Bitte bewege den Roboter nicht während der Relokalisierung","localizationDone":"Roboter ist auf der Karte lokalisiert: ","deletionDone":"Löschen erledigt","error":"Fehler","cantlaunch":"Start der Anwendung ist fehlgeschlagen. Prüfe, ob der Roboter korrekt eingeschaltet ist.","nointernet":"Keine Internetverbindung","connectedToI":"Der Roboter ist mit dem Internet verbunden","btn_save":"Sichern","receivepic":"Fotos erhalten","editpswd":"PASSWORT BEARBEITEN","currentpswd":"Aktuelles Passwort :","newpswd":"Neues Kennwort :","save":"Speichern","success":"Erfolg","pswdsaved":"Passwort gespeichert","langage1":"Sprache","editpswd1":"Passwort bearbeiten","wrongpass":"Falsches Passwort","smsSOS":"Der SOS Knopf wurde gedrückt","mailSOS_suj":"SOS","mailSOS_body":"<br> Eine Person benötigt Hilfe","mailBattery_suj":"Batterie schwach","mailBattery_body":"<br> Der Roboter muss aufgeladen werden. Bitte stelle ihn auf die Ladestation.","mailBlocked_suj":"Roboter ist blockiert","mailBlocked_body":"<br> Der Roboter ist blockiert <br>","mailFall_body":"Der Roboter erkennt eine Person <br>","mailFall_suj":"STURZ erkannt","mailPerson_suj":"Person erkannt","mailPerson_body":"Der Roboter hat eine Person erkannt <br>","presentAlert_title":"Roboter lädt auf","presentAlert_message":"Ich gehe zurück !","presentConfirm_title":"Runde unterbrochen","presentConfirm_message":"Runde fortführen?","FallConfirm_title":"Sturz erkannt!","FallConfirm_message":"Kontrollfahrt fortführen ?","RemoteConfirm_title":"Ferngesteuert Runde stoppen","RemoteConfirm_message":"Runde fortführen?","blockedAlert_title":"Roboter ist blockiert !","blockedAlert_message":"Bitte entferne das Hindernis oder bewege mich","goDockingConfirm_title":"Batterie: ","goDockingConfirm_message":"Zur Ladestation gehen  ?","lowBattery_title":"Batterie schwach","lowBattery_message":"Bitte bringen Sie den Roboter zu seiner Ladestation.","errorlaunchAlert_title":"Ein Fehler ist aufgetreten","errorlaunchAlert_message":"Bitte versuche erneut die Runde zu starten","robotmuststayondocking_title":"Batterie schwach","robotmuststayondocking_message":"Der Roboter muss auf der Ladestation bleiben","errorNavAlert_title":"Ein Fehler ist aufgetreten","errorNavAlert_message":"Bitte kontaktiere den technischen Support wenn das Problem weiterhin besteht","lostAlert_title":"Roboter hat sich verirrt !","lostAlert_message":"Bitte kontaktiere den technischen Support","quitConfirm_title":"Die Anwendung beenden ?","errorBlocked_title":"Roboter ist blockiert","errorBlocked_message":"Bitte prüfe, ob der Roboter frei ist","statusRedPresent_title":"Fehler","statusRedPresent_message":"Bitte starte mich neu ODER kontaktiere meinen Hersteller wenn das Problem weiterhin besteht","statusGreenPresent_title":"Grüner Status","statusGreenPresent_message":"Der Roboter ist in Ordnung !","leaveDockingConfirm_title":"Batterie: ","leaveDockingConfirm_message":"Die Ladestation verlassen ?","leaveDockingAlert_title":"Roboter lädt auf","leaveDockingAlert_message":"Bitte entferne mich von der Ladestation","askHelp_title":"Hilfe ist angefordert","askHelp_message":"Jemand kommt um zu helfen","map":"%      KARTE : ","password":"Passwort","enterPassword":"Erforderlich für den Zugriff auf die Einstellungen.","wrongPassword":"Falsches Passwort. Bitte versuche es erneut.","nodocking":"Der Roboter weiß nicht, wo die Ladestation ist","URL_battery":"assets/pdf/tuto_EN.pdf","rgpd_txt":"In Übereinstimmung mit dem geänderten Gesetz vom 6. Januar 1978 und der « Allgemeinen Datenschutzverordnung » werden Ihre persönlichen Kontaktdaten gesammelt, damit Sie Benachrichtigungen vom Kompai-Roboter erhalten können (Person erkannt, Roboter blockiert, schwache Batterie usw.). Ihre Daten werden von Korian und Kompai für den Zeitraum verarbeitet, der für die Nutzung der Plattform erforderlich ist. Sie können sich insbesondere der Verarbeitung Ihrer Daten widersetzen, eine Kopie Ihrer Daten erhalten, diese berichtigen oder löschen lassen, indem Sie sich schriftlich und unter Angabe Ihrer Identität an den Datenschutzbeauftragten wenden: rgpd@kompai.com or KOMPAI Robotics, Technopole d 'Izarbel - 97 allée Théodore Monod 64210 Bidart - France.","noqrdetected":"Kein QR erkannt"}

/***/ }),

/***/ 317:
/***/ (function(module, exports) {

module.exports = {"quit":"ΕΞΟΔΟΣ","battery":"ΜΠΑΤΑΡΙΑ","return":"ΕΠΙΣΤΡΟΦΗ","tutorial":"ΕΚΠΑΙΔΕΥΣΗ","param":"ΡΥΘΜΙΣΕΙΣ","receivesms":"ΛΗΨΗ ΕΙΔΟΠΟΙΗΣΕΩΝ ΜΕ SMS","receivemail":"ΛΗΨΗ ΕΙΔΟΠΟΙΗΣΕΩΝ ΜΕ EMAIL","changelangage":"ΑΛΛΑΓΗ ΓΛΩΣΣΑΣ","godocking":"ΣΤΟ ΔΡΟΜΟ ΓΙΑ ΦΟΡΤΙΣΗ","moving":"ΜΕΤΑΚΙΝΕΙΤΑΙ ΓΙΑ ΝΑ :","charging_remaining":"ΤΟ ΡΟΜΠΟΤ ΦΟΡΤΙΖΕΙ - ΜΠΑΤΑΡΙΑ: ","mails":"Emails","sms":"SMS","langage":"ΓΛΩΣΣΑ","goto":"ΠΗΓΑΙΝΕ ΣΕ :","inProgress":"ΣΕ ΕΞΕΛΙΞΗ","post":"ΘΕΣΗ","btn_go":"ΠΗΓΑΙΝΕ","btn_walk":"ΞΕΚΙΝΑ","btn_stop":"ΣΤΑΜΑΤΑ","btn_charge":"ΦΟΡΤΩΣΗ","btn_cancel":"ΑΚΥΡΩΣΗ","btn_ok":"ΕΝΤΑΞΕΙ","btn_yes":"ΝΑΙ","btn_no":"ΟΧΙ","numexist":"ΑΥΤΟ ΤΟ ΤΗΛΕΦΩΝΟ ΗΔΗ ΥΠΑΡΧΕΙ !","deletenum":"ΠΑΡΑΚΑΛΩ ΣΒΗΣΤΕ ΤΟΝ ΑΡΙΘΜΟ ΤΗΛΕΦΩΝΟΥ ΠΡΩΤΑ","numadd":"ΑΡΙΘΜΟΣ ΤΗΛΕΦΩΝΟΥ ΠΡΟΣΤΕΘΗΚΕ","numincorrect":"ΛΑΘΟΣ ΑΡΙΘΜΟΣ ΤΗΛΕΦΩΝΟΥ","newnum":"ΝΕΟΣ ΤΗΛΕΦΩΝΙΚΟΣ ΑΡΙΘΜΟΣ","mailexist":"ΑΥΤΗ Η ΔΙΕΥΘΥΝΣΗ ΗΔΗ ΥΠΑΡΧΕΙ ! ","deletemail":"ΠΑΡΑΚΑΛΩ ΣΒΗΣΤΕ ΤΗΝ ΔΙΕΥΘΥΝΣΗ ΠΡΩΤΑ","mailadd":"ΠΡΟΣΤΕΘΗΚΕ ΔΙΕΥΘΥΝΣΗ Email !","mailincorrect":"ΛΑΘΟΣ ΔΙΕΥΘΥΝΣΗ email","newmail":"ΝΕΑ ΔΙΕΥΘΥΝΣΗ","manualreloc":"ΧΕΙΡΟΚΙΝΗΤΗ ΜΕΤΑΦΟΡΑ","reloc":"ΜΕΤΑΦΟΡΑ","qrcode":"ΕΠΙΛΟΓΗ QR ΚΩΔΙΚΟΥ","unknownqrcode":"ΑΓΝΩΣΤΟΣ QR ΚΩΔΙΚΟΣ","relocalizing":"ΜΕΤΑΦΟΡΑ ...","alertReloc":"ΠΑΡΑΚΑΛΩ ΜΗ ΜΕΤΑΚΙΝΕΙΤΕ ΤΟ ΡΟΜΠΟΤ ΚΑΤΑ ΤΗ ΔΙΑΡΚΕΙΑ ΜΕΤΑΦΟΡΑΣ","localizationDone":"ΤΟ ΡΟΜΠΟΤ ΤΟΠΟΘΕΤΗΘΗΚΕ ΣΤΟΝ ΧΑΡΤΗ : ","deletionDone":"Η ΔΙΑΓΡΑΦΗ ΕΓΙΝΕ","error":"ΛΑΘΟΣ","cantlaunch":"ΑΠΟΤΥΧΙΑ ΕΚΚΙΝΗΣΗΣ ΕΦΑΡΜΟΓΗΣ. ΕΛΕΓΞΤΕ ΟΤΙ ΤΟ ΡΟΜΠΟΤ ΑΝΟΙΞΕ ΣΩΣΤΑ.","nointernet":"ΔΕΝ ΥΠΑΡΧΕΙ ΠΡΟΣΒΑΣΗ ΣΤΟ ΙΝΤΕΡΝΕΤ","connectedToI":"ΤΟ ΡΟΜΠΟΤ ΣΥΝΔΕΘΗΚΕ ΣΤΟ ΙΝΤΕΡΝΕΤ","btn_save":"ΑΠΟΘΗΚΕΥΣΗ","receivepic":"ΛΗΨΗ ΦΩΤΟΓΡΑΦΙΩΝ","editpswd":"ΔΙΟΡΘΩΣΗ ΚΩΔΙΚΟΥ","currentpswd":"ΤΡΕΧΟΝ ΚΩΔΙΚΟΣ:","newpswd":"ΝΕΟΣ ΚΩΔΙΚΟΣ:","save":"ΑΠΟΘΗΚΕΥΣΗ","success":"ΕΠΙΤΥΧΙΑ","pswdsaved":"Ο ΚΩΔΙΚΟΣ ΑΠΟΘΗΚΕΥΤΗΚΕ","langage1":"ΓΛΩΣΣΑ","editpswd1":"ΔΙΟΡΘΩΣΗ ΚΩΔΙΚΟΥ","wrongpass":"ΛΑΘΟΣ ΚΩΔΙΚΟΣ","smsSOS":"ΚΑΠΟΙΟΣ ΠΑΤΗΣΕ ΤΟ ΚΟΥΜΠΙ SOS ΤΟΥ ΡΟΜΠΟΤ","mailSOS_suj":"SOS","mailSOS_body":"<br> ΚΑΠΟΙΟΣ ΘΕΛΕΙ ΒΟΗΘΕΙΑ","mailBattery_suj":"ΧΑΜΗΛΗ ΜΠΑΤΑΡΙΑ","mailBattery_body":"<br> ΤΟ ΡΟΜΠΟΤ ΠΡΕΠΕΙ ΝΑ ΦΟΡΤΙΣΤΕΙ. ΠΑΡΑΚΑΛΩ ΤΟΠΟΘΕΤΗΣΤΕ ΤΟ ΣΤΟ ΣΤΑΘΜΟ ΦΟΡΤΙΣΗΣ.","mailBlocked_suj":"ΤΟ ΡΟΜΠΟΤ ΜΠΛΟΚΑΡΙΣΤΗΚΕ","mailBlocked_body":"<br> ΤΟ ΡΟΜΠΟΤ ΕΙΝΑΙ ΜΠΛΟΚΑΡΙΣΜΕΝΟ <br>","presentAlert_title":"ΤΟ ΡΟΜΠΟΤ ΦΟΡΤΙΖΕΤΑΙ","presentAlert_message":"ΠΡΟΚΕΙΤΑΙ ΝΑ ΚΑΝΩ ΑΝΤΙΓΡΑΦΟ ΑΣΦΑΛΕΙΑΣ !","blockedAlert_title":"ΤΟ ΡΟΜΠΟΤ ΜΠΛΟΚΑΡΙΣΤΗΚΕ !","blockedAlert_message":"ΠΑΡΑΚΑΛΩ ΑΦΑΙΡΕΣΤΕ ΤΟ ΕΜΠΟΔΙΟ Ή ΜΕΤΑΚΙΝΗΣΤΕ ΜΕ","goDockingConfirm_title":"ΜΠΑΤΑΡΙΑ: ","goDockingConfirm_message":"ΠΗΓΑΙΝΕ ΣΤΟ ΣΤΑΘΜΟ ΦΟΡΤΙΣΗΣ ?","lowBattery_title":"ΧΑΜΗΛΗ ΜΠΑΤΑΡΙΑ","lowBattery_message":"ΠΑΡΑΚΑΛΩ ΠΗΓΑΙΝΕΤΕ ΤΟ ΡΟΜΠΟΤ ΣΤΟ ΣΤΑΘΜΟ ΦΟΡΤΙΣΗΣ ΤΟΥ.","errorlaunchAlert_title":"ΚΑΠΟΙΟ ΛΑΘΟΣ ΕΧΕΙ ΣΥΜΒΕΙ","errorlaunchAlert_message":"ΠΑΡΑΚΑΛΩ ΠΡΟΣΠΑΘΗΣΤΕ ΞΑΝΑ ","robotmuststayondocking_title":"ΧΑΜΗΛΗ ΜΠΑΤΑΡΙΑ","robotmuststayondocking_message":"ΤΟ ΡΟΜΠΟΤ ΠΡΕΠΕΙ ΝΑ ΜΕΙΝΕΙ ΣΤΟ ΣΤΑΘΜΟ ΦΟΡΤΙΣΗΣ","errorNavAlert_title":"ΕΝΑ ΛΑΘΟΣ ΕΧΕΙ ΣΥΜΒΕΙ","errorNavAlert_message":"ΠΑΡΑΚΑΛΩ ΚΑΛΕΣΤΕ ΤΕΧΝΙΚΗ ΥΠΟΣΤΗΡΙΞΗ ΕΑΝ ΤΟ ΠΡΟΒΛΗΜΑ ΠΑΡΑΜΕΝΕΙ","lostAlert_title":"ΤΟ ΡΟΜΠΟΤ ΧΑΘΗΚΕ !","lostAlert_message":"ΠΑΡΑΚΑΛΩ ΚΑΛΕΣΤΕ ΤΕΧΝΙΚΗ ΥΠΟΣΤΗΡΙΞΗ","quitConfirm_title":"ΕΞΟΔΟΣ ΑΠΟ ΤΗΝ ΕΦΑΡΜΟΓΗ ?","errorBlocked_title":"ΤΟ ΡΟΜΠΟΤ ΜΠΛΟΚΑΡΙΣΤΗΚΕ","errorBlocked_message":"ΠΑΡΑΚΑΛΩ ΕΛΕΓΞΤΕ ΟΤΙ ΤΟ ΡΟΜΠΟΤ ΕΙΝΑΙ ΕΛΕΥΘΕΡΟ","statusRedPresent_title":"ΛΑΘΟΣ","statusRedPresent_message":"ΠΑΡΑΚΑΛΩ ΕΠΑΝΕΚΚΙΝΗΣΤΕ ΜΕ Ή ΚΑΛΕΣΤΕ ΤΟΝ ΚΑΤΑΣΚΕΥΑΣΤΗ ΜΟΥ ΕΑΝ ΤΟ ΠΡΟΒΛΗΜΑ ΠΑΡΑΜΕΝΕΙ","statusGreenPresent_title":"ΠΡΑΣΙΝΗ ΚΑΤΑΣΤΑΣΗ","statusGreenPresent_message":"ΤΟ ΡΟΜΠΟΤ ΕΊΝΑΙ ΥΓΙΕΣ !","leaveDockingConfirm_title":"ΜΠΑΤΑΡΙΑ: ","leaveDockingConfirm_message":"ΝΑ ΦΥΓΩ ΑΠΟ ΤΟΝ ΣΤΑΘΜΟ ΦΟΡΤΙΣΗΣ ?","leaveDockingAlert_title":"ΤΟ ΡΟΜΠΟΤ ΣΕ ΦΟΡΤΙΣΗ","leaveDockingAlert_message":"ΠΑΡΑΚΑΛΩ, ΑΦΑΙΡΕΣΤΕ ΜΕ ΑΠΟ ΤΟΝ ΣΤΑΘΜΟ ΦΟΡΤΙΣΗΣ","askHelp_title":"ΖΗΤΗΘΗΚΕ ΒΟΗΘΕΙΑ","askHelp_message":"ΚΑΠΟΙΟΣ ΘΑ ΕΡΘΕΙ ΝΑ ΣΕ ΒΟΗΘΗΣΕΙ","map":"%      ΧΑΡΤΗΣ : ","password":"ΚΩΔΙΚΟΣ","enterPassword":"ΑΠΑΙΤΕΙΤΑΙ ΓΙΑ ΠΡΟΣΒΑΣΗ ΣΤΙΣ ΡΥΘΜΙΣΕΙΣ.","wrongPassword":"ΛΑΘΟΣ ΚΩΔΙΚΟΣ. ΠΑΡΑΚΑΛΩ ΔΟΚΙΜΑΣΤΕ ΞΑΝΑ.","URL_battery":"assets/pdf/tuto_EN.pdf","nodocking":"ΤΟ ΡΟΜΠΟΤ ΔΕΝ ΞΕΡΕΙ ΠΟΥ ΕΙΝΑΙ Ο ΣΤΑΘΜΟΣ ΦΟΡΤΙΣΗΣ","rgpd_txt":"Σύμφωνα με τον τροποποιημένο νόμο του Ιανουαριου 1978 και το «Γενικό Κανονισμό Προστασίας Δεδομένων», Τα προσωπικά δεδομένα επικοινωνίας σας συλλέγονται έτσι ώστε να μπορείτε να λαμβάνετε ειδοποιήσεις από το KOMPAI ρομπότ (ανιχνεύθηκε άτομο, μπλοκαρίστηκε το ρομπότ, χαμηλή μπαταρία κ.λπ.). Τα δεδομένα σας θα υποβληθούν σε επεξεργασία από την Κorian και Kompai για το χρόνο που απαιτείται για τη χρήση της πλατφόρμας. Μπορείτε, ειδικότερα, να αντιταχθείτε στην επεξεργασία των δεδομένων σας, να λάβετε αντίγραφο των δεδομένων σας, διορθώστε ή διαγράψτε τα, γράφοντας προς τον Υπεύθυνο Επεξεργασίας Δεδομένων και πιστοποιώντας την ταυτότητά σας: rgpd@kompai.com ή KOMPAI Robotics, Technopole d 'Izarbel - 97 allée Théodore Monod 64210 Bidart - Γαλλία.","noqrdetected":"Δεν ανιχνεύθηκε QR"}

/***/ }),

/***/ 326:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 328:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 33:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Battery */
/* unused harmony export Statistics */
/* unused harmony export Anticollision */
/* unused harmony export Iostate */
/* unused harmony export Docking */
/* unused harmony export Round */
/* unused harmony export Differential */
/* unused harmony export Localization */
/* unused harmony export Navigation */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__alert_service__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__param_service__ = __webpack_require__(13);
// Allows Http communication with komnav
// cf Marc komnav_http_manual for more information
// https://drive.google.com/drive/u/3/folders/1-18kyKv6Ep-fzPU1F-dp0tQ1r8usyuZ7
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





var Battery = /** @class */ (function () {
    function Battery() {
    }
    return Battery;
}());

var Statistics = /** @class */ (function () {
    function Statistics() {
    }
    return Statistics;
}());

var Anticollision = /** @class */ (function () {
    function Anticollision() {
    }
    return Anticollision;
}());

var Iostate = /** @class */ (function () {
    function Iostate() {
    }
    return Iostate;
}());

var Docking = /** @class */ (function () {
    function Docking() {
    }
    return Docking;
}());

/*
{ Docking status
  "Unknown    ",
  "Undocked   ",
  "Docking    ",
  "Undocking  ",
  "Error      "
};*/
var Round = /** @class */ (function () {
    function Round() {
    }
    return Round;
}());

var Differential = /** @class */ (function () {
    function Differential() {
    }
    return Differential;
}());

var Localization = /** @class */ (function () {
    function Localization() {
    }
    return Localization;
}());

var Navigation = /** @class */ (function () {
    function Navigation() {
    }
    return Navigation;
}());

// The status can have the following values:
// • 0 - Waiting: the robot is ready for a new destination speciﬁed with a PUT /navigation/destination request.
// • 1 - Following: the robot is following the trajectory computed to join the destination.
// • 2 - Aiming: the robot has completed the trajectory and is now rotating to aim the destination accurately
// • 3 - Translating: the robot has ﬁnished aiming and is now translating to reach the destination
// • 4 - Rotating: this is the last rotation, executed only if the destination was speciﬁed with “Rotate” : true
// • 5-Error: indicatesthattherobothasencounteredanerrorthatpreventedittojointherequireddestination.
// This can be cleared by sending a new destination.
var ApiService = /** @class */ (function () {
    function ApiService(httpClient, app, alert, param) {
        this.httpClient = httpClient;
        this.app = app;
        this.alert = alert;
        this.param = param;
        this.socketok = false;
        this.is_connected = false; // to know if we are connected to the robot
        this.is_background = false;
        //this.mapLoaded=false;
        this.is_localized = false;
        this.roundActive = false; // to know if the round is in process
        this.towardDocking = false; //to know if the robot is moving toward the docking
        this.towardPOI; //to know if th robot is moving toward a poi for the sentinel mode
        this.start = false;
        this.fct_startRound = false;
        this.fct_onGo = false;
        this.statusRobot = 0;
        this.cpt_jetsonOK = 0;
        this.close_app = false;
        this.appOpened = false;
        this.textHeadBand = "";
        this.robotok = false;
    }
    ApiService.prototype.ngOnInit = function () { };
    ApiService.prototype.instanciate = function () {
        this.socketok = true;
        var apiserv = this;
        ////////////////////// localization socket
        this.localization_status = new Localization();
        this.localization_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/localization/socket", "json");
        this.localization_socket.onerror = function (event) {
            console.log("error localization socket");
        };
        this.localization_socket.onopen = function (event) {
            console.log("localization socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.localization_status.positionx = test["Pose"]["X"];
                apiserv.localization_status.positiony = test["Pose"]["Y"];
                apiserv.localization_status.positiont = test["Pose"]["T"];
            };
            this.onclose = function () {
                console.log("localization socket closed");
            };
        };
        ////////////////////// docking socket
        this.docking_status = new Docking();
        this.docking_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/docking/socket", "json");
        this.docking_socket.onerror = function (event) {
            console.log("error docking socket");
        };
        this.docking_socket.onopen = function (event) {
            console.log("docking socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.docking_status.status = test["Status"];
                apiserv.docking_status.detected = test["Detected"];
            };
            this.onclose = function () {
                console.log("docking socket closed");
            };
        };
        ////////////////////// differential socket
        this.differential_status = new Differential();
        this.differential_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/differential/socket", "json");
        this.differential_socket.onerror = function (event) {
            console.log("error differential socket");
        };
        this.differential_socket.onopen = function (event) {
            console.log("differential socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.differential_status.status = test["Status"];
                apiserv.differential_status.TargetLinearSpeed = test["TargetLinearSpeed"];
                apiserv.differential_status.TargetAngularSpeed = test["TargetAngularSpeed"];
            };
            this.onclose = function () {
                console.log("differential socket closed");
            };
        };
        ////////////////////// navigation socket
        this.navigation_status = new Navigation();
        this.navigation_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/navigation/socket", "json");
        this.navigation_socket.onerror = function (event) {
            console.log("error navigation socket");
        };
        this.navigation_socket.onopen = function (event) {
            console.log("navigation socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.navigation_status.status = test["Status"];
                apiserv.navigation_status.avoided = test["Avoided"];
            };
            this.onclose = function () {
                console.log("navigation socket closed");
            };
        };
        ////////////////////// anticollision socket
        this.anticollision_status = new Anticollision();
        this.anticollision_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/anticollision/socket", "json");
        this.anticollision_socket.onerror = function (event) {
            console.log("error anticollision socket");
        };
        this.anticollision_socket.onopen = function (event) {
            console.log("anticollision socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.anticollision_status.timestamp = test["Timestamp"];
                apiserv.anticollision_status.enabled = test["Enabled"];
                apiserv.anticollision_status.locked = test["Locked"];
                apiserv.anticollision_status.forward = parseInt(test["Forward"]);
                apiserv.anticollision_status.right = parseInt(test["Right"]);
                apiserv.anticollision_status.left = parseInt(test["Left"]);
                apiserv.anticollision_status.reverse = parseInt(test["Reverse"]);
            };
            this.onclose = function () {
                console.log("anticollision socket closed");
            };
        };
        ////////////////////// statistics socket
        this.statistics_status = new Statistics();
        this.statistics_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/statistics/socket", "json");
        this.statistics_socket.onerror = function (event) {
            console.log("error statistic socket");
        };
        this.statistics_socket.onopen = function (event) {
            console.log("statistic socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.statistics_status.totalTime = test["TotalTime"];
                apiserv.statistics_status.totalDistance = test["TotalDistance"];
                apiserv.statistics_status.timestamp = test["Timestamp"];
            };
            this.onclose = function () {
                console.log("statistic socket closed");
            };
        };
        ///////////////////////// battery socket
        this.battery_status = new Battery();
        this.battery_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/battery/socket", "json");
        this.battery_socket.onerror = function (event) {
            console.log("error battery socket");
        };
        this.battery_socket.onopen = function (event) {
            console.log("battery socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.battery_status.autonomy = test["Autonomy"];
                apiserv.battery_status.current = test["Current"];
                apiserv.battery_status.remaining = test["Remaining"];
                apiserv.battery_status.status = test["Status"];
                apiserv.battery_status.timestamp = test["Timestamp"];
                apiserv.battery_status.voltage = test["Voltage"];
            };
            this.onclose = function () {
                console.log("battery socket closed");
            };
        };
        //////////////////////// round socket
        this.round_status = new Round();
        this.round_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/rounds/socket", "json");
        this.round_socket.onerror = function (event) {
            console.log("error round socket");
        };
        this.round_socket.onopen = function (event) {
            console.log("round socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.round_status.round = test["Round"];
                apiserv.round_status.acknowledge = test["Acknowledge"];
                apiserv.round_status.abort = test["Abort"];
                apiserv.round_status.pause = test["Pause"];
                apiserv.round_status.status = test["Status"];
            };
            this.onclose = function () {
                console.log("round socket closed");
            };
        };
        //////////////////////////////// io socket
        this.b_pressed = false;
        this.btnPush = false;
        this.io_status = new Iostate();
        this.io_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/io/socket", "json");
        this.io_socket.onerror = function (event) {
            console.log("error iosocket");
        };
        this.io_socket.onopen = function (event) {
            console.log("io socket opened");
            this.onclose = function () {
                console.log("io socket closed");
            };
        };
        this.io_socket.onmessage = function (event) {
            var test = JSON.parse(event.data);
            apiserv.io_status.timestamp = test["Timestamp"];
            apiserv.io_status.dIn = test["DIn"];
            apiserv.io_status.aIn = test["AIn"];
            if (apiserv.io_status.dIn[2] ||
                apiserv.io_status.dIn[3] ||
                apiserv.io_status.dIn[8]) {
                //smart button and button A of the game pad
                if (!apiserv.b_pressed) {
                    apiserv.btnPush = true;
                    apiserv.b_pressed = true;
                }
            }
            else {
                if (apiserv.b_pressed) {
                    apiserv.b_pressed = false;
                }
            }
        };
        //////////////////////////////// ROS TOPIC connection
        if (this.param.robot.cam_USB == 0) {
            this.ros = new ROSLIB.Ros({
                url: 'ws://' + this.param.robot.rosip
            });
            this.ros.on('connection', function () {
                console.log('Connected to ros websocket server.');
            });
            this.ros.on('error', function (error) {
                console.log('Error connecting to ros websocket server: ', error);
            });
            this.ros.on('close', function () {
                console.log('Connection to ros websocket server closed.');
            });
            //////////////////////////////// ROS TOPIC qrcode
            this.listener_qr = new ROSLIB.Topic({
                ros: this.ros,
                name: '/barcode',
                messageType: 'std_msgs/String'
            });
            //////////////////////////////// ROS TOPIC cam
            this.listener_camera = new ROSLIB.Topic({
                ros: this.ros,
                name: '/top_webcam/image_raw/compressed',
                //name : '/fisheye_cam/image_raw',
                //name : '/D435_camera_FWD/color/image_raw',
                //name : '/fisheye_cam/compressed',
                messageType: 'sensor_msgs/CompressedImage'
            });
        }
    };
    ApiService.prototype.mailAddInformationBasic = function () {
        var now = new Date().toLocaleString("en-GB", {
            day: "numeric",
            month: "numeric",
            year: "numeric",
            hour: "numeric",
            minute: "numeric",
            second: "numeric",
        });
        return ("<br> Time : " +
            now +
            "<br> Serial number : " +
            this.param.serialnumber +
            "<br> Battery remaining: " +
            this.battery_status.remaining +
            "%" +
            "<br> Battery state : " +
            this.battery_status.status +
            "<br> Battery voltage : " +
            this.battery_status.voltage +
            "<br> Battery current : " +
            this.battery_status.current +
            "<br> Battery autonomy : " +
            this.battery_status.autonomy +
            "<br> Docking state : " +
            this.docking_status.status +
            "<br> Status Forward : " +
            this.anticollision_status.forward +
            "<br> Status Right : " +
            this.anticollision_status.right +
            "<br> Status Left : " +
            this.anticollision_status.left +
            "<br> Status Reverse : " +
            this.anticollision_status.reverse +
            "<br> Navigation state : " +
            this.navigation_status.status +
            "<br> Differential state : " +
            this.differential_status.status +
            "<br> Odometer : " +
            this.statistics_status.totalDistance +
            "<br> Position X : " +
            this.localization_status.positionx +
            "<br> Position Y : " +
            this.localization_status.positiony +
            "<br> Position T : " +
            this.localization_status.positiont +
            "<br> Application : 4" +
            "<br>");
    };
    ApiService.prototype.checkrobot = function () {
        var _this = this;
        this.httpClient
            .get(this.httpskomnav + this.param.localhost + "/api/battery/state")
            .subscribe(function (data) {
            _this.robotok = true;
        }, function (err) {
            console.log(err);
            _this.robotok = false;
        });
    };
    ApiService.prototype.jetsonOK = function () {
        var _this = this;
        this.checkInternet();
        this.alert.checkwifi(this.wifiok);
        this.httpClient
            .get(this.httpskomnav + this.param.localhost + "/api/battery/state", {
            observe: "response",
        })
            .subscribe(function (resp) {
            if (_this.statusRobot === 2) {
                _this.alert.appError(_this.mailAddInformationBasic());
                document.location.reload(); //if error then refresh the page and relaunch websocket
            }
            if (resp.status === 200) {
                _this.cpt_jetsonOK = 0;
                _this.statusRobot = 0; // everything is ok
                _this.connect();
            }
            else {
                _this.cpt_jetsonOK += 1;
                if (_this.cpt_jetsonOK > 5) {
                    _this.statusRobot = 2; //no connection
                    _this.connectionLost();
                }
            }
        }, function (err) {
            console.log(err); //no conection
            _this.cpt_jetsonOK += 1;
            if (_this.cpt_jetsonOK > 10) {
                _this.statusRobot = 2;
                _this.connectionLost();
            }
        });
    };
    ApiService.prototype.pauseHttp = function () {
        // suspend the round
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/rounds/current/pause", {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("pauseOK");
        }, function (err) {
            console.log(err);
            console.log("pausePBM");
        });
    };
    ApiService.prototype.isConnectedInternet = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, text, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        return [4 /*yield*/, fetch('http://localhost/ionicDB/internetping.php')];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, response.text()];
                    case 2:
                        text = _a.sent();
                        //console.log(text);
                        // Analyse du texte pour déterminer la connectivité Internet
                        return [2 /*return*/, text.trim() === 'true']; // Renvoie true si le texte est 'true', sinon false
                    case 3:
                        error_1 = _a.sent();
                        console.error('Error checking internet connectivity:', error_1);
                        return [2 /*return*/, false];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ApiService.prototype.checkInternet = function () {
        var _this = this;
        this.isConnectedInternet().then(function (connecte) {
            if (connecte) {
                //console.log("L'ordinateur est connecté à Internet");
                _this.wifiok = true;
            }
            else {
                //console.log("L'ordinateur n'est pas connecté à Internet");
                _this.wifiok = false;
            }
        });
    };
    ApiService.prototype.connectHttp = function () {
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/docking/connect", {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("connectOK");
        }, function (err) {
            console.log(err);
            console.log("connectPBM");
        });
    };
    ApiService.prototype.disconnectHttp = function () {
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/docking/disconnect", {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("disconnectOK");
        }, function (err) {
            console.log(err);
            console.log("disconnectPBM");
        });
    };
    ApiService.prototype.abortDockingHttp = function () {
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/docking/abort", {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("abortdockingOK");
        }, function (err) {
            console.log(err);
            console.log("abortdockingPBM");
        });
    };
    ApiService.prototype.getCurrentMap = function () {
        var _this = this;
        this.httpClient
            .get(this.httpskomnav + this.param.localhost + "/api/maps/current/properties")
            .subscribe(function (data) {
            console.log(data);
            _this.mapdata = data;
            //console.log(this.mapdata.Id);
            if (_this.mapdata) {
                _this.id_current_map = _this.mapdata.Id;
            }
        }, function (err) {
            console.log(err);
        });
    };
    ApiService.prototype.changeMapbyIdHttp = function () {
        this.httpClient
            .put(this.httpskomnav +
            this.param.localhost +
            "/api/maps/current/id/" +
            this.QRselected.id_map, { observe: "response" })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("changeMapOK");
        }, function (err) {
            console.log(err);
            console.log("changeMapPBM");
        });
    };
    ApiService.prototype.resetLocationHttp = function () {
        var _this = this;
        var pose = {
            X: this.firstlocation[0].Pose.X,
            Y: this.firstlocation[0].Pose.Y,
            T: this.firstlocation[0].Pose.T,
        };
        var body = JSON.stringify(pose);
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ "Content-Type": "application/json" });
        var options = { headers: headers };
        return new Promise(function (resolve) {
            _this.httpClient
                .put(_this.httpskomnav + _this.param.localhost + "/api/localization/pose", body, options)
                .subscribe(function (resp) {
                console.log(resp);
                console.log("resetlocationOK");
            }, function (error) {
                console.log(error);
                console.log("resetlocationPBM");
            });
        });
    };
    ApiService.prototype.getFirstLocationHttp = function () {
        var _this = this;
        this.httpClient
            .get(this.httpskomnav + this.param.localhost + "/api/maps/current/locations")
            .subscribe(function (data) {
            console.log(data);
            _this.locationdata = data;
            if (_this.locationdata) {
                _this.firstlocation = _this.locationdata.filter(function (x) { return x.Id == _this.QRselected.id_poi; });
            }
        }, function (err) {
            console.log(err);
        });
    };
    ApiService.prototype.goDockingHttp = function () {
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/navigation/dock", {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("godockingOK");
        }, function (err) {
            console.log(err);
            console.log("godockingPBM");
        });
    };
    ApiService.prototype.reachHttp = function (poiname) {
        this.httpClient
            .put(this.httpskomnav +
            this.param.localhost +
            "/api/navigation/destination/name/" +
            poiname, { observe: "response" })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("reachOK");
        }, function (err) {
            console.log(err);
            console.log("reachPBM");
        });
    };
    ApiService.prototype.abortHttp = function () {
        // stop the round
        this.start = false;
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/rounds/current/abort", {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("abortOK");
        }, function (err) {
            console.log(err);
            console.log("abortPBM");
        });
    };
    ApiService.prototype.abortNavHttp = function () {
        // stop the navigation
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/navigation/abort", {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("abortNavOK");
        }, function (err) {
            console.log(err);
            console.log("abortNavPBM");
        });
    };
    ApiService.prototype.resumeHttp = function () {
        // resume round
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/rounds/current/resume", {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("resumeOK");
        }, function (err) {
            console.log(err);
            console.log("resumePBM");
        });
    };
    ApiService.prototype.acknowledgeHttp = function () {
        //go to next poi
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/rounds/current/acknowledge", { observe: "response" })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("acknowledgeOK");
        }, function (err) {
            console.log(err);
            console.log("acknowledgePBM");
        });
    };
    ApiService.prototype.startRoundHttp = function (idRound) {
        // start the round
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/rounds/current/id/" + idRound, { observe: "response" })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("startRoundOK");
        }, function (err) {
            console.log(err);
            console.log("startRoundPBM");
        });
    };
    ApiService.prototype.connect = function () {
        this.is_connected = true;
    };
    ApiService.prototype.connectionLost = function () {
        this.is_connected = false;
    };
    ApiService.prototype.background = function () {
        this.is_background = true;
    };
    ApiService.prototype.foreground = function () {
        this.is_background = false;
    };
    ApiService.prototype.eyesHttp = function (id) {
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/eyes?id=" + id, {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("eyesOK");
        }, function (err) {
            console.log(err);
            console.log("eyesPBM");
        });
    };
    ApiService.prototype.deleteEyesHttp = function (id) {
        this.httpClient
            .delete(this.httpskomnav + this.param.localhost + "/api/eyes?id=" + id, {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("eyesOK");
        }, function (err) {
            console.log(err);
            console.log("eyesPBM");
        });
    };
    ApiService.prototype.poiDocking = function () {
        var _this = this;
        this.httpClient
            .get(this.httpskomnav + this.param.localhost + "/api/maps/current/locations")
            .subscribe(function (data) {
            _this.dockingdata = data;
            console.log(_this.dockingdata.filter(function (x) { return x.Name == "docking"; }));
            if (_this.dockingdata.filter(function (x) { return x.Name == "docking"; }).length > 0) {
                console.log("docking exist");
                _this.dockingexist = true;
                _this.docking = _this.dockingdata.filter(function (x) { return x.Name == "docking"; })[0];
            }
            else {
                _this.dockingexist = false;
            }
        }, function (err) {
            console.log(err);
        });
    };
    ApiService.prototype.joystickHttp = function (lin, rad) {
        var _this = this;
        var cmd = { Enable: true, TargetLinearSpeed: lin, TargetAngularSpeed: rad };
        //console.log(lin, rad);
        var body = JSON.stringify(cmd);
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ "Content-Type": "application/json" });
        var options = { headers: headers };
        return new Promise(function (resolve) {
            _this.httpClient
                .put(_this.httpskomnav + _this.param.localhost + "/api/differential/command", body, options)
                .subscribe(function (resp) {
                //console.log(resp);
                console.log("joystickOK");
            }, function (error) {
                console.log(error);
                console.log("joystickPBM");
            });
        });
    };
    ApiService.prototype.startQrDetection = function (start) {
        var serviceQrDetection = new ROSLIB.Service({
            ros: this.ros,
            name: '/toggle_barcode_detection_node',
            serviceType: 'std_srvs/SetBool'
        });
        var request = new ROSLIB.ServiceRequest({
            data: start,
        });
        serviceQrDetection.callService(request, function (result) {
            console.log("*********service qr****************");
            console.log(result);
        });
        if (!start) {
            this.listener_qr.removeAllListeners();
        }
    };
    ApiService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_3__alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_4__param_service__["a" /* ParamService */]])
    ], ApiService);
    return ApiService;
}());

//# sourceMappingURL=api.service.js.map

/***/ }),

/***/ 348:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 349:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 350:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 365:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 40:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__param_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(60);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// to send mail and sms alert




// function send mail in index.html
//declare var envoyerSMS;
// var in index.html
//declare var messageAenvoyer;
//declare var numTelToElement;
var AlertService = /** @class */ (function () {
    function AlertService(app, param, http) {
        this.app = app;
        this.param = param;
        this.http = http;
        this.videoWidth = 0;
        this.videoHeight = 0;
        this.constraints = {
            //audio: false,
            //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
            video: true,
            facingMode: "environment",
            width: { ideal: 1920 },
            height: { ideal: 1080 }
        };
    }
    AlertService.prototype.ngOnInit = function () {
    };
    AlertService.prototype.handleError = function (error) {
        console.log('Error: ', error);
    };
    AlertService.prototype.checkwifi = function (bool) {
        this.allowmail = bool;
    };
    // function send mail without image
    // bod : corps du mail
    // suj : sujet du mail
    // dest : adresse mail du destinataire
    AlertService.prototype.sendmailPerso = function (bod, suj, dest, data, usern, passw, robotn) {
        var _this = this;
        //console.log("sendmail");
        var usermailData = {
            username: usern,
            email: data,
            bcc: dest,
            date: new Date(Date.now()).toDateString(),
            subject: suj,
            message: bod,
            password: passw,
            robotname: robotn
        };
        var link = "http://localhost/ionicDB/SendMail/sendmail.php";
        var postParams = JSON.stringify(usermailData);
        //console.log(postParams);
        if (this.allowmail) {
            this.http.post(link, postParams).subscribe(function (response) {
                //console.log(response);
                //console.log(this.param.durationNS);
                if (response == "OK") {
                    var s = _this.param.serialnumber + ' : Duration App';
                    if (suj == s) {
                        _this.param.updateDurationNS(_this.param.durationNS[0].id_duration);
                        _this.param.durationNS.shift();
                        //console.log(this.param.durationNS);
                    }
                }
            }, function (error) {
                console.log(error);
            });
        }
    };
    AlertService.prototype.sendAlertData = function (bod, suj) {
        this.sendmailPerso(bod, suj, this.param.datamaillist, this.param.datamail, this.param.datamail, this.param.maildatapassw, this.param.datamail);
    };
    AlertService.prototype.sendAlertClient = function (bod, suj) {
        if (this.param.maillist.length) {
            this.sendmailPerso(bod, suj, this.param.maillist, this.param.datamail, this.param.robotmail, this.param.mailrobotpassw, this.param.name);
        }
    };
    // function send mail with image
    // bod : corps du mail
    // suj : sujet du mail
    // dest : adresse mail du destinataire
    // url : image attachment
    AlertService.prototype.sendmailImgperso = function (url, bod, suj, dest, data, usern, passw, robotn) {
        //console.log("sendmail");
        var usermailData = {
            username: usern,
            email: data,
            bcc: dest,
            date: new Date(Date.now()).toDateString(),
            subject: suj,
            message: bod,
            password: passw,
            robotname: robotn,
            url: url
        };
        var link = "http://localhost/ionicDB/SendMail/sendmailImg.php";
        var postParams = JSON.stringify(usermailData);
        //console.log(postParams);
        if (this.allowmail) {
            this.http.post(link, postParams).subscribe(function (response) {
                console.log(response);
            }, function (error) {
                console.log(error);
            });
        }
    };
    AlertService.prototype.sendAlertImgData = function (url, bod, suj) {
        this.sendmailImgperso(url, bod, suj, this.param.datamail, this.param.datamail, this.param.datamail, this.param.maildatapassw, this.param.datamail);
    };
    AlertService.prototype.sendAlertImgClient = function (url, bod, suj) {
        if (this.param.maillist.length) {
            this.sendmailImgperso(url, bod, suj, this.param.maillist, this.param.datamail, this.param.robotmail, this.param.mailrobotpassw, this.param.name);
        }
    };
    // function send sms
    AlertService.prototype.sendsmsperso = function (mess, num) {
        //messageAenvoyer = mess;
        //numTelToElement = num;
        //envoyerSMS();
    };
    AlertService.prototype.reloc = function (info) {
        console.log("reloc alert");
        this.sendAlertData('<br> Someone wants to relocate the robot' +
            '<br> Code alert : 27' +
            info, this.param.serialnumber + ' : Reloc');
    };
    AlertService.prototype.duration = function (olddata, info) {
        console.log("duration alert");
        this.sendAlertData('<br> Here is a count of the duration of use of the apps in minutes' +
            '<br> Date duration : ' + olddata.date +
            '<br> Round duration : ' + olddata.round +
            '<br> Patrol duration : ' + olddata.patrol +
            '<br> Walk duration : ' + olddata.walk +
            '<br> Battery duration : ' + olddata.battery +
            '<br> Toolbox duration : ' + olddata.toolbox +
            '<br> Logistic duration : ' + olddata.logistic +
            '<br> Code alert : 26' +
            info, this.param.serialnumber + ' : Duration App');
    };
    AlertService.prototype.appError = function (info) {
        this.sendAlertData('<br> An error occurred, the round application has been reload automatically' +
            '<br> Code alert : 1' +
            info, this.param.serialnumber + ' : Error (App Battery)');
    };
    AlertService.prototype.charging = function (info) {
        this.sendAlertData('<br> The robot has docked successfully' +
            '<br> Code alert : 2' +
            info, this.param.serialnumber + ' : Charging');
    };
    AlertService.prototype.lowBattery = function (info) {
        this.sendAlertData('<br> The robot must be sent to the docking station' +
            '<br> Code alert : 5' +
            info, this.param.serialnumber + ' : Battery Critical');
    };
    AlertService.prototype.remoteStop = function (info) {
        this.sendAlertData('<br> The navigation has been abort remotely' +
            '<br> Code alert : 14' +
            info, this.param.serialnumber + ' : Remote control');
    };
    AlertService.prototype.errorblocked = function (info) {
        this.sendAlertData('<br> The robot has been stopped because the rated current is exceeded' +
            '<br> Code alert : 6' +
            info, this.param.serialnumber + ' : High current');
    };
    AlertService.prototype.leaveDocking = function (info) {
        this.sendAlertData('<br> Kompai is no longer on docking' +
            '<br> Code alert : 11' +
            info, this.param.serialnumber + ' : Leave docking');
    };
    AlertService.prototype.roundCompleted = function (info) {
        this.sendAlertData('<br> Kompai has been running since 1h now. The Patrol is completed. The robot must move towards docking' +
            '<br> Code alert : 15' +
            +info, this.param.serialnumber + ' : Patrol completed');
    };
    AlertService.prototype.displayTuto = function (info) {
        this.sendAlertData('<br> Someone is reading the tuto' +
            '<br> Code alert : 10' +
            info, this.param.serialnumber + ' : Tuto opened');
    };
    AlertService.prototype.noLongerBlocked = function (info) {
        this.sendAlertData('<br> The robot is no longer blocked' +
            '<br> Code alert : 3' +
            info, this.param.serialnumber + ' : No more blocked');
    };
    AlertService.prototype.naverror = function (info) {
        this.sendAlertData('<br> A navigation error has occurred. The robot may be lost. The nominal current may have been exceeded.' +
            '<br> Code alert : 4' +
            info, this.param.serialnumber + ' : Navigation error');
    };
    AlertService.prototype.robotLost = function (info) {
        this.sendAlertData('<br> The robot is lost. It must be relocated' +
            '<br> Code alert : 7' +
            info, this.param.serialnumber + ' : Robot Lost');
    };
    AlertService.prototype.blockingdocking = function (info) {
        this.sendAlertData('<br> The robot encountered an obstacle while heading for the docking station' +
            '<br> Code alert : 8' +
            info, this.param.serialnumber + " : Can't reach docking");
    };
    AlertService.prototype.roundLaunched = function (info) {
        this.sendAlertData('<br> Somebody has started a patrol' +
            '<br> Code alert : 16' +
            info, this.param.serialnumber + ' : Patrol launched');
    };
    AlertService.prototype.roundFailed = function (info) {
        this.sendAlertData('<br> The Patrol has not run correctly' +
            '<br> Code alert : 17' +
            info, this.param.serialnumber + ' : Patrol failed');
    };
    AlertService.prototype.roundPaused = function (info) {
        this.sendAlertData('<br> Somebody has paused the patrol in progress' +
            '<br> Code alert : 18' +
            info, this.param.serialnumber + ' : Patrol paused');
    };
    AlertService.prototype.askCharge = function (info) {
        this.sendAlertData('<br> The robot must move towards docking' +
            '<br> Code alert : 12' +
            info, this.param.serialnumber + ' : Charging request');
    };
    AlertService.prototype.appClosed = function (info) {
        this.sendAlertData('<br> Somebody has closed the battery application' +
            '<br> Code alert : 13' +
            info, this.param.serialnumber + ' : App Battery Closed');
    };
    AlertService.prototype.appOpen = function (info) {
        this.sendAlertData('<br> Someone oppened the battery app.' +
            '<br> Code alert : 24' +
            info, this.param.serialnumber + ' : App opened (Battery)');
    };
    /////////////////////////////////// Mail client
    AlertService.prototype.SOS_c = function () {
        this.sendAlertClient(this.param.datatext.mailSOS_body, this.param.datatext.mailSOS_suj);
    };
    AlertService.prototype.Battery_c = function () {
        this.sendAlertClient(this.param.datatext.mailBattery_body, this.param.datatext.mailBattery_suj);
    };
    AlertService.prototype.Blocked_c = function (url) {
        this.sendAlertImgClient(url, this.param.datatext.mailBlocked_body, this.param.datatext.mailBlocked_suj);
    };
    AlertService.prototype.Block_c = function () {
        this.sendAlertClient(this.param.datatext.mailBlocked_body, this.param.datatext.mailBlocked_suj);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('video'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], AlertService.prototype, "videoElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('canvas'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], AlertService.prototype, "canvas", void 0);
    AlertService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_2__param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */]])
    ], AlertService);
    return AlertService;
}());

//# sourceMappingURL=alert.service.js.map

/***/ }),

/***/ 41:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopupService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__alert_service__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__api_service__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__param_service__ = __webpack_require__(13);
// to send mail and sms alert
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var PopupService = /** @class */ (function () {
    function PopupService(app, param, alert, api, alertCtrl, toastCtrl) {
        this.app = app;
        this.param = param;
        this.alert = alert;
        this.api = api;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
    }
    PopupService.prototype.onSomethingHappened1 = function (fn) {
        this.accessparam = fn;
    };
    PopupService.prototype.onSomethingHappened2 = function (fn) {
        this.addMail = fn;
    };
    PopupService.prototype.ngOnInit = function () { };
    PopupService.prototype.startFailedAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.error,
            message: this.param.datatext.cantlaunch,
            cssClass: "alertstyle",
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("Oui clicked");
                        window.close();
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.presentAlert = function () {
        var _this = this;
        // pop up if the robot is in the docking when you start the round
        var alert = this.alertCtrl.create({
            title: this.param.datatext.presentAlert_title,
            message: this.param.datatext.presentAlert_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: this.param.datatext.btn_cancel,
                    cssClass: "cancelpopup",
                    role: "cancel",
                    handler: function () {
                        console.log("cancel clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("Yes clicked");
                        _this.api.roundActive = true;
                        _this.api.fct_startRound = true;
                        setTimeout(function () {
                            _this.alert.roundLaunched(_this.api.mailAddInformationBasic());
                        }, 3000);
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.presentConfirm = function () {
        var _this = this;
        // pop up code to resume or stop the round
        var alert = this.alertCtrl.create({
            title: this.param.datatext.presentConfirm_title,
            message: this.param.datatext.presentConfirm_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_no,
                    role: "cancel",
                    handler: function () {
                        console.log("Non clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_yes,
                    handler: function () {
                        console.log("Oui clicked");
                        _this.api.fct_onGo = true;
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.FallConfirm = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.param.datatext.FallConfirm_title,
            message: this.param.datatext.FallConfirm_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_no,
                    role: "cancel",
                    handler: function () {
                        console.log("Non clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_yes,
                    handler: function () {
                        console.log("Oui clicked");
                        _this.api.fct_onGo = true;
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.RemoteConfirm = function () {
        var _this = this;
        // pop up code to resume or stop the round
        var alert = this.alertCtrl.create({
            title: this.param.datatext.RemoteConfirm_title,
            message: this.param.datatext.RemoteConfirm_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_no,
                    role: "cancel",
                    handler: function () {
                        console.log("No clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_yes,
                    handler: function () {
                        console.log("Yes clicked");
                        _this.api.fct_onGo = true;
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.blockedAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.blockedAlert_title,
            message: this.param.datatext.blockedAlert_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("Oui clicked");
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.lowBattery = function () {
        var alert = this.alertCtrl.create({
            title: this.param.datatext.lowBattery_title,
            message: this.param.datatext.lowBattery_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log('ok clicked');
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.errorlaunchAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.errorlaunchAlert_title,
            message: this.param.datatext.errorlaunchAlert_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        //console.log('Oui clicked');
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.robotmuststayondocking = function () {
        // pop up if the robot is in the docking when you start the round
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.robotmuststayondocking_title,
            message: this.param.datatext.robotmuststayondocking_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log('Oui clicked');
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.errorNavAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.errorNavAlert_title,
            message: this.param.datatext.errorNavAlert_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log('Oui clicked');
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.lostAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.lostAlert_title,
            message: this.param.datatext.lostAlert_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log('Oui clicked');
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.quitConfirm = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.param.datatext.quitConfirm_title,
            cssClass: "alertstyle",
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: this.param.datatext.btn_no,
                    role: "cancel",
                    handler: function () {
                        //console.log('Non clicked');
                    },
                },
                {
                    text: this.param.datatext.btn_yes,
                    handler: function () {
                        //console.log('Oui clicked');
                        _this.api.close_app = true;
                        _this.alert.appClosed(_this.api.mailAddInformationBasic());
                        //stop the round and quit the app
                        _this.api.abortHttp();
                        setTimeout(function () {
                            window.close();
                        }, 1000);
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.errorBlocked = function () {
        var alert = this.alertCtrl.create({
            title: this.param.datatext.errorBlocked_title,
            message: this.param.datatext.errorBlocked_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log('OK clicked');
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.statusRedPresent = function () {
        var alert = this.alertCtrl.create({
            title: this.param.datatext.statusRedPresent_title,
            message: this.param.datatext.statusRedPresent_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("OK clicked");
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.statusGreenPresent = function () {
        var alert = this.alertCtrl.create({
            title: this.param.datatext.statusGreenPresent_title,
            message: this.param.datatext.statusGreenPresent_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("OK clicked");
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.leaveDockingConfirm = function () {
        var _this = this;
        // pop up to ask if the robot must leave the docking
        var alert = this.alertCtrl.create({
            title: this.param.datatext.leaveDockingConfirm_title +
                this.api.battery_status.remaining +
                "%",
            message: this.param.datatext.leaveDockingConfirm_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_no,
                    role: "cancel",
                    handler: function () {
                        console.log("Non clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_yes,
                    handler: function () {
                        console.log("Oui clicked");
                        _this.alert.leaveDocking(_this.api.mailAddInformationBasic());
                        _this.api.disconnectHttp();
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.askpswd = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.param.datatext.password,
            message: this.param.datatext.enterPassword,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            inputs: [
                {
                    name: "password",
                    placeholder: this.param.datatext.password,
                    type: "password",
                },
            ],
            buttons: [
                {
                    text: this.param.datatext.btn_cancel,
                    role: "cancel",
                    handler: function (data) {
                        console.log("Cancel clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_save,
                    role: "backdrop",
                    handler: function (data) {
                        if (data.password === atob(_this.param.robot.password)) {
                            _this.accessparam();
                        }
                        else {
                            _this.wrongPassword();
                        }
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.wrongPassword = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.param.datatext.password,
            message: this.param.datatext.wrongPassword,
            cssClass: "alertstyle_wrongpass",
            enableBackdropDismiss: true,
            inputs: [
                {
                    name: "password",
                    placeholder: this.param.datatext.password,
                    type: "password",
                },
            ],
            buttons: [
                {
                    text: this.param.datatext.btn_cancel,
                    role: "cancel",
                    handler: function (data) {
                        console.log("Cancel clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_save,
                    role: "backdrop",
                    handler: function (data) {
                        if (data.password === atob(_this.param.robot.password)) {
                            _this.accessparam();
                        }
                        else {
                            _this.wrongPassword();
                        }
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.displayrgpd = function (ev) {
        var _this = this;
        ev.preventDefault();
        var alert = this.alertCtrl.create({
            title: "RGPD",
            message: this.param.datatext.rgpd_txt,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_cancel,
                    role: "cancel",
                    handler: function (data) {
                        console.log("Cancel clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_save,
                    role: "backdrop",
                    handler: function (data) {
                        _this.addMail(ev);
                    }
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.showToast = function (msg, duration, position) {
        return __awaiter(this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: msg,
                            duration: duration,
                            position: position,
                            cssClass: "toastok"
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    PopupService.prototype.showToastRed = function (msg, duration, position) {
        return __awaiter(this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: msg,
                            duration: duration,
                            position: position,
                            cssClass: "toast"
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    PopupService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_4__param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_2__alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_3__api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */]])
    ], PopupService);
    return PopupService;
}());

//# sourceMappingURL=popup.service.js.map

/***/ }),

/***/ 61:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpeechService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__param_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SpeechService = /** @class */ (function () {
    function SpeechService(param) {
        this.param = param;
        this.msg = new SpeechSynthesisUtterance();
        this.msg.volume = parseFloat("1");
        this.msg.rate = parseFloat("0.9");
        this.msg.pitch = parseFloat("1");
        this.msg.localService = true;
        this.synth = window.speechSynthesis;
        //this.msg.voice = speechSynthesis.getVoices().filter(function(voice) { return voice.name ; })[10];
    }
    SpeechService.prototype.getVoice = function () {
        var _this = this;
        var voices = speechSynthesis
            .getVoices()
            .filter(function (voice) {
            return voice.localService == true && voice.lang == _this.param.langage;
        });
        if (voices.length > 1) {
            this.msg.voice = voices[1];
        }
        else {
            this.msg.voice = voices[0];
        }
    };
    // Create a new utterance for the specified text and add it to
    // the queue.
    SpeechService.prototype.speak = function (text) {
        this.synth.cancel();
        this.msg.lang = this.param.langage;
        this.getVoice();
        // Create a new instance of SpeechSynthesisUtterance.
        // Set the text.
        this.msg.text = text;
        //this.msg.voice = speechSynthesis.getVoices().filter(function(voice) { return voice.name ; })[10];
        //console.log(msg);
        // Queue this utterance.
        this.synth.speak(this.msg);
    };
    SpeechService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__param_service__["a" /* ParamService */]])
    ], SpeechService);
    return SpeechService;
}());

//# sourceMappingURL=speech.service.js.map

/***/ })

},[237]);
//# sourceMappingURL=main.js.map