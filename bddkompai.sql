-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 29 oct. 2020 à 10:08
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `bddkompai`
--

-- --------------------------------------------------------

--
-- Structure de la table `mail`
--

DROP TABLE IF EXISTS `mail`;
CREATE TABLE IF NOT EXISTS `mail` (
  `id_mail` int(11) NOT NULL AUTO_INCREMENT,
  `serialnumber` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  PRIMARY KEY (`id_mail`),
  KEY `fk_mail_robot` (`serialnumber`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `mail`
--

INSERT INTO `mail` (`id_mail`, `serialnumber`, `mail`) VALUES
(1, 'K3V1.9248.A20011.001', 'laetitia.lerandy@kompai.com'),
(3, 'K3V1.9248.A20011.001', 'llerandy@ensc.fr');

-- --------------------------------------------------------

--
-- Structure de la table `phonenumber`
--

DROP TABLE IF EXISTS `phonenumber`;
CREATE TABLE IF NOT EXISTS `phonenumber` (
  `id_phonenumber` int(11) NOT NULL AUTO_INCREMENT,
  `serialnumber` varchar(255) NOT NULL,
  `phonenumber` varchar(255) NOT NULL,
  PRIMARY KEY (`id_phonenumber`),
  KEY `fk_phonenumber_robot` (`serialnumber`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `phonenumber`
--

INSERT INTO `phonenumber` (`id_phonenumber`, `serialnumber`, `phonenumber`) VALUES
(1, 'K3V1.9248.A20011.001', '+33642517645');

-- --------------------------------------------------------

--
-- Structure de la table `robot`
--

DROP TABLE IF EXISTS `robot`;
CREATE TABLE IF NOT EXISTS `robot` (
  `serialnumber` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `mailrobot` varchar(255) NOT NULL,
  `maildata` varchar(255) NOT NULL,
  `tokenmail` varchar(255) NOT NULL,
  `tokendata` varchar(255) NOT NULL,
  `langage` varchar(255) NOT NULL,
  `localhost` varchar(255) NOT NULL,
  `allowspeech` tinyint(1) NOT NULL DEFAULT '1',
  `idpatrol1` int(11) NOT NULL,
  `idpatrol2` int(11) NOT NULL,
  `idround1` int(11) NOT NULL,
  `idround2` int(11) NOT NULL,
  PRIMARY KEY (`serialnumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `robot`
--

INSERT INTO `robot` (`serialnumber`, `name`, `mailrobot`, `maildata`, `tokenmail`, `tokendata`, `langage`, `localhost`, `allowspeech`, `idpatrol1`, `idpatrol2`, `idround1`, `idround2`) VALUES
('K3V1.9248.A20011.001', 'robot', 'k3v1.9248.a20011.001@gmail.com', 'data@kompai.com', '627437e1-7e94-491a-855c-af65912a8df9', 'baa54d7d-10f4-4a29-9763-1f58e6cf29bf', 'fr-FR', '192.168.1.2:7007', 1, 2, 3, 2, 3);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `mail`
--
ALTER TABLE `mail`
  ADD CONSTRAINT `fk_mail_robot` FOREIGN KEY (`serialnumber`) REFERENCES `robot` (`serialnumber`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `phonenumber`
--
ALTER TABLE `phonenumber`
  ADD CONSTRAINT `fk_phonenumber_robot` FOREIGN KEY (`serialnumber`) REFERENCES `robot` (`serialnumber`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
